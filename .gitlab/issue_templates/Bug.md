<!-- Please describe the problem in a short, but detailed manner. Space for more at the end. -->
`<<< PROVIDE POWERFUL DESCRIPTION HERE >>>`

  ---

<!-- Please give the error message. Full stack trace further down. -->
Error message: `<<< PASTE ERROR MESSAGE HERE >>>`
 
  ---

#### How to reproduce the bug

<!-- Please provide a list of steps which resulted in this bug. -->
* *Please describe briefly what you did before the error occurred.*
* *Step 1*
* *Step 2*
* /tableflip

  ---

#### Build information

<!-- Please provide information on the build you were using. -->
* Build: `#<<< PASTE BUILD NUMBER HERE >>>`
* Branch: `<<< PASTE BRANCH HERE >>>`
* Commit: `<<< PASTE COMMIT SHA HERE >>>`

  ---

#### Environment

<!-- Please provide some information on your system environment. -->
* Operating system: `<<< PASTE OS HERE >>>`
* Java version: `<<< PASTE JAVA VERSION HERE >>>`

  ---

#### Stack trace

<!-- Please provide the full stack trace of the exception. -->
````
<<< PASTE STACKTRACE HERE >>>
````

  ---

<!-- You can give additional information here not covered by the above categories. -->
`<<< SPACE FOR ADDITIONAL INFORMATION >>>`
