stages:
  - lint
  - build
  - documentation
  - test
  - bundle

default:
  image: gradle:jdk11

variables:
  GIT_SUBMODULE_STRATEGY: recursive
  GRADLE_USER_HOME: "${CI_PROJECT_DIR}/gradle_home"
  GRADLE_OPTS: "-Dorg.gradle.daemon=false"

ktlint:
  stage: lint
  inherit:
    default: false
    variables: false
  image: kkopper/ktlint:latest
  script:
    - ktlint

classes:
  stage: build
  dependencies: []
  script:
    - gradle classes
  artifacts:
    expire_in: 30 min
    paths:
      - .gradle
      - gradle_home/
      - build/
      - lib/pseuco-java-compiler/build/
      - src/main/resources/build.properties
      - src/main/resources/licenses/LICENSE
      - src/main/resources/include.zip
      - src/main/resources/includeDebug.zip
      - src/main/resources/includeRunner.zip

dokka:
  stage: documentation
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
    - if: $CI_COMMIT_TAG
  dependencies:
    - classes
  script:
    - gradle dokkaHtml
  after_script:
    - cp -R build/dokka .
  artifacts:
    name: "pseuCo_IDE_${CI_PIPELINE_ID}-docs"
    expire_in: 2 weeks
    paths:
      - dokka
  allow_failure: true  # Failing documentation does not impact the other jobs.

tests:
  stage: test
  dependencies:
    - classes
  script:
    - gradle :test  # Only executes the test task of the main project. Thus the compiler tests are excluded.
  after_script:
    - cp -R build/reports/tests/test .
  artifacts:
    name: "pseuCo_IDE_${CI_PIPELINE_ID}-tests"
    when: always
    expire_in: 2 weeks
    paths:
      - test
    reports:
      junit: build/test-results/test/TEST-*.xml

.jar:
  stage: bundle
  dependencies:
    - classes
  script:
    - gradle jar
  after_script:
    - cp build/libs/pseuco-ide-*.jar .

jar:
  extends: .jar
  rules:
    - if: $CI_COMMIT_TAG
      when: never
    - when: on_success
  after_script:
    - cp build/libs/pseuco-ide-*.jar pseuco-ide-${CI_PIPELINE_ID}.jar
  artifacts:
    name: "pseuCo_IDE_${CI_PIPELINE_ID}"
    expire_in: 4 weeks
    paths:
      - pseuco-ide-*.jar

release:
  extends: .jar
  rules:
    - if: $CI_COMMIT_TAG
  after_script:
    - cp build/libs/pseuco-ide-*.jar pseuco-ide-${CI_COMMIT_TAG}.jar
  artifacts:
    name: "pseuCo_IDE_${CI_COMMIT_TAG}"
    paths:
      - pseuco-ide-*.jar
