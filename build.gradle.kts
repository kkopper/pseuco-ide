import org.jetbrains.dokka.gradle.DokkaTask
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

if (!JavaVersion.current().isJava8Compatible)
    error("Only JDK 8 or newer is supported!")

plugins {
    kotlin("jvm") version "1.4.31"
    kotlin("plugin.serialization") version "1.4.31"
    id("org.jetbrains.dokka") version "1.4.30"
}

repositories {
    jcenter()
}

val ktorVersion = "1.5.2"
val junitVersion = "5.7.1"
val aspectjVersion = "1.9.6"

dependencies {
    // The pseuco-java-compiler
    implementation(project(":pseuco-java-compiler"))

    // Kotlin language support
    implementation(kotlin("stdlib-jdk8"))

    // Coroutines
    implementation("org.jetbrains.kotlinx", "kotlinx-coroutines-core", "1.4.3")

    // Kotlin serialization
    implementation("org.jetbrains.kotlinx", "kotlinx-serialization-core", "1.1.0")

    // OpenJFX
    if (JavaVersion.current().isJava11Compatible) {
        for (pkg in listOf("base", "controls", "fxml", "graphics"))
            for (os in listOf("linux", "mac", "win"))
                implementation("org.openjfx", "javafx-$pkg", "15.0.1", classifier = os)
    }

    // Provides RichText TextAreas. Used for code input area.
    implementation("org.fxmisc.richtext", "richtextfx", "0.10.6")

    // Google GSON library for JSON
    implementation("com.google.code.gson", "gson", "2.8.6")

    // Apache Commons
    implementation("commons-io", "commons-io", "2.8.0")
    implementation("org.apache.commons", "commons-lang3", "3.12.0")

    // Ktor Apache client and server framework
    implementation("io.ktor", "ktor-client-apache", ktorVersion)
    implementation("io.ktor", "ktor-client-gson", ktorVersion)
    implementation("io.ktor", "ktor-server-netty", ktorVersion)
    implementation("io.ktor", "ktor-websockets", ktorVersion)
    implementation("io.ktor", "ktor-freemarker", ktorVersion)

    // http4k WebSocket client library
    implementation("org.http4k", "http4k-client-websocket", "4.5.0.1")

    // AspectJ runtime
    runtimeOnly("org.aspectj", "aspectjrt", aspectjVersion)

    // NOP (no-operation) logger implementation
    runtimeOnly("org.slf4j", "slf4j-nop", "1.7.30")

    testImplementation("org.junit.jupiter", "junit-jupiter-api", junitVersion)
    testImplementation("org.junit.jupiter", "junit-jupiter-params", junitVersion)
    testRuntimeOnly("org.junit.jupiter", "junit-jupiter-engine", junitVersion)

    testImplementation("org.mockito", "mockito-junit-jupiter", "3.8.0")
}

version = "2.0.4"
description = "pseuCo IDE"

val compileKotlin by tasks.getting(KotlinCompile::class) {
    kotlinOptions {
        jvmTarget = "1.8"
    }
}

val compileTestKotlin by tasks.getting(KotlinCompile::class) {
    kotlinOptions {
        jvmTarget = "1.8"
    }
}

val jar by tasks.getting(Jar::class) {
    manifest {
        attributes(mapOf("Main-Class" to "fxGui.MainKt", "Class-Path" to "."))
    }

    duplicatesStrategy = DuplicatesStrategy.INCLUDE

    listOf("implementation", "runtimeOnly")
        .map { name -> configurations[name].apply { isCanBeResolved = true } }
        .forEach { config -> from(config.map { if (it.isDirectory) files(it) else zipTree(it) }) }
}

val test by tasks.getting(Test::class) {
    useJUnitPlatform()
}

// Update property file
val processResources by tasks.getting(ProcessResources::class) {
    filesMatching("**/about.properties") {
        filter { it.replace("%APP_VERSION%", project.version as String) }
        filter { it.replace("%APP_DESCRIPTION%", project.description ?: "") }
    }
}

val createBuildProperties by tasks.creating(WriteProperties::class) {
    comment = " Build properties. Set during build process."
    outputFile = File("src/main/resources/build.properties")

    property(
        "branch",
        try {
            "git rev-parse --abbrev-ref HEAD".exec().text
        } catch (e: Exception) {
            logger.warn("Could not gather branch name from git.")
            logger.debug("Stacktrace:", e)
            System.getenv("CI_COMMIT_REF_NAME") ?: ""
        }
    )
    property(
        "commit",
        try {
            "git rev-parse --short HEAD".exec().text
        } catch (e: Exception) {
            logger.warn("Could not gather commit sha from git.")
            logger.debug("Stacktrace:", e)
            System.getenv("CI_COMMIT_SHA")?.substring(0, 8) ?: ""
        }
    )
    property("pipeline", System.getenv("CI_PIPELINE_ID") ?: "")
}

val copyLicense by tasks.creating(Copy::class) {
    from("LICENSE")
    into("src/main/resources/licenses/")
}

processResources.dependsOn(createBuildProperties)
processResources.dependsOn(copyLicense)

val removeBuildFiles by tasks.creating(Delete::class) {
    delete(createBuildProperties.outputFile)
    delete("src/main/resources/licenses/LICENSE")
}

val clean by tasks.getting(Delete::class)
clean.dependsOn(removeBuildFiles)

val compressIncludeFolder by tasks.creating(Zip::class) {
    from("lib/pseuco-java-compiler/include")

    archiveFileName.set("include.zip")
    destinationDirectory.set(file("src/main/resources"))
}

val compressIncludeRunnerFolder by tasks.creating(Zip::class) {
    from("lib/pseuco-java-compiler/include") {
        exclude("Handshake.java")
        exclude("PseuCoThread.java")
        exclude("Simulate.java")
    }
    from("src/main/resources/runner")

    archiveFileName.set("includeRunner.zip")
    destinationDirectory.set(file("src/main/resources"))
}

val compressIncludeDebugFolder by tasks.creating(Zip::class) {
    from("lib/pseuco-java-compiler/include") {
        exclude("Handshake.java")
        exclude("PseuCoThread.java")
        exclude("Simulate.java")
        exclude("*chan.java")
    }
    from("src/main/resources/debugger")

    archiveFileName.set("includeDebug.zip")
    destinationDirectory.set(file("src/main/resources"))
}

processResources.dependsOn(compressIncludeFolder)
processResources.dependsOn(compressIncludeRunnerFolder)
processResources.dependsOn(compressIncludeDebugFolder)

val removeIncludeFolders by tasks.creating(Delete::class) {
    delete("src/main/resources/include.zip")
    delete("src/main/resources/includeRunner.zip")
    delete("src/main/resources/includeDebug.zip")
}

clean.dependsOn(removeIncludeFolders)

/* --- AOP --- */

val ajtools: Configuration by configurations.creating
val ajrt: Configuration by configurations.creating

dependencies {
    ajtools("org.aspectj", "aspectjtools", aspectjVersion)
    ajrt("org.aspectj", "aspectjrt", aspectjVersion)
}

open class AspectJTask : DefaultTask() {

    @InputFiles
    lateinit var inPath: FileCollection

    @InputFiles
    lateinit var sources: FileCollection

    @InputFiles
    lateinit var classpath: FileCollection

    @Internal
    lateinit var taskClasspath: String

    @OutputDirectory
    lateinit var destination: File

    @TaskAction
    fun compileAspect() {
        val iajcArgs = mapOf(
            "inPath" to inPath.asPath,
            "sourceRoots" to sources.asPath,
            "classpath" to classpath.asPath,
            "destDir" to destination.path,
            "source" to "1.8",
            "target" to "1.8",
            "fork" to "true",
            "xlint" to "warning"
        )

        ant.withGroovyBuilder {
            "taskdef"(
                "resource" to "org/aspectj/tools/ant/taskdefs/aspectjTaskdefs.properties",
                "classpath" to taskClasspath
            )
            "iajc"(iajcArgs)
        }
    }
}

val pseucoJavaCompilerProject = project(":pseuco-java-compiler")

val compilerAspects by tasks.creating(AspectJTask::class) {
    classpath = files(pseucoJavaCompilerProject.configurations.compileClasspath, ajrt)
    taskClasspath = ajtools.asPath

    inPath = pseucoJavaCompilerProject.files("build/originalClasses")
    sources = project.files("src/main/kotlin/pseuco/javaCompiler/")
    destination = pseucoJavaCompilerProject.tasks.compileJava.get().destinationDir

    dependsOn(pseucoJavaCompilerProject.tasks.compileJava)
}

pseucoJavaCompilerProject.tasks {
    compileJava { destinationDir = pseucoJavaCompilerProject.file("build/originalClasses") }
    classes { dependsOn(compilerAspects) }
}

/* --- Dokka --- */

tasks.withType<DokkaTask>().configureEach {
    dokkaSourceSets {
        configureEach {
            jdkVersion.set(8)
            includeNonPublic.set(true)

            platform.set(org.jetbrains.dokka.Platform.jvm)

            externalDocumentationLink {
                if (JavaVersion.current().isJava11Compatible) {
                    val path = "https://openjfx.io/javadoc/${dependencyVersion("javafx").substringBefore(".")}/"
                    url.set(url(path))
                    packageListUrl.set(url(path + "element-list"))
                } else
                    url.set(url("https://docs.oracle.com/javase/8/javafx/api/"))
            }

            externalDocumentationLink("https://fxmisc.github.io/richtext/javadoc/${dependencyVersion("richtextfx")}/")
            externalDocumentationLink("https://commons.apache.org/proper/commons-lang/javadocs/api-release/")
            externalDocumentationLink("https://commons.apache.org/proper/commons-io/javadocs/api-release/")
            externalDocumentationLink("https://http4k.org/api/")

            externalDocumentationLink {
                val path = "https://www.javadoc.io/com.google.code.gson/gson/${dependencyVersion("gson")}/"
                url.set(url(path))
                packageListUrl.set(url("${path}element-list"))
            }

            sourceLink {
                localDirectory.set(file("src/main/kotlin"))
                remoteUrl.set(url("https://dgit.cs.uni-saarland.de/pseuco/pseuco-ide/blob/master/src/main/kotlin"))
                remoteLineSuffix.set("#L")
            }
        }
    }
}

// Utility functions

fun String.exec(): Process = Runtime.getRuntime().exec(this)
val Process.text
    get() = inputStream.bufferedReader().use { it.readLine() }.trim()

fun url(path: String) = uri(path).toURL()

fun dependencyVersion(dependencyName: String, configuration: String = "implementation") =
    configurations[configuration].dependencies.find { it.name.startsWith(dependencyName) }!!.version!!
