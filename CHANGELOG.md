# pseuCo IDE Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

<!---
Use the following template:

## <VERSION> - <RELEASE-DATE>
### Added
*   New features go here.

### Changed
*   Changes in existing functionality go here.

### Deprecated
*   Soon-to-be removed features go here.

### Fixed
*   Any bug fixes go here.

### Removed
*   Now removed features go here.

### Security
*   Fixes of vulnerabilities go here.
-->

## [Unreleased][]

## [2.0.4][] - 2020-06-05
### Fixed
*   Autocompletion no longer triggered on `<?`.
*   Debugger initialises internal code mapping only after saving the executed file.
*   Companion web application correctly lists browser support for WebSocket connections. See #35.

## [2.0.3][] - 2019-05-28
### Added
*   Button for clearing the console window.

## [2.0.2][] - 2019-05-27
### Fixed
*   Validate stored workspace path during startup. See #34.

## [2.0.1][] - 2019-04-11
### Added
*   Automatic check for newer versions. See #31.

### Fixed
*   Java 11+ support for pre-built versions.

## 2.0 - 2018-08-25
Initial release of the new pseuCo IDE.
The original IDE is archived [here](https://dgit.cs.uni-saarland.de/pseuco/pseuco-ide-legacy).

<!--- Links -->

[Unreleased]: https://dgit.cs.uni-saarland.de/pseuco/pseuco-ide/compare/v2.0.4...master
[2.0.4]: https://dgit.cs.uni-saarland.de/pseuco/pseuco-ide/compare/v2.0.3...v2.0.4
[2.0.3]: https://dgit.cs.uni-saarland.de/pseuco/pseuco-ide/compare/v2.0.2...v2.0.3
[2.0.2]: https://dgit.cs.uni-saarland.de/pseuco/pseuco-ide/compare/v2.0.1...v2.0.2
[2.0.1]: https://dgit.cs.uni-saarland.de/pseuco/pseuco-ide/compare/v2.0...v2.0.1
