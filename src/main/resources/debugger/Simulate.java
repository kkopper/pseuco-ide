/*******************************************************************************
 * Copyright (c) 2013, Saarland University. All rights reserved.
 * Lisa Detzler
 ******************************************************************************/

package include;

import java.util.LinkedList;
import java.util.Random;

/**
 * Represents a simulator for a hardware interrupt.
 * Extended with functionality to debug pseuCo programs.
 *
 * @author Lisa Detzler, Konstantin Kopper
 */
public class Simulate {

    /**
     * Simulates a random hardware interrupt.
     *
     * @author Lisa Detzler
     */
    public static void HWInterrupt() {
        handleDebugging();

        if (Math.random() < 0.8) {
            try {
                Thread.sleep(5);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            Thread.currentThread().yield();
        }
    }

    /**
     * Checks if the pseuCo program is being debugged.
     * If so, the current threads listener is informed, and the thread is going to wait.
     *
     * @author Konstantin Kopper
     */
    private static void handleDebugging() {
        Thread current = Thread.currentThread();

        if (current.isInterrupted()) {
            throw new RuntimeException(new InterruptedException());
        }

        if (Debugger.isDebugging(current)) {
            Debugger.threadStepped(current);
        }
    }
}
