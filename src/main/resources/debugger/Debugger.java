package include;

import java.util.HashMap;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Class providing static methods for debug mode.
 * Contains listeners which inform the GUI.
 *
 * @author Konstantin Kopper
 */
public class Debugger {

    /**
     * Thread listener for new threads.
     * To be informed whenever a new thread is created during execution.
     */
    static Comparable<Thread> startListener;

    /**
     * Collection of flags indicating if a thread is in debug mode.
     */
    static HashMap<Thread, AtomicBoolean> threadIsDebugged;

    /**
     * Collection of listeners to be informed after breaking.
     */
    static HashMap<Thread, Comparable<Thread>> stepListeners;

    /**
     * Collection of listeners to be informed after a thread terminated.
     */
    static HashMap<Thread, Comparable<Thread>> terminateListeners;

    /**
     * Collection of semaphores used to block threads while debugging.
     * The IDE releases the semaphores, such that the PseuCoThread continues.
     */
    static HashMap<Thread, Semaphore> semaphores;

    static void threadStarted(Thread t) {
        startListener.compareTo(t);
    }

    static void threadStepped(Thread t) {
        final Comparable<Thread> listener;
        synchronized (stepListeners) {
            listener = stepListeners.get(t);
        }
        final Semaphore semaphore;
        synchronized (semaphores) {
            semaphore = semaphores.get(t);
        }
        try {
            listener.compareTo(t);
            semaphore.acquire();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    static void threadTerminated(Thread t) {
        final Comparable<Thread> listener;
        synchronized (terminateListeners) {
            listener = terminateListeners.get(t);
        }
        listener.compareTo(t);
    }

    static boolean isDebugging(Thread t) {
        synchronized (threadIsDebugged) {
            return threadIsDebugged.get(t).get();
        }
    }

    /**
     * Channel listener for new buffered integer channels.
     */
    static Comparable<BlockingQueue<Integer>> newIntChannelListener;

    /**
     * Channel listener for updates in a buffered integer channel.
     */
    static Comparable<BlockingQueue<Integer>> updatedIntChannelListener;

    static void newIntChannel(BlockingQueue<Integer> queue) {
        newIntChannelListener.compareTo(queue);
    }

    static void updatedIntChannel(BlockingQueue<Integer> queue) {
        updatedIntChannelListener.compareTo(queue);
    }

    /**
     * Channel listener for new buffered bool channels.
     */
    static Comparable<BlockingQueue<Boolean>> newBoolChannelListener;

    /**
     * Channel listener for updates in a buffered bool channel.
     */
    static Comparable<BlockingQueue<Boolean>> updatedBoolChannelListener;

    static void newBoolChannel(BlockingQueue<Boolean> queue) {
        newBoolChannelListener.compareTo(queue);
    }

    static void updatedBoolChannel(BlockingQueue<Boolean> queue) {
        updatedBoolChannelListener.compareTo(queue);
    }

    /**
     * Channel listener for new buffered s channels.
     */
    static Comparable<BlockingQueue<String>> newStringChannelListener;

    /**
     * Channel listener for updates in a buffered s channel.
     */
    static Comparable<BlockingQueue<String>> updatedStringChannelListener;

    static void newStringChannel(BlockingQueue<String> queue) {
        newStringChannelListener.compareTo(queue);
    }

    static void updateStringChannel(BlockingQueue<String> queue) {
        updatedStringChannelListener.compareTo(queue);
    }

}
