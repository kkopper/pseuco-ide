<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="/css/style.css">

    <title>Settings - pseuCo IDE</title>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <!-- Navbar content -->
    <span class="navbar-brand mb-0 h1">pseuCo IDE</span>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup"
            aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div class="navbar-nav">
            <a class="nav-item nav-link" href="/"><i class="fas fa-home"></i></a>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">Help</a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="/help/sharing">File sharing with pseuCo.com</a>
                </div>
            </li>
            <a class="nav-item nav-link active" href="/settings"><i class="fas fa-cog"></i></a>
            <a class="nav-link" href="https://dgit.cs.uni-saarland.de/pseuco/pseuco-ide/" target="_blank"
               rel="noopener noreferrer"><i class="fab fa-gitlab"></i></a>
        </div>
    </div>
</nav>
<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/"><i class="fas fa-home"></i></a></li>
            <li class="breadcrumb-item active" aria-current="page"><i class="fas fa-cog"></i>&nbsp;Settings</li>
        </ol>
    </nav>

    <h1>Settings</h1>
    <p>
        This page combines all available settings of the IDE.
    </p>

    <#if confirmation??>
    <div class="alert alert-success" role="alert">
        <i class="fas fa-check"></i>&nbsp;
        The settings have been successfully updated!
    </div>
    <!--
    <div class="alert alert-warning" role="alert">
        <i class="fas fa-exclamation-triangle"></i>&nbsp;
        You need to restart the IDE to apply changes.
    </div>
    -->
    </#if>

    <h2>Workspace</h2>

    <code>
        <div class="input-group mb-3" aria-describedby="workspaceHint">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-folder-open"></i></span>
            </div>
            <input type="text" class="form-control" value="${workspace}" readonly>
        </div>
    </code>
    <small class="form-text text-muted" id="workspaceHint">The workspace can be changed directly in the IDE.</small>
    <br>

    <form action="/settings" method="post">
        <h2>Show <i>missing JDK</i> warning</h2>
        <p>
            To execute or debug your pseuCo programs, the IDE requires an installed JDK.
            Without a JDK, you can still use the IDE besides all functions related to the compiler.
        </p>

        <p>
            If you have no JDK installed, a warning is displayed at the start of the IDE. You can opt out here.
        </p>

        <!--
        <p>
            Do you want a warning to be displayed at the start of the IDE?
        </p>
        -->

        <div class="form-check">
            <input class="form-check-input" type="checkbox" id="jdkWarning" name="jdkWarning"
                <#if !skip_jdk_warning>checked</#if>>
            <label class="form-check-label" for="jdkWarning">Show <i>missing JDK</i> warning</label>
        </div>
        <br>

        <h2>Interaction with pseuCo.com</h2>
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">The pseuCo.com sharing agreement</h5>
                <h6 class="card-subtitle mb-2 text-muted">copied from pseuCo.com</h6>
                <p class="card-text">
                    <b>If you continue, the following will happen:</b> The file will be uploaded to the server and
                    published under a random id. Anyone knowing or guessing this id, and the server operators, can read
                    your file. There is no possibility for you to modify or delete it. Your IP address will be logged if
                    you share files.
                </p>
                <p class="card-text">
                    <b>The fine print:</b> Only upload files to which you own the copyright. By continuing, you grant us
                    a non-exclusive, worldwide, royalty-free, sub-licensable and transferable license to use, publish
                    and create derivative works of the file you submit. We cannot guarantee the availability of the file
                    you uploaded.</p>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="sharingAgreement" name="sharingAgreement"
                        <#if accept_sharing_agreement>checked</#if>>
                    <label class="form-check-label" for="sharingAgreement">Accept the sharing agreement</label>
                </div>
            </div>
        </div>
        <br>

        <div class="card">
            <div class="card-body">
                <h5 class="card-title">The pseuCo.com analysis agreement</h5>
                <h6 class="card-subtitle mb-2 text-muted">copied from pseuCo.com</h6>
                <p class="card-text">
                    PseuCo.com can identify potential data races in your program. This analysis is run on the server.
                    You need to be online to use it.</p>
                <p class="card-text">By using this service, you agree that we submit your pseuCo program to the server
                    to process it. We may temporarily retain a copy of your program as part of log files to help us
                    improve the service.</p>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="analysisAgreement" name="analysisAgreement"
                        <#if accept_analysis_agreement>checked</#if>>
                    <label class="form-check-label" for="analysisAgreement">Accept the analysis agreement</label>
                </div>
            </div>
        </div>
        <br>

        <h3>Local file exchange with pseuCo.com</h3>
        <p>
            See <a href="/help/sharing">this page</a> for information on the available sharing mechanisms.
        </p>

        <#if enforce_api>
            <div class="alert alert-warning">
                Currently, you are always using the sharing API (most likely because the start of the WebSocket server
                failed). Hence changes made here are saved, but not taken into account before restarting the IDE.
            </div>
        </#if>

        <div class="form-check">
            <input class="form-check-input" type="radio" name="sharingMechanism" id="api" value="api"
                <#if !use_websocket>checked</#if>>
            <label class="form-check-label" for="api">Sharing API</label>
        </div>
        <div class="form-check">
            <input class="form-check-input" type="radio" name="sharingMechanism" id="ws" value="ws"
                <#if use_websocket>checked</#if>>
            <label class="form-check-label" for="ws">WebSockets</label>
        </div>
        <br>
        <input class="btn btn-outline-secondary btn-block" type="reset" value="Reset">
        <input class="btn btn-outline-success btn-lg btn-block" type="submit" value="Save">

    </form>
    <hr>

    <h1>Backup configuration</h1>

    <p>
        The IDE uses a configuration file to store and manage all settings on this page.
    </p>

    <a class="btn btn-outline-primary btn-lg btn-block" href="/config" download><i class="fas fa-download"></i>
        &nbsp;Download configuration file</a>
    <hr>

    <h1>Start from scratch</h1>

    <form action="/reset" method="post">
        <p>
            If you want to clear all settings and start with a fresh instance of the IDE, click here.
        </p>
        <div class="form-check">
            <input class="form-check-input" type="checkbox" id="resetConfirm" name="resetConfirm">
            <label class="form-check-label" for="resetConfirm">Please tick the checkbox to confirm that you really want
                to reset the IDE. Otherwise, the button has no effect at all.</label>
        </div>
        <br>
        <input class="btn btn-outline-danger btn-lg btn-block" type="submit" value="Reset">
    </form>
    <br>

</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="/js/jquery.min.js"></script>
<script src="/js/popper.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
</body>
</html>
