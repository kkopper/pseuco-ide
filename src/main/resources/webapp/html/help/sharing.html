<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="/css/style.css">

    <title>File sharing - Help - pseuCo IDE</title>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <!-- Navbar content -->
    <span class="navbar-brand mb-0 h1">pseuCo IDE</span>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup"
            aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div class="navbar-nav">
            <a class="nav-item nav-link" href="/"><i class="fas fa-home"></i></a>
            <li class="nav-item dropdown active">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">Help</a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item active" href="/help/sharing">File sharing with pseuCo.com</a>
                </div>
            </li>
            <a class="nav-item nav-link" href="/settings"><i class="fas fa-cog"></i></a>
            <a class="nav-link" href="https://dgit.cs.uni-saarland.de/pseuco/pseuco-ide/" target="_blank"
               rel="noopener noreferrer"><i class="fab fa-gitlab"></i></a>
        </div>
    </div>
</nav>
<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/"><i class="fas fa-home"></i></a></li>
            <li class="breadcrumb-item"><a href="/help">Help</a></li>
            <li class="breadcrumb-item active" aria-current="page">File sharing with pseuCo.com</li>
        </ol>
    </nav>

    <h1>File sharing with pseuCo.com</h1>
    <p>
        The IDE provides two mechanisms how it can connect to and interact with
        <a href="https://pseuco.com">pseuCo.com</a>. They are presented in the following.
    </p>

    <table class="table">
        <thead>
        <tr>
            <th scope="col"></th>
            <th scope="col"><a href="https://www.google.com/chrome/" target="_blank" rel="noreferrer noopener"><i
                class="fab fa-chrome" title="Google Chrome"></i></a></th>
            <th scope="col"><a href="https://microsoft.com/edge/" target="_blank" rel="noreferrer noopener"><i
                class="fab fa-edge" title="Microsoft Edge"></i></a></th>
            <th scope="col"><a href="https://www.mozilla.org/firefox/new/" target="_blank" rel="noreferrer noopener"><i
                class="fab fa-firefox-browser" title="Mozilla Firefox"></i></a></th>
            <th scope="col"><a href="https://www.opera.com/" target="_blank" rel="noreferrer noopener"><i
                class="fab fa-opera" title="Opera"></i></a></th>
            <th scope="col"><a href="https://www.apple.com/safari/" target="_blank" rel="noreferrer noopener"><i
                class="fab fa-safari" title="Safari"></i></a></th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <th scope="row"><a href="#api">Sharing API</a></th>
            <td><i class="fas fa-check" style="color: green"></i></td>
            <td><i class="fas fa-check" style="color: green"></i></td>
            <td><i class="fas fa-check" style="color: green"></i></td>
            <td><i class="fas fa-check" style="color: green"></i></td>
            <td><i class="fas fa-check" style="color: green"></i></td>
        </tr>
        <tr>
            <th scope="row"><a href="#ws">WebSockets</a></th>
            <td><i class="fas fa-check" style="color: green"></i></td>
            <td><i class="fas fa-check" style="color: green"></i></td>
            <td><i class="fas fa-check" style="color: orange"></i></td>
            <td><i class="fas fa-check" style="color: green"></i></td>
            <td><i class="fas fa-times" style="color: red"></i></td>
        </tr>
        </tbody>
    </table>

    <p>You can choose your preferred mechanism in the <a href="/settings">Settings</a> page.</p>

    <h2 id="api">The sharing API</h2>

    <p>
        Established for file exchange between multiple users, it does not matter whether you query the API using
        pseuCo.com or the IDE. Hence the API can be misused to exchange files with pseuCo.com, by simply uploading a
        file from whatever tool you use, and downloading it again with the other tool.
    </p>

    <div class="alert alert-warning" role="alert">
        Using the API requires an active internet connection.
    </div>

    <p>
        While having a well-established solution working with any modern browser, the drawback of this approach is
        obvious: To connect two local processes running on your machine, an external <b>online</b> service is used. Thus
        one may aim at getting rid of the API and establish a local connection. However, this approach also has
        drawbacks, as discussed in the next section.
    </p>

    <h2 id="ws">A WebSockets based approach</h2>

    <p>
        The problem with any local solution is security: Since pseuCo.com is served over HTTPS and all modern browsers
        restrict insecure connections from secure environments, every local service provided by the IDE must also use
        HTTPS. However, this would require a signed certificate, and since the IDE only operates on
        <code>localhost</code>, who would provide a signed certificate for that hostname?
    </p>

    <div class="alert alert-warning" role="alert">
        This solution does not work with every browser. Check the compatibility table above.
    </div>

    <p>
        Nevertheless, it turned out that most browsers allow insecure connections using WebSockets. Hence the IDE
        provides a WebSocket server, from which pseuCo.com is able to retrieve files from and upload files too, which
        are then opened in the IDE.
    </p>

    <h4>Browser settings</h4>

    <p>
        As you can conclude from the compatibility table above, Chrome, Edge and Opera inherently support this solution,
        while Safari seems to not support it at all. Only Firefox requires you to take action by specifically enabling
        required features:
    </p>
    <ol>
        <li>Open <code>about:config</code>.</li>
        <li>Accept the displayed warning. <i>You know what you do. If you crash something, you are the only one to
            blame.</i>
        </li>
        <li>Find <code>network.websocket.allowInsecureFromHTTPS</code> and activate it by double click.</li>
    </ol>
</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="/js/jquery.min.js"></script>
<script src="/js/popper.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
</body>
</html>
