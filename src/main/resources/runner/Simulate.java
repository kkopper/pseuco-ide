/*******************************************************************************
 * Copyright (c) 2013, Saarland University. All rights reserved.
 * Lisa Detzler
 ******************************************************************************/

package include;

import java.util.LinkedList;
import java.util.Random;

/**
 * Represents a simulator for a hardware interrupt.
 * Extended with functionality to debug pseuCo programs.
 *
 * @author Lisa Detzler, Konstantin Kopper
 */
public class Simulate {

    /**
     * Simulates a random hardware interrupt.
     *
     * @author Lisa Detzler
     */
    public static void HWInterrupt() {
        if (Thread.currentThread().isInterrupted())
            throw new RuntimeException(new InterruptedException());

        if (Math.random() < 0.8) {
            try {
                Thread.sleep(5);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            Thread.currentThread().yield();
        }
    }
}
