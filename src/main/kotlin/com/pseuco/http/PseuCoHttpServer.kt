package com.pseuco.http

import com.google.gson.Gson
import com.pseuco.PseuCoComFile
import com.pseuco.PseuCoServer
import com.pseuco.api.PseuCoComExchange
import io.ktor.application.call
import io.ktor.http.ContentType
import io.ktor.http.HttpStatusCode
import io.ktor.response.header
import io.ktor.response.respondText
import io.ktor.routing.get
import io.ktor.routing.routing
import io.ktor.server.engine.ApplicationEngine
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import java.util.Random

/**
 * Web server used for communicating with [pseuCo.com](https://pseuco.com).
 *
 * @author Konstantin Kopper
 * @since 2.0.0
 * @property port The port the server is listening on (default: 28373).
 * @constructor Creates a new server listening on the given port.
 */
class PseuCoHttpServer(private val port: Int = 28373) : PseuCoServer {

    /**
     * The files available when calling the server.
     *
     * @since 2.0.0
     */
    private val files = HashMap<Int, PseuCoComFile>()

    /**
     * The internal server instance used for communication.
     *
     * @since 2.0.0
     */
    private val server: ApplicationEngine

    /**
     * Random-number generator used for creating unique file ids.
     *
     * @since 2.0.0
     */
    private val idGenerator = Random()

    init {
        server = embeddedServer(Netty, port) {
            routing {
                get("/") {
                    call.respondText("Hello, world!", ContentType.Text.Html)
                }
                get("/files/get") {
                    val fileId = call.request.queryParameters["id"]?.toInt()

                    when (fileId) {
                        in files -> {
                            // Required by pseuCo.com
                            call.response.header("Access-Control-Allow-Origin", "*")

                            call.respondText(Gson().toJson(PseuCoComExchange(files[fileId]!!)), ContentType.Application.Json)
                        }
                        else -> {
                            call.response.status(HttpStatusCode.NotFound)
                            call.respondText("No file was found for the given ID.", ContentType.Text.Plain)
                        }
                    }
                }
                get("/teapot") {
                    call.response.status(HttpStatusCode.fromValue(418))
                    call.respondText("I'm a teapot!", ContentType.Text.Plain)
                }
            }
        }
    }

    override fun start(wait: Boolean) {
        server.start(wait = wait)
    }

    override fun stop() = this.server.stop(0, 0)

    override fun storeFile(file: PseuCoComFile): Int = generateId().also { this.files[it] = file }

    /**
     * Generates a new unique ID.
     * If no free IDs are left, the method will diverge.
     *
     * @author Konstantin Kopper
     * @see Random
     * @since 2.0.0
     */
    private fun generateId(): Int {
        var id: Int

        do {
            id = idGenerator.nextInt(Int.MAX_VALUE)
        } while (id in files)

        return id
    }
}
