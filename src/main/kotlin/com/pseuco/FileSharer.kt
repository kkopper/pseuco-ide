package com.pseuco

import java.net.URI

/**
 * Interface providing functionality to share a file with [pseuCo.com](https://pseuco.com).
 *
 * @author Konstantin Kopper
 * @since 2.0.0
 */
interface FileSharer {

    /**
     * Uploads a given [file] to [pseuCo.com](https://pseuco.com).
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     * @return An URL this file is available at.
     */
    suspend fun uploadFile(file: PseuCoComFile): URI
}
