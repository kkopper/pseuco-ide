package com.pseuco

import kotlinx.serialization.Required
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

/**
 * Class representing sharable pseuCo.com files.
 *
 * @author Konstantin Kopper
 * @since 2.0.0
 * @property name The name of the file.
 * @property content The actual body of the file.
 * @property type The file type.
 */
@Serializable
data class PseuCoComFile(val name: String, val content: String, @Required val type: Type = Type.PSEUCO) {

    /**
     * Enum representing the file types supported by the sharing API.
     */
    @Serializable
    enum class Type {

        /**
         * A pseuCo file.
         */
        @SerialName("pseuco")
        PSEUCO,

        /**
         * A CCS file.
         */
        @SerialName("ccs")
        CCS,

        /**
         * A LTS file.
         */
        @SerialName("lts")
        LTS;
    }
}
