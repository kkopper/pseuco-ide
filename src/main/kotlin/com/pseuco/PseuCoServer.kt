package com.pseuco

/**
 * Common interface for sharing servers operated by the IDE.
 * These server should provide files given an id, while this id was returned by [storeFile].
 *
 * @author Konstantin Kopper
 * @since 2.0.0
 */
interface PseuCoServer {

    /**
     * Starts this server.
     *
     * @param wait Flag indicating if the calling thread should wait until the server terminated.
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    fun start(wait: Boolean = false)

    /**
     * Stops this server.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    fun stop()

    /**
     * Stores the given file such that it is available in requests.
     *
     * @param file The file to be stored.
     * @return The id the file is available at.
     * @author Konstantin Kopper
     * @see PseuCoComFile
     * @since 2.0.0
     */
    fun storeFile(file: PseuCoComFile): Int
}
