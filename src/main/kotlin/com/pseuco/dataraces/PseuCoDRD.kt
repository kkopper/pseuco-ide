package com.pseuco.dataraces

import jani.JaniJSONAdapter
import jani.interaction.ConnectionState
import jani.interaction.JaniWebSocket
import jani.interaction.basic.AnyModel
import jani.interaction.basic.ModellingFormalism
import jani.interaction.basic.messages.Authenticate
import jani.interaction.basic.messages.Close
import jani.interaction.basic.messages.JaniMessage
import jani.interaction.tasks.ProvideTaskMessage
import jani.interaction.tasks.analyse.messages.QueryAnalysisEngines
import jani.interaction.tasks.analyse.messages.StartAnalysisTask
import org.http4k.client.WebsocketClient
import org.http4k.core.Uri
import org.http4k.websocket.WsMessage
import org.http4k.websocket.WsStatus
import java.io.File
import java.util.concurrent.BrokenBarrierException
import java.util.concurrent.CountDownLatch
import java.util.concurrent.CyclicBarrier

/**
 * Analyser of potential data races using the pseuCo.com **D**ata **R**ace **D**etector.
 *
 * @author Konstantin Kopper
 * @since 2.0.0
 */
internal object PseuCoDRD {

    /**
     * The data race detector server.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    private val drdServer = Uri.of("wss://pseuco.com:15292")

    /**
     * The connection established to the data race detection service.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    private lateinit var connection: JaniConnection

    /**
     * Action to be executed on [ProvideTaskMessage] jani messages.
     * Can be set using the [onMessage] function.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    private var onMessage: (ProvideTaskMessage.Severity, String) -> Unit = { _, _ -> }

    /**
     * Sets [action] to be executed whenever a [ProvideTaskMessage] is received.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    fun onMessage(action: (severity: ProvideTaskMessage.Severity, msg: String) -> Unit) {
        onMessage = action
    }

    /**
     * Uploads [file] to the online data race detection service.
     * Waits until a task ended message is received.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    fun analyse(file: File) {
        initializeConnection()

        val model = AnyModel.Textual(
            file.nameWithoutExtension,
            AnyModel.Textual.Part(file.name, "source", file.readText())
        )

        connection.startAnalysisBlocking(model)
    }

    private val isValidConnection: Boolean
        get() = ::connection.isInitialized && connection.state != ConnectionState.CLOSED

    /**
     * Checks whether [connection] is usable, if not creates a new one.
     * If returned, [connection] is in [ConnectionState.INTERACTIVE] state or [DRDException] is thrown.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     * @throws DRDException Could not establish a connection with [ConnectionState.INTERACTIVE] state.
     */
    @Throws(DRDException::class)
    private fun initializeConnection() {
        if (!isValidConnection) {
            connection = JaniConnection()
            connection.onMessage { severity, msg -> onMessage(severity, msg) }

            // Wait until connection is interactive.
            connection.isInteractive.await()
            if (connection.error != null)
                throw connection.error!!
        }

        assert(connection.state == ConnectionState.INTERACTIVE)
    }

    /**
     * Custom exception used to encapsulate any error related to the data race detection service.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     * @param msg An error message.
     * @param cause The exception causing this exception. May be `null`.
     */
    class DRDException(msg: String = "", cause: Throwable? = null) : Exception(msg, cause)

    /**
     * A connection according to the JANI interaction protocol.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     * @constructor Creates a new connection to [drdServer].
     */
    private class JaniConnection {

        /**
         * The [JaniWebSocket] used for actual communication.
         *
         * @author Konstantin Kopper
         * @since 2.0.0
         */
        val socket: JaniWebSocket

        /**
         * The [ConnectionState] of this connection as specified in the JANI interaction protocol.
         *
         * @author Konstantin Kopper
         * @since 2.0.0
         */
        var state = ConnectionState.CONNECTED

        /**
         * The current message identifier.
         * To be incremented before use of a new identifier.
         *
         * @author Konstantin Kopper
         * @since 2.0.0
         */
        var messageId: Int = 0

        /**
         * Error which occurred while the connection was used.
         * Set by [breakConnection].
         *
         * @author Konstantin Kopper
         * @since 2.0.0
         */
        var error: DRDException? = null

        /**
         * Countdown used to signal that the connection is in [ConnectionState.INTERACTIVE] state
         * and the server endpoint provides a valid analysis engine.
         *
         * @author Konstantin Kopper
         * @since 2.0.0
         */
        val isInteractive = CountDownLatch(2)

        /**
         * Cyclic barrier used to await a task ended message.
         * Requires two threads waiting, hence the calling thread and the thread who received the message.
         *
         * @author Konstantin Kopper
         * @since 2.0.0
         */
        val receivedTaskEnded = CyclicBarrier(2)

        init {
            socket = JaniWebSocket(WebsocketClient.nonBlocking(drdServer, listOf("Origin" to "https://pseuco.com")) {
                it.send(WsMessage(JaniJSONAdapter.serialize(Authenticate(1))))
                state = ConnectionState.AUTHENTICATING
            })

            socket.onError {
                state = ConnectionState.CLOSED
                socket.close()
                error = DRDException("Socket was closed with an error.", it)
            }

            socket.onClose { state = ConnectionState.CLOSED }

            socket.onJaniClose {
                state = ConnectionState.CLOSED
                socket.close()
            }

            socket.onCapabilities {
                if (state != ConnectionState.AUTHENTICATING)
                    breakConnection("Received 'Capabilities' while not being in 'Authenticating' state.")

                if (it.version != 1)
                    breakConnection("No common JANI version found.")

                if ("analyse" !in it.roles)
                    breakConnection("Missing analyse role.")

                state = ConnectionState.INTERACTIVE
                socket.send(QueryAnalysisEngines(++messageId))
                isInteractive.countDown()
            }

            socket.onReplyAnalysisEngines {
                if (it.id != messageId)
                    breakConnection("ID not matching.")

                if (state != ConnectionState.INTERACTIVE)
                    breakConnection("Received analysis-engines reply while not being in 'INTERACTIVE' state.")

                if (it.id != messageId)
                    breakConnection("ID not matching.")

                var providesValidEngine = false
                it.engines.forEach { engine ->
                    var foundDRD = false
                    var supportsPseuCo = false

                    if (engine.id.plainString == "DRD")
                        foundDRD = true

                    if (engine.modellingFormalisms.let { it != null && ModellingFormalism.new("x-pseuco") in it })
                        supportsPseuCo = true

                    if (foundDRD && supportsPseuCo) {
                        providesValidEngine = true
                        return@forEach
                    }
                }

                if (!providesValidEngine)
                    breakConnection("No supported engine provided.")

                isInteractive.countDown()
            }

            socket.onProvideTaskMessage {
                if (it.id != messageId)
                    breakConnection("Message ID not matching.")

                println(it.message)
            }

            socket.onTaskEnded {
                if (it.id != messageId)
                    breakConnection("Message ID not matching.")

                receivedTaskEnded.await()
            }

            // TODO more message types
        }

        /**
         * Send a [StartAnalysisTask] message with [model] and the DRD service.
         * Blocks until a *task ended* message was received or the connection was closed prematurely.
         *
         * [onMessage] should be set before calling this method.
         *
         * @author Konstantin Kopper
         * @since 2.0.0
         * @throws DRDException The connection was closed prematurely.
         */
        fun startAnalysisBlocking(model: AnyModel) {
            send(StartAnalysisTask(++messageId, "DRD", "x-pseuco", model, emptyList()))

            // Wait until a task ended message is received.
            try {
                receivedTaskEnded.await()
            } catch (e: BrokenBarrierException) {
                throw error!!
            }

            receivedTaskEnded.reset()
        }

        /**
         * Sets [socket]`.onProvideTaskMessage` to execute [action] when called.
         *
         * @author Konstantin Kopper
         * @since 2.0.0
         */
        inline fun onMessage(crossinline action: (severity: ProvideTaskMessage.Severity, msg: String) -> Unit) {
            socket.onProvideTaskMessage {
                if (it.id != messageId)
                    breakConnection("Message identifier not matching, expected $messageId, got ${it.id}.")

                action(it.severity, it.message)
            }
        }

        /**
         * Sends a [JaniMessage] over [socket].
         *
         * @author Konstantin Kopper
         * @since 2.0.0
         */
        private fun send(msg: JaniMessage) {
            if (state != ConnectionState.INTERACTIVE)
                breakConnection("Not in 'Interactive' state.")

            socket.send(msg)
        }

        /**
         * Sends [Close] message with [reason] and throws a [DRDException], which is also stored in [error].
         *
         * @author Konstantin Kopper
         * @since 2.0.0
         * @throws DRDException encapsulating [reason].
         */
        @Throws(DRDException::class)
        private fun breakConnection(reason: String): Nothing {
            error = DRDException(reason)

            // Send a Close message.
            socket.janiClose(reason)

            // Release all threads waiting for the connection to become interactive.
            while (isInteractive.count > 0)
                isInteractive.countDown()

            // Break task ended barrier. Waiting threads receive a BrokenBarrierException.
            if (receivedTaskEnded.numberWaiting != 0)
                receivedTaskEnded.reset()

            throw error!!
        }
    }

    /**
     * Closes the underlying [connection] (if previously established).
     *
     * @author Konstantin Kopper
     */
    fun leave() {
        if (::connection.isInitialized)
            connection.socket.close(WsStatus.GOING_AWAY)
    }
}
