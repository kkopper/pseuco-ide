package com.pseuco.api

/**
 * Exception used to wrap all exceptions occurring in the [pseuCo.com](https://pseuco.com) sharing process.

 * @author Konstantin Kopper
 * @since 2.0.0
 * @see PseuCoShare
 */
class PseuCoShareException(message: String? = null, cause: Throwable? = null) : Exception(message, cause)
