package com.pseuco.api

import com.pseuco.PseuCoComFile
import kotlinx.serialization.KSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.builtins.serializer
import kotlinx.serialization.descriptors.buildClassSerialDescriptor
import kotlinx.serialization.encoding.CompositeDecoder
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

/**
 * Class mimicking the JSON object format used by the [pseuCo.com](https://pseuco.com) sharing API.
 *
 * @author Konstantin Kopper
 * @since 2.0.0
 * @property file The encoded file.
 * @property temporary Boolean flag indicating whether the file should be stored permanently.
 * @property sharingAgreementVersion The version of the accepted sharing agreement. Required if [temporary] is `false`.
 * @constructor Creates a new file exchange wrapper.
 */
@Serializable(PseuCoComExchange.Companion::class)
data class PseuCoComExchange(val file: PseuCoComFile, val temporary: Boolean, val sharingAgreementVersion: Int?) {

    init {
        require(temporary or (sharingAgreementVersion != null))
    }

    /**
     * Creates a new wrapper for a file to be not stored permanently.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    constructor(file: PseuCoComFile) : this(file, true, null)

    /**
     * Creates a new wrapper for a file to be stored permanently.
     */
    constructor(file: PseuCoComFile, sharingAgreementVersion: Int) : this(file, false, sharingAgreementVersion)

    /**
     * Serializer for [PseuCoComExchange] objects.
     *
     * @author Konstantin Kopper
     * @since 2.0.4
     */
    companion object : KSerializer<PseuCoComExchange> {
        override val descriptor = buildClassSerialDescriptor("PseuCoComExchange") {
            element("file", PseuCoComFile.serializer().descriptor)
            element("temporary", Boolean.serializer().descriptor)
            element("sharingAgreementVersion", Int.serializer().descriptor, isOptional = true)
        }

        override fun deserialize(decoder: Decoder): PseuCoComExchange {
            val structure = decoder.beginStructure(descriptor)

            lateinit var file: PseuCoComFile
            var temporary = true
            var sharingAgreementVersion: Int? = null

            tailrec fun decodeAttributes(): PseuCoComExchange {
                when (structure.decodeElementIndex(descriptor)) {
                    CompositeDecoder.DECODE_DONE -> {
                        structure.endStructure(descriptor)
                        return PseuCoComExchange(file, temporary, sharingAgreementVersion)
                    }
                    0 -> file = decoder.decodeSerializableValue(PseuCoComFile.serializer())
                    1 -> temporary = decoder.decodeBoolean()
                    2 -> sharingAgreementVersion = decoder.decodeInt()
                }
                return decodeAttributes()
            }

            return decodeAttributes()
        }

        override fun serialize(encoder: Encoder, value: PseuCoComExchange) {
            val structure = encoder.beginStructure(descriptor)
            structure.encodeSerializableElement(descriptor, 0, PseuCoComFile.serializer(), value.file)
            structure.encodeBooleanElement(descriptor, 1, value.temporary)
            if (!value.temporary)
                structure.encodeIntElement(descriptor, 2, value.sharingAgreementVersion!!)
            structure.endStructure(descriptor)
        }
    }
}
