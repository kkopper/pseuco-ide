package com.pseuco.api

import com.pseuco.FileSharer
import com.pseuco.PseuCoComFile
import io.ktor.client.HttpClient
import io.ktor.client.engine.apache.Apache
import io.ktor.client.request.accept
import io.ktor.client.request.get
import io.ktor.client.request.post
import io.ktor.client.statement.HttpStatement
import io.ktor.http.ContentType
import io.ktor.http.HttpStatusCode
import io.ktor.http.URLProtocol
import io.ktor.http.content.TextContent
import io.ktor.http.withCharset
import io.ktor.utils.io.jvm.javaio.toInputStream
import kotlinx.serialization.SerializationException
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.jsonObject
import kotlinx.serialization.json.jsonPrimitive
import java.awt.Desktop
import java.net.URI
import java.net.URISyntaxException

/**
 * Provides methods to communicate with the [pseuCo.com](https://pseuco.com) sharing API.
 *
 * @author Konstantin Kopper
 * @since 2.0.0
 */
@Suppress("EXPERIMENTAL_API_USAGE")
object PseuCoShare : FileSharer {

    /**
     * The client object used for all communication.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     * @see Apache
     */
    private val client = HttpClient(Apache)

    fun close() = client.close()

    /**
     * Upload a file to the [pseuCo.com](https://pseuco.com) sharing API.
     * The file is only kept temporary.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     * @param file The wrapped file to be uploaded.
     * @return The URI the file is made available at by the API.
     * @throws PseuCoShareException Something went wrong.
     */
    @Throws(PseuCoShareException::class)
    override suspend fun uploadFile(file: PseuCoComFile): URI = shareFile(file, true)

    /**
     * Uploads [file] to the [pseuCo.com](https://pseuco.com) sharing API.
     *
     * @author Konstantin Kopper
     * @since 2.00
     * @param temporary Flag indicating if the API should store the [file] only temporary. Defaults to `false`.
     * @return The [URI] the file is available at.
     * @throws PseuCoShareException Something went wrong.
     */
    @Throws(PseuCoShareException::class)
    suspend fun shareFile(file: PseuCoComFile, temporary: Boolean = false): URI {
        val c = client.post<HttpStatement> {
            url {
                protocol = URLProtocol.HTTPS
                host = "pseuco.com"
                port = protocol.defaultPort
                path("api", "paste", "add")
            }
            accept(ContentType.Application.Json.withCharset(Charsets.UTF_8))
            body = TextContent(
                Json.encodeToString(
                    if (temporary) PseuCoComExchange(file) else PseuCoComExchange(file, 1)
                ),
                contentType = ContentType.Application.Json
            )
        }.execute()

        if (c.status != HttpStatusCode.OK)
            throw PseuCoShareException("Unexpected status code, expected ${HttpStatusCode.OK.value}, got ${c.status}",
                c.content.toInputStream().reader().use { it.readText() }.takeUnless { it.isEmpty() }
                    ?.let { IllegalStateException("Response body: $it") })

        return try {
            URI(
                Json { allowStructuredMapKeys = true }.parseToJsonElement(
                    c.content.toInputStream().reader().use { it.readText() }).jsonObject["url"]!!.jsonPrimitive.content
            )
        } catch (e: SerializationException) {
            throw PseuCoShareException("Parsing the API response failed.", e)
        } catch (e: URISyntaxException) {
            throw PseuCoShareException("Malformed URL.", e)
        }
    }

    /**
     * Download a file from the [pseuCo.com](https://pseuco.com) sharing API.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     * @param id The identifier the file is available at the API.
     * @return A wrapped file.
     * @throws PseuCoShareException Something went wrong.
     */
    @Throws(PseuCoShareException::class)
    suspend fun downloadFile(id: String): PseuCoComFile {
        val c = client.get<HttpStatement> {
            url {
                protocol = URLProtocol.HTTPS
                host = "pseuco.com"
                port = protocol.defaultPort
                path("api", "paste", "get")
                parameters["id"] = id
            }
            accept(ContentType.Application.Json.withCharset(Charsets.UTF_8))
        }.execute()

        if (c.status != HttpStatusCode.OK)
            throw PseuCoShareException("Unexpected status code, expected ${HttpStatusCode.OK.value}, got ${c.status}.",
                c.content.toInputStream().reader().use { it.readText() }.takeUnless { it.isEmpty() }
                    ?.let { IllegalStateException("Response body: $it") })

        return try {
            Json.decodeFromString<PseuCoComExchange>(c.content.toInputStream().reader().use { it.readText() }).file
        } catch (e: SerializationException) {
            throw PseuCoShareException("Parsing the API response failed.", e)
        }
    }

    /**
     * Opens the given URI in the local default browser.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     * @param uri The URI to be opened.
     * @see URI
     */
    fun openInBrowser(uri: URI) = Desktop.getDesktop().browse(uri)
}
