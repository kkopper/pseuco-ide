package com.pseuco

import com.pseuco.api.PseuCoShare
import java.net.URI

/**
 * [FileSharer] delegating to another [FileSharer].
 * Allows to easily change the globally used [FileSharer].
 *
 * @author Konstantin Kopper
 * @since 2.0.0
 */
object PseuCoFileSharer : FileSharer {

    /**
     * The file sharer this sharer delegates to.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    private lateinit var sharer: FileSharer

    /**
     * Flag indicating if the use of the API should be enforced.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    var enforceAPI = false
        set(value) {
            if (value) useAPI()
            field = value
        }

    /**
     * Update settings to use the [FileSharer] connecting to the [pseuCo.com](https://pseuco.com) file sharing API.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    fun useAPI() {
        sharer = PseuCoShare
    }

    /**
     * Update settings to use a local server [pseuCo.com](https://pseuco.com) can connect to.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    fun useServer(s: PseuCoServer) {
        if (enforceAPI) return
        sharer = PseuCoServerShare { server = s }
    }

    override suspend fun uploadFile(file: PseuCoComFile): URI = sharer.uploadFile(file)
}
