package com.pseuco

import java.net.URI

/**
 * File sharer uploading a file to a local server.
 *
 * @author Konstantin Kopper
 * @since 2.0.0
 */
object PseuCoServerShare : FileSharer {

    /**
     * The server used for sharing.
     */
    lateinit var server: PseuCoServer

    override suspend fun uploadFile(file: PseuCoComFile): URI = server.storeFile(file).let {
        URI.create("https://pseuco.com/#/edit/ide/$it")
    }

    /**
     * Executes [block] on this file sharer.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    inline operator fun invoke(block: PseuCoServerShare.() -> Unit): PseuCoServerShare = this.apply(block)
}
