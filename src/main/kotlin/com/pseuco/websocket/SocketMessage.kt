package com.pseuco.websocket

import com.pseuco.PseuCoComFile
import kotlinx.serialization.Required
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

/**
 * All valid messages used in the pseuCo WebSockets file sharing protocol.
 * See [this GitLab issue](https://dgit.cs.uni-saarland.de/pseuco/pseuco-ide/issues/15) for more information.
 *
 * @author Konstantin Kopper
 * @since 2.0.0
 * @see PseuCoWebSocket
 * @param type A string representation of the message type.
 * @constructor Creates a new Socket message.
 */
@Serializable
sealed class SocketMessage {

    /**
     * All valid request messages.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     * @constructor Creates a new request with [type].
     */
    @Serializable
    sealed class Request : SocketMessage() {

        /**
         * A request of [Request.type] `get`.
         * Holds a unique [id].
         * Should be answered with a [Response.File] message.
         *
         * @author Konstantin Kopper
         * @since 2.0.0
         * @param id Unique identifier of the requested file.
         */
        @Serializable
        @SerialName("get")
        data class Get(val id: Int) : Request()

        /**
         * A request of [Request.type] `open`.
         * Holds a [file], which should be stored locally and opened in the IDE.
         * Should be answered with a [Response.Success] message.
         *
         * @author Konstantin Kopper
         * @since 2.0.0
         * @param file The file to be opened.
         */
        @Serializable
        @SerialName("open")
        data class Open(val file: PseuCoComFile) : Request()
    }

    /**
     * All valid response messages.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     * @constructor Creates a new response with [type].
     */
    @Serializable
    sealed class Response : SocketMessage() {

        /**
         * A response of [Response.type] `file`.
         * Holds the requested [file].
         * Should follow on a [Request.Get] message.
         *
         * @author Konstantin Kopper
         * @since 2.0.0
         * @param file The requested file.
         */
        @Serializable
        @SerialName("file")
        data class File(val file: PseuCoComFile) : Response()

        /**
         * A response of [Response.type] `success`.
         * Should follow on a [Request.Open] message.
         *
         * @author Konstantin Kopper
         * @since 2.0.0
         */
        @Serializable
        @SerialName("success")
        object Success : Response()

        /**
         * A response of [Response.type] `error`.
         * Holds a [message] describing the error, and a [code] which may identify the error precisely.
         * May follow on any [Request] message.
         *
         * @author Konstantin Kopper
         * @since 2.0.0
         * @param message A description of the error.
         * @param code An identifier which may identify the error precisely.
         */
        @Serializable
        @SerialName("error")
        data class Error(val message: String, @Required val code: Code = Code.GENERAL) : Response() {

            /**
             * Possible error codes used in the [Error] messages.
             *
             * @author Konstantin Kopper
             */
            @Serializable
            enum class Code {

                /**
                 * Default code describing arbitrary error messages.
                 *
                 * @author Konstantin Kopper
                 * @since 2.0.0
                 */
                @SerialName("general")
                GENERAL,

                /**
                 * Error following a [Request.Get], where no file was found for [Request.Get.id].
                 *
                 * @author Konstantin Kopper
                 * @since 2.0.0
                 */
                @SerialName("fileNotFound")
                FILE_NOT_FOUND;
            }
        }
    }
}
