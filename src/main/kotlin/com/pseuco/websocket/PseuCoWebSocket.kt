@file:Suppress("EXPERIMENTAL_API_USAGE")

package com.pseuco.websocket

import com.pseuco.PseuCoComFile
import com.pseuco.PseuCoServer
import com.pseuco.websocket.SocketMessage.Response.Error.Code
import io.ktor.application.ApplicationCallPipeline
import io.ktor.application.ApplicationFeature
import io.ktor.application.install
import io.ktor.http.HttpStatusCode
import io.ktor.http.cio.websocket.CloseReason
import io.ktor.http.cio.websocket.Frame
import io.ktor.http.cio.websocket.WebSocketSession
import io.ktor.http.cio.websocket.close
import io.ktor.http.cio.websocket.readText
import io.ktor.request.header
import io.ktor.response.respond
import io.ktor.routing.routing
import io.ktor.server.engine.ApplicationEngine
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import io.ktor.util.AttributeKey
import io.ktor.websocket.WebSocketServerSession
import io.ktor.websocket.WebSockets
import io.ktor.websocket.webSocket
import kotlinx.coroutines.launch
import kotlinx.serialization.SerializationException
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.decodeFromJsonElement
import kotlinx.serialization.json.jsonObject
import util.Workspace
import java.io.File
import java.util.Random
import kotlin.collections.set
import kotlin.reflect.KProperty

/**
 * File exchange server using WebSockets.
 * Implements the pseuCo WebSockets file sharing protocol
 * (see [this issue](https://dgit.cs.uni-saarland.de/pseuco/pseuco-ide/issues/15) for more information).
 *
 * @author Konstantin Kopper
 * @since 2.0.0
 * @see SocketMessage
 */
object PseuCoWebSocket : PseuCoServer {

    /**
     * The port this server is listening at.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    private const val port = 28373

    /**
     * The files this server provides.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    private val files = mutableMapOf<Int, PseuCoComFile>()

    /**
     * Action triggered when receiving [SocketMessage.Request.Open] messages. Takes the newly created file as parameter.
     * Can be set to update GUI accordingly.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    var onOpen: (File) -> Unit = {}

    /**
     * List of active connections.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    private val connections = mutableListOf<WebSocketServerSession>()

    /**
     * [Json] instance used to parse and serialize [SocketMessage]s.
     * Ignores unknown keys during parsing.
     *
     * @author Konstantin Kopper
     * @since 2.0.4
     */
    private val jsonNonStrict = Json { ignoreUnknownKeys = true }

    /**
     * The actual server.
     * Listens on [port] for new WebSocket connections.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    private val server: ApplicationEngine = embeddedServer(Netty, port) {
        install(CheckOrigin)
        install(WebSockets)
        routing {
            webSocket("/files") {
                connections += this
                serverLoop@ while (!incoming.isClosedForReceive) {
                    val frame = incoming.receive()
                    if (frame !is Frame.Text) {
                        close(CloseReason(CloseReason.Codes.CANNOT_ACCEPT, "Only text frames are supported."))
                        connections.remove(this)
                        return@webSocket
                    }

                    @Suppress("MoveVariableDeclarationIntoWhen")
                    val request = Json.parseToJsonElement(frame.readText()).let {
                        val o = try {
                            it.jsonObject
                        } catch (e: IllegalArgumentException) {
                            sendError("Expected a JSON object.")
                            return@let null
                        }

                        if ("type" !in o) {
                            sendError("Missing type attribute.")
                            return@let null
                        }

                        try {
                            jsonNonStrict.decodeFromJsonElement<SocketMessage>(it)
                        } catch (e: SerializationException) {
                            sendError("Parsing the request failed.")
                            null
                        } catch (e: Throwable) {
                            sendError("Internal server error: ${e.localizedMessage}")
                            null
                        }
                    } ?: continue@serverLoop

                    when (request) {
                        is SocketMessage.Request.Get -> {
                            if (request.id !in files) {
                                sendError(Code.FILE_NOT_FOUND, "No file available for the requested id.")
                                continue@serverLoop
                            }

                            send(SocketMessage.Response.File(files[request.id]!!))
                        }

                        is SocketMessage.Request.Open -> {
                            try {
                                onOpen(Workspace.createNewFile(request.file))
                            } catch (e: Exception) {
                                sendError("Internal server error: ${e.localizedMessage}")
                                continue@serverLoop
                            }
                            send(SocketMessage.Response.Success)
                        }
                    }
                }
            }
        }
    }

    override fun start(wait: Boolean) {
        server.start(wait = wait)
    }

    override fun stop() {
        // Close remaining connections and stop server.
        connections.forEach { it.launch { it.close(CloseReason(CloseReason.Codes.NORMAL, "Shutting down.")) } }
        server.stop(10, 10)
    }

    override fun storeFile(file: PseuCoComFile): Int = generateID().also { files[it] = file }

    /**
     * Creates a random integer in [0, [Int.MAX_VALUE]).
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    private fun generateID(): Int = Random().let {
        var id: Int
        do id = it.nextInt(Int.MAX_VALUE)
        while (id in files)
        id
    }

    /* Utility functions */

    /**
     * Create a WebSockets frame from a given [msg].
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    private fun createFrame(msg: SocketMessage): Frame = Frame.Text(Json.encodeToString(msg))

    /**
     * Send [msg] over a WebSockets connection.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    private suspend fun WebSocketSession.send(msg: SocketMessage) = send(createFrame(msg))

    /**
     * Send error [msg] over a WebSockets connection.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     * @see SocketMessage.Response.Error
     */
    private suspend fun WebSocketSession.sendError(msg: String) = send(SocketMessage.Response.Error(msg))

    /**
     * Send error [msg] with a special [code] over a WebSockets connection.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     * @see SocketMessage.Response.Error
     * @see SocketMessage.Response.Error.Code
     */
    private suspend fun WebSocketSession.sendError(code: Code, msg: String) = send(SocketMessage.Response.Error(msg, code))

    /* Custom features */

    /**
     * Custom feature checking the origin header of a request.
     * Discards requests where the header does not matches [regex].
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     * @see ApplicationFeature
     * @constructor Creates a new feature based on the passed configuration.
     */
    private class CheckOrigin(configuration: Configuration) {

        /**
         * Regular expression origin headers are matched against.
         * Delegated to this features [Configuration].
         *
         * @author Konstantin Kopper
         * @since 2.0.0
         * @see Configuration.regex
         */
        val regex by configuration

        /**
         * Configuration of the [CheckOrigin] feature.
         *
         * @author Konstantin Kopper
         * @since 2.0.0
         */
        class Configuration {
            /**
             * Regular expression used for matching origin headers.
             * If not changed, the following origins are accepted:
             *  * [`pseuCo.com`](https://pseuco.com)
             *  * `localhost`
             *  * `127.0.0.1`
             *  * `::1`
             *
             * @author Konstantin Kopper
             * @since 2.0.0
             * @see [CheckOrigin.regex].
             */
            var regex: Regex = Regex("^https?://((.+\\.)?pseuco\\.com|localhost|127\\.0\\.0\\.1|::1)")

            /**
             * Get a delegated property.
             *
             * @author Konstantin Kopper
             * @since 2.0.0
             */
            operator fun getValue(thisRef: Any?, property: KProperty<*>): Regex = when (property.name) {
                "regex" -> regex
                else -> throw RuntimeException("$thisRef delegated an unknown property: '${property.name}'")
            }
        }

        /**
         * Installable implementation of this feature.
         *
         * @author Konstantin Kopper
         * @since 2.0.0
         */
        companion object : ApplicationFeature<ApplicationCallPipeline, Configuration, CheckOrigin> {
            /**
             * Unique key identifying [CheckOrigin].
             *
             * @author Konstantin Kopper
             * @since 2.0.0
             */
            override val key: AttributeKey<CheckOrigin> = AttributeKey("CheckOrigin")

            /**
             * [CheckOrigin]s install script.
             *
             * @author Konstantin Kopper
             * @since 2.0.0
             */
            override fun install(pipeline: ApplicationCallPipeline, configure: Configuration.() -> Unit): CheckOrigin {
                val config = Configuration().apply(configure)
                val feature = CheckOrigin(config)

                pipeline.intercept(ApplicationCallPipeline.Call) {
                    context.request.header("origin").let {
                        if (it == null || !feature.regex.matches(it))
                            context.respond(HttpStatusCode.Forbidden)
                    }
                }

                return feature
            }
        }
    }
}
