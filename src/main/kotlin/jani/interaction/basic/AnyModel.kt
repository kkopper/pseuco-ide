package jani.interaction.basic

import jani.model.models.Model

sealed class AnyModel {

    // TODO check if best solution
    data class JaniModel(private val m: Model) : AnyModel()

    data class Textual(
        val name: String,
        val parts: List<Part>
    ) : AnyModel() {
        constructor(name: String, vararg parts: Part) : this(name, parts.asList())

        data class Part(
            val name: String,
            val role: String,
            val part: String
        )
    }
}
