package jani.interaction.basic

import com.google.gson.annotations.SerializedName

data class ParameterDefinition(
    val id: Identifier,
    val name: String,
    val description: String?,
    val category: String?,
    @SerializedName("is-global") val isGlobal: Boolean,
    val type: ParameterType,
    @SerializedName("default-value") val defaultValue: DefaultValue
) {
    init {
        // TODO do some checks
    }

    sealed class DefaultValue {
        object Null : DefaultValue()
        object True : DefaultValue()
        object False : DefaultValue()
        sealed class Number : DefaultValue() {
            data class Integer(val n: Int) : Number()
            data class Real(val n: Double) : Number()
        }
        // TODO string and enums
    }
}
