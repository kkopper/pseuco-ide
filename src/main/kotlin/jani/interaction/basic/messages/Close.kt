package jani.interaction.basic.messages

data class Close(val reason: String?) : JaniMessage("close")
