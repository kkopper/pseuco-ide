package jani.interaction.basic.messages

data class ReplyUpdateServerParameters private constructor(
    val id: Int,
    val success: Boolean,
    val error: String?
) : JaniMessage("server-parameters") {

    init {
        assert(id >= 1)
        assert(success xor (error != null && !success))
    }

    /**
     * Creates a new message indicating a successful parameter update.
     *
     * @param id The unique identifier of the request that this is a reply for.
     */
    constructor(id: Int) : this(id, true, null)

    /**
     * Creates a new message indicating unsuccessful parameter update with an [error] message.
     *
     * @param id The unique identifier of the request that this is a reply for.
     */
    constructor(id: Int, error: String) : this(id, false, error)
}
