package jani.interaction.basic.messages

import com.google.gson.annotations.SerializedName
import jani.interaction.basic.Extension

data class Authenticate(
    @SerializedName("jani-versions") val versions: List<Int>,
    val extensions: List<Extension>?,
    val login: String?,
    val password: String?
) : JaniMessage() {

    init {
        assert(versions.isNotEmpty())
        assert((login == null && password == null) xor (login != null && password != null))
    }

    constructor(versions: List<Int>) : this(versions, null, null, null)

    constructor(versions: List<Int>, extensions: List<Extension>) : this(versions, extensions, null, null)

    constructor(vararg versions: Int) : this(versions.toList(), null, null, null)

    constructor(versions: List<Int>, login: String, password: String) : this(versions, null, login, password)

    constructor(login: String, password: String, vararg versions: Int) : this(versions.toList(), login, password)
}
