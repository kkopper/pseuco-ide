package jani.interaction.basic.messages

abstract class JaniMessage protected constructor(protected val type: String?) {

    protected constructor() : this(null)
}
