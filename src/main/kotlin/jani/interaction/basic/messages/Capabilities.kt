package jani.interaction.basic.messages

import com.google.gson.annotations.SerializedName
import jani.interaction.basic.Extension
import jani.interaction.basic.Metadata
import jani.interaction.basic.ParameterDefinition

data class Capabilities(
    @SerializedName("jani-version") val version: Int,
    val extensions: List<Extension>?,
    val metadata: Metadata,
    val parameters: List<ParameterDefinition>?,
    val roles: List<String>
) : JaniMessage("capabilities") {

    init {
        assert(version >= 1)
    }
}
