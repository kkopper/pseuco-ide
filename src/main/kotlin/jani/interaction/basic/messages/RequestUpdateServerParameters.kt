package jani.interaction.basic.messages

import jani.interaction.basic.ParameterValue

data class RequestUpdateServerParameters(
    val id: Int,
    val parameters: List<ParameterValue>
) : JaniMessage("server-parameters") {

    init {
        assert(id >= 1)
    }
}
