package jani.interaction.basic

sealed class ModellingFormalism(val string: String) {

    val asString
        get() = string

    object IOSA : ModellingFormalism("iosa")
    object Modest : ModellingFormalism("modest")
    object pGCL : ModellingFormalism("pgcl")
    object PRISM : ModellingFormalism("prism")
    object xSADF : ModellingFormalism("xsadf")

    private class ModellingFormalismImpl(string: String) : ModellingFormalism(string) {
        init {
            require(string.startsWith("x-"), { "The name of custom modelling formalisms must start 'x-'." })
        }

        override fun toString(): String = asString
    }

    override fun equals(other: Any?) = other != null && other is ModellingFormalism && other.string == this.string

    override fun hashCode() = string.hashCode()

    companion object {
        fun new(s: String): ModellingFormalism = ModellingFormalismImpl(s)
    }
}
