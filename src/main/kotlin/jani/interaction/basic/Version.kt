package jani.interaction.basic

data class Version(val major: Int, val minor: Int, val revision: Int) {

    init {
        assert(major >= 0)
        assert(minor >= 0)
        assert(revision >= 0)
    }
}
