package jani.interaction.basic

import com.google.gson.annotations.SerializedName

data class ParameterValue(
    @SerializedName("id") val identifier: Identifier,
    val value: Values
) {

    sealed class Values {
        object True : Values()
        object False : Values()

        data class Number(val number: Int) : Values()
        data class String(val string: kotlin.String) : Values()
    }
}
