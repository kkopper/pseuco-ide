package jani.interaction.basic

data class Metadata(
    val name: String,
    val version: Version,
    val author: String?,
    val description: String?,
    val url: String?
)
