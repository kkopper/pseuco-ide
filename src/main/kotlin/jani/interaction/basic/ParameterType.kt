package jani.interaction.basic

sealed class ParameterType {

    object Bool : ParameterType()
    object Int : ParameterType()
    object Real : ParameterType()
    object String : ParameterType()

    data class Enum(val values: List<kotlin.String>) : ParameterType() {
        init {
            assert(values.isNotEmpty())
        }

        constructor(vararg values: kotlin.String) : this(values.asList())

        operator fun get(index: kotlin.Int) = values[index]
        operator fun contains(value: kotlin.String) = value in values
    }

    data class Option(val type: ParameterType) : ParameterType() {
        val kind = "option"

        init {
            assert(type !is Option)
        }
    }
}
