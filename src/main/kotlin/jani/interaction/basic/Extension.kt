package jani.interaction.basic

sealed class Extension {

    object PersistentState : Extension()
}
