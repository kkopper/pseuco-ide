package jani.interaction

/**
 * Connection states as defined in the jani-interaction specification.
 *
 * @author Konstantin Kopper
 */
enum class ConnectionState {
    CONNECTED,
    AUTHENTICATING,
    INTERACTIVE,
    CLOSED,
}
