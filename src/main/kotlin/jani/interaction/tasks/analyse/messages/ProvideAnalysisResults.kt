package jani.interaction.tasks.analyse.messages

import jani.interaction.tasks.JaniTask
import jani.interaction.tasks.analyse.AnalysisDataSet

class ProvideAnalysisResults(id: Int, val results: AnalysisDataSet) : JaniTask("analysis-result", id)
