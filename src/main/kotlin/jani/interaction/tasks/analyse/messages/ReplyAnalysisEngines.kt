package jani.interaction.tasks.analyse.messages

import com.google.gson.annotations.SerializedName
import jani.interaction.basic.Identifier
import jani.interaction.basic.JaniModelFeature
import jani.interaction.basic.JaniModelType
import jani.interaction.basic.Metadata
import jani.interaction.basic.ModellingFormalism
import jani.interaction.basic.ParameterDefinition
import jani.interaction.tasks.JaniTask

class ReplyAnalysisEngines(
    id: Int,
    val engines: List<AnalysisEngine>
) : JaniTask("analysis-engines", id) {

    init {
        assert(engines.isNotEmpty())
    }

    data class AnalysisEngine(
        val id: Identifier,
        val metadata: Metadata,
        val parameters: List<ParameterDefinition>?,
        @SerializedName("model-features") val modelFeatures: List<JaniModelFeature>?,
        @SerializedName("model-types") val modelTypes: List<JaniModelType>?,
        @SerializedName("modelling-formalisms") val modellingFormalisms: List<ModellingFormalism>?
    )
}
