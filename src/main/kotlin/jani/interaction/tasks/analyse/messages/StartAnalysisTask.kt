package jani.interaction.tasks.analyse.messages

import com.google.gson.annotations.SerializedName
import jani.interaction.basic.AnyModel
import jani.interaction.basic.Identifier
import jani.interaction.basic.ParameterValue
import jani.interaction.tasks.JaniTask

class StartAnalysisTask(
    id: Int,
    val engine: Identifier,
    @SerializedName("modelling-formalism") val modellingFormalism: Identifier?,
    val model: AnyModel,
    val properties: List<Identifier>?,
    val parameters: List<ParameterValue>,
    val experiments: List<Experiment>?
) : JaniTask("analysis-start", id) {

    constructor(id: Int, engine: Identifier, model: AnyModel, parameters: List<ParameterValue>) :
            this(id, engine, null, model, null, parameters, null)

    constructor(id: Int, engine: Identifier, modellingFormalism: Identifier?, model: AnyModel, parameters: List<ParameterValue>) :
            this(id, engine, modellingFormalism, model, null, parameters, null)

    constructor(id: Int, engine: String, modellingFormalism: String?, model: AnyModel, parameters: List<ParameterValue>) :
            this(id, Identifier(engine),
                    if (modellingFormalism != null) Identifier(modellingFormalism) else null,
                    model, parameters)

    init {
        assert((model is AnyModel.JaniModel && modellingFormalism == null) ||
                (model is AnyModel.Textual && modellingFormalism != null))
    }

    data class Experiment(
        val experiment: Identifier,
        val values: List<Value>
    ) {
        data class Value(
            val name: Identifier,
            val value: Value
        ) {
            sealed class Value {
                object True : Value()
                object False : Value()
                data class Number(val n: Int) : Value()
            }
        }
    }
}
