package jani.interaction.tasks.analyse

import com.google.gson.annotations.SerializedName
import jani.interaction.basic.Identifier

data class AnalysisDataSet(
    val id: Int,
    val label: String?,
    val experiment: Identifier?,
    val property: Identifier?,
    val results: List<Result>?,
    @SerializedName("data-sets") val dataSets: List<AnalysisDataSet>?

) {
    init {
        assert(id >= 1)
    }

    data class Result(
        val label: String,
        val type: Type,
        val value: Value,
        val unit: Unit?,
        @SerializedName("formatted-value") val formattedValue: String?
    ) {

        constructor(label: String, boolean: Boolean) :
                this(label, Type.Bool, if (boolean) Value.True else Value.False, null, null)

        constructor(label: String, boolean: Boolean, formattedValue: String) :
                this(label, Type.Bool, if (boolean) Value.True else Value.False, null, formattedValue)

        // TODO more constructors

        init {
            when (type) {
                is Type.Bool -> assert(value === Value.True || value === Value.False)
                is Type.Int -> assert(true) // TODO
                is Type.Rational -> assert(value is Value.Rational)
                is Type.Decimal -> assert(true) // TODO
                is Type.String -> assert(true) // TODO
            }
            when (unit) {
                is Unit.Second, Unit.Millisecond -> assert(type === Type.Int || type === Type.Rational || type === Type.Decimal)
                is Unit.Byte -> assert(type === Type.Int)
            }
        }

        sealed class Type {
            object Bool : Type()
            object Int : Type()
            object Rational : Type()
            object Decimal : Type()
            object String : Type()
        }

        sealed class Value {
            // TODO
            object True : Value()

            object False : Value()
            data class Rational(val num: Int, val den: Int) : Value() {
                init {
                    assert(den >= 0)
                }
            }
        }

        sealed class Unit {
            object Second : Unit()
            object Millisecond : Unit()
            object Byte : Unit()
        }
    }
}
