package jani.interaction.tasks

class ProvideTaskStatus(id: Int, val status: String) : JaniTask("task-status", id)
