package jani.interaction.tasks

import jani.interaction.basic.messages.JaniMessage

abstract class JaniTask protected constructor(type: String, val id: Int) : JaniMessage(type) {

    init {
        assert(id >= 1)
    }
}
