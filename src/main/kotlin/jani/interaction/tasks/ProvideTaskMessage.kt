package jani.interaction.tasks

import com.google.gson.annotations.SerializedName

class ProvideTaskMessage(id: Int, val severity: Severity, val message: String) : JaniTask("task-message", id) {

    enum class Severity {
        @SerializedName("info")
        INFO,

        @SerializedName("warning")
        WARNING,

        @SerializedName("error")
        ERROR,
    }
}
