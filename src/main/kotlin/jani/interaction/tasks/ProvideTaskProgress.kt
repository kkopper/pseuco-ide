package jani.interaction.tasks

class ProvideTaskProgress(id: Int, val progress: Double) : JaniTask("task-progress", id) {

    init {
        assert(progress >= 0.0)
        assert(progress <= 1.0)
    }
}
