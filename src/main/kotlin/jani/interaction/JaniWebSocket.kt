package jani.interaction

import jani.JaniJSONAdapter
import jani.interaction.basic.messages.Capabilities
import jani.interaction.basic.messages.Close
import jani.interaction.basic.messages.JaniMessage
import jani.interaction.basic.messages.ReplyUpdateServerParameters
import jani.interaction.tasks.ProvideTaskMessage
import jani.interaction.tasks.ProvideTaskProgress
import jani.interaction.tasks.ProvideTaskStatus
import jani.interaction.tasks.TaskEnded
import jani.interaction.tasks.analyse.messages.ProvideAnalysisResults
import jani.interaction.tasks.analyse.messages.ReplyAnalysisEngines
import org.http4k.websocket.Websocket
import org.http4k.websocket.WsMessage

/**
 * WebSockets client adapted for JANI messages.
 * Automatically parses incoming messages provides handlers for each message type.
 * If not specified otherwise, the default handlers do nothing.
 *
 * @author Konstantin Kopper
 * @since 2.0.0
 */
class JaniWebSocket(socket: Websocket) : Websocket by socket {

    /**
     * Handler taking an arbitrary [JaniMessage]. Always replies with a [Close] message.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    private var replyMessageWithClose: (JaniMessage) -> Unit = { _ -> janiClose("Unsupported message type.") }

    // TODO replace empty functions with the above default?

    /**
     * Handler called when a [Capabilities] message is received.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    private var onCapabilities: (Capabilities) -> Unit = {}

    /**
     * Handler called when a [Close] message is received.
     * Default behaviour: closes the socket.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    private var onJaniClose: (Close) -> Unit = { _ -> close() }

    /**
     * Handler called when a [ReplyUpdateServerParameters] message is received.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    private var onReplyUpdateServerParameters: (ReplyUpdateServerParameters) -> Unit = {}

    /**
     * Handler called when a [ProvideTaskStatus] message is received.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    private var onProvideTaskStatus: (ProvideTaskStatus) -> Unit = {}

    /**
     * Handler called when a [ProvideTaskProgress] message is received.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    private var onProvideTaskProgress: (ProvideTaskProgress) -> Unit = {}

    /**
     * Handler called when a [ProvideTaskMessage] message is received.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    private var onProvideTaskMessage: (ProvideTaskMessage) -> Unit = {}

    /**
     * Handler called when a [TaskEnded] message is received.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    private var onTaskEnded: (TaskEnded) -> Unit = {}

    /**
     * Handler called when a [ReplyAnalysisEngines] message is received.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    private var onReplyAnalysisEngines: (ReplyAnalysisEngines) -> Unit = {}

    /**
     * Handler called when a [ProvideAnalysisResults] message is received.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    private var onProvideAnalysisResults: (ProvideAnalysisResults) -> Unit = {}

    init {
        onError { it.printStackTrace() }

        onMessage {
            val msg = JaniJSONAdapter.deserializeMessage(it.bodyString())
            when (msg) {
                is Capabilities -> onCapabilities(msg)
                is Close -> onJaniClose(msg)
                is ReplyUpdateServerParameters -> onReplyUpdateServerParameters(msg)
                is ProvideTaskStatus -> onProvideTaskStatus(msg)
                is ProvideTaskProgress -> onProvideTaskProgress(msg)
                is ProvideTaskMessage -> onProvideTaskMessage(msg)
                is TaskEnded -> onTaskEnded(msg)
                is ReplyAnalysisEngines -> onReplyAnalysisEngines(msg)
                is ProvideAnalysisResults -> onProvideAnalysisResults(msg)
                else -> replyMessageWithClose(msg)
            }
        }
    }

    /**
     * Sends [msg] encoded as JSON over this socket.
     *
     * @author Konstantin Kopper
     * @since
     * @see JaniJSONAdapter.serialize
     */
    fun send(msg: JaniMessage) = send(WsMessage(JaniJSONAdapter.serialize(msg)))

    /**
     * Sends a [Close] message with [msg] as body.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     * @see send
     */
    fun janiClose(msg: String? = null) = send(Close(msg))

    /**
     * Specifies [fn] to be executed when a [Capabilities] message is received.
     * Sets [onCapabilities] to [fn].
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    fun onCapabilities(fn: (Capabilities) -> Unit) {
        onCapabilities = fn
    }

    /**
     * Specifies [fn] to be executed when a [Close] message is received.
     * Sets [onJaniClose] to [fn].
     *
     * @author Konstantin Kopper
     *
     */
    fun onJaniClose(fn: (Close) -> Unit) {
        onJaniClose = fn
    }

    /**
     * Specifies [fn] to be executed when a [ReplyUpdateServerParameters] message is received.
     * Sets [onReplyUpdateServerParameters] to [fn].
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    fun onReplyUpdateServerParameters(fn: (ReplyUpdateServerParameters) -> Unit) {
        onReplyUpdateServerParameters = fn
    }

    /**
     * Specifies [fn] to be executed when a [ProvideTaskStatus] message is received.
     * Sets [onProvideTaskStatus] to [fn].
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    fun onProvideTaskStatus(fn: (ProvideTaskStatus) -> Unit) {
        onProvideTaskStatus = fn
    }

    /**
     * Specifies [fn] to be executed when a [ProvideTaskProgress] message is received.
     * Sets [onProvideTaskProgress] to [fn].
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    fun onProvideTaskProgress(fn: (ProvideTaskProgress) -> Unit) {
        onProvideTaskProgress = fn
    }

    /**
     * Specifies [fn] to be executed when a [ProvideTaskMessage] message is received.
     * Sets [onProvideTaskMessage] to [fn].
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    fun onProvideTaskMessage(fn: (ProvideTaskMessage) -> Unit) {
        onProvideTaskMessage = fn
    }

    /**
     * Specifies [fn] to be executed when a [TaskEnded] message is received.
     * Sets [onTaskEnded] to [fn].
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    fun onTaskEnded(fn: (TaskEnded) -> Unit) {
        onTaskEnded = fn
    }

    /**
     * Specifies [fn] to be executed when a [ReplyAnalysisEngines] message is received.
     * Sets [onReplyAnalysisEngines] to [fn].
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    fun onReplyAnalysisEngines(fn: (ReplyAnalysisEngines) -> Unit) {
        onReplyAnalysisEngines = fn
    }

    /**
     * Specifies [fn] to be executed when a [ProvideAnalysisResults] message is received.
     * Sets [onProvideAnalysisResults] to [fn].
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    fun onProvideAnalysisResults(fn: (ProvideAnalysisResults) -> Unit) {
        onProvideAnalysisResults = fn
    }
}
