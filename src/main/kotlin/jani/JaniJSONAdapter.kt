package jani

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonObject
import com.google.gson.JsonParseException
import com.google.gson.JsonParser
import com.google.gson.JsonSyntaxException
import com.google.gson.TypeAdapter
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonToken
import com.google.gson.stream.JsonWriter
import jani.interaction.basic.Extension
import jani.interaction.basic.ModellingFormalism
import jani.interaction.basic.ParameterDefinition
import jani.interaction.basic.ParameterType
import jani.interaction.basic.ParameterValue
import jani.interaction.basic.messages.Authenticate
import jani.interaction.basic.messages.Capabilities
import jani.interaction.basic.messages.Close
import jani.interaction.basic.messages.JaniMessage
import jani.interaction.basic.messages.ReplyUpdateServerParameters
import jani.interaction.basic.messages.RequestUpdateServerParameters
import jani.interaction.tasks.ProvideTaskMessage
import jani.interaction.tasks.ProvideTaskProgress
import jani.interaction.tasks.ProvideTaskStatus
import jani.interaction.tasks.StopTask
import jani.interaction.tasks.TaskEnded
import jani.interaction.tasks.analyse.messages.ProvideAnalysisResults
import jani.interaction.tasks.analyse.messages.QueryAnalysisEngines
import jani.interaction.tasks.analyse.messages.ReplyAnalysisEngines
import jani.interaction.tasks.analyse.messages.StartAnalysisTask
import jani.model.Identifier
import jani.model.models.ModelFeature

/**
 * Provides methods to convert all JANI related objects to JSON and back.
 * Uses specific [TypeAdapter]s.
 *
 * @author Konstantin Kopper
 * @since 2.0.0
 */
object JaniJSONAdapter {

    /**
     * Create a JSON string from any object using [jsonBuilder],
     * hence all type adapters are used.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    fun serialize(obj: Any): String = jsonBuilder.toJson(obj)

    /**
     * Parse a JSON encoded type using [jsonBuilder],
     * hence all type adapters are used.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    fun <T> deserialize(text: String, type: java.lang.reflect.Type): T = jsonBuilder.fromJson(text, type)

    /**
     * Parse a JSON encoded [JaniMessage] to a concrete object.
     * The resulting object has the actual type of the message,
     * such that distinction by type information is possible.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     * @throws JsonParseException The given JSON does not describe a [JaniMessage].
     */
    fun deserializeMessage(text: String): JaniMessage = JsonParser.parseString(text).let {
        assert(it.isJsonObject)
        jsonBuilder.fromJson(it, when (it.asJsonObject["type"]?.asString) {
            // Basic messages
            null -> Authenticate::class.java
            "capabilities" -> Capabilities::class.java
            "close" -> Close::class.java
            "server-parameters" -> if ("parameters" in it.asJsonObject)
                RequestUpdateServerParameters::class.java
            else
                ReplyUpdateServerParameters::class.java
            // Tasks
            "task-end" -> TaskEnded::class.java
            "task-message" -> ProvideTaskMessage::class.java
            "task-progress" -> ProvideTaskProgress::class.java
            "task-status" -> ProvideTaskStatus::class.java
            "task-stop" -> StopTask::class.java
            // Analyse tasks
            "analysis-engines" -> if ("engines" in it.asJsonObject)
                ReplyAnalysisEngines::class.java
            else
                QueryAnalysisEngines::class.java
            "analysis-start" -> StartAnalysisTask::class.java
            "analysis-result" -> ProvideAnalysisResults::class.java
            // Unknown (or unsupported)
            else -> throw JsonParseException("Unknown message type.")
        })
    }

    private val jsonBuilder: Gson = GsonBuilder()
        .registerTypeHierarchyAdapter(Extension::class.java, ExtensionTypeAdapter)
        .registerTypeAdapter(Identifier::class.java, IdentifierTypeAdapter)
        .registerTypeHierarchyAdapter(ModelFeature::class.java, ModelFeatureTypeAdapter)
        .registerTypeHierarchyAdapter(ModellingFormalism::class.java, ModellingFormalismTypeAdapter)
        .registerTypeHierarchyAdapter(ParameterType::class.java, ParameterTypeTypeAdapter)
        .registerTypeAdapter(ParameterValue.Values::class.java, ParameterValuesTypeAdapter)
        .registerTypeAdapter(ParameterDefinition.DefaultValue::class.java, ParameterDefaultValuesTypeAdapter)
        .create()

    private object ExtensionTypeAdapter : TypeAdapter<Extension>() {
        override fun write(out: JsonWriter, value: Extension?) {
            when (value) {
                null -> out.nullValue()
                is Extension.PersistentState -> out.value("persistent-state")
            }
        }

        override fun read(`in`: JsonReader): Extension? = when (`in`.nextString()) {
            "persistent-state" -> Extension.PersistentState
            else -> throw JsonSyntaxException("Unexpected extension.")
        }
    }

    private object ModelFeatureTypeAdapter : TypeAdapter<ModelFeature>() {
        override fun write(out: JsonWriter, value: ModelFeature?) {
            when (value) {
                null -> out.nullValue()
                else -> out.value(value.asString)
            }
        }

        override fun read(`in`: JsonReader): ModelFeature? =
            if (!`in`.hasNext()) null else when (val str = `in`.nextString()) {
                ModelFeature.Arrays.asString -> ModelFeature.Arrays
                ModelFeature.Datatypes.asString -> ModelFeature.Datatypes
                ModelFeature.DerivedOperators.asString -> ModelFeature.DerivedOperators
                ModelFeature.EdgePriorities.asString -> ModelFeature.EdgePriorities
                ModelFeature.Functions.asString -> ModelFeature.Functions
                ModelFeature.HyperbolicFunctions.asString -> ModelFeature.HyperbolicFunctions
                ModelFeature.NamedExpressions.asString -> ModelFeature.NamedExpressions
                ModelFeature.NonDetSelection.asString -> ModelFeature.NonDetSelection
                ModelFeature.TradeoffProperties.asString -> ModelFeature.TradeoffProperties
                ModelFeature.TrigonometricFunctions.asString -> ModelFeature.TrigonometricFunctions
                else -> try {
                    ModelFeature.new(str)
                } catch (e: IllegalArgumentException) {
                    throw JsonParseException("Creating a custom model feature failed.", e)
                }
            }
    }

    private object ModellingFormalismTypeAdapter : TypeAdapter<ModellingFormalism>() {
        override fun write(out: JsonWriter, value: ModellingFormalism?) {
            when (value) {
                null -> out.nullValue()
                else -> out.value(value.asString)
            }
        }

        override fun read(`in`: JsonReader): ModellingFormalism? =
            if (!`in`.hasNext()) null else when (val str = `in`.nextString()) {
                ModellingFormalism.IOSA.asString -> ModellingFormalism.IOSA
                ModellingFormalism.Modest.asString -> ModellingFormalism.Modest
                ModellingFormalism.pGCL.asString -> ModellingFormalism.pGCL
                ModellingFormalism.PRISM.asString -> ModellingFormalism.PRISM
                ModellingFormalism.xSADF.asString -> ModellingFormalism.xSADF
                else -> try {
                    ModellingFormalism.new(str)
                } catch (e: IllegalArgumentException) {
                    throw JsonParseException("Creating a custom modelling formalism failed.", e)
                }
            }
    }

    private object IdentifierTypeAdapter : TypeAdapter<Identifier>() {
        override fun write(out: JsonWriter, value: Identifier?) {
            when (value) {
                null -> out.nullValue()
                else -> out.value(value.plainString)
            }
        }

        override fun read(`in`: JsonReader): Identifier? =
            if (!`in`.hasNext()) null else Identifier(`in`.nextString())
    }

    private object ParameterTypeTypeAdapter : TypeAdapter<ParameterType>() {
        override fun write(out: JsonWriter, value: ParameterType?) {
            when (value) {
                null -> out.nullValue()
                ParameterType.Bool -> out.value("bool")
                ParameterType.Int -> out.value("int")
                ParameterType.Real -> out.value("real")
                ParameterType.String -> out.value("string")
                is ParameterType.Enum -> {
                    out.beginArray()
                    value.values.forEach { out.value(it) }
                    out.endArray()
                }
                is ParameterType.Option -> {
                    // TODO use default serialization
                    out.beginObject()
                    out.name("kind")
                    out.value("option")
                    out.name("type")
                    out.jsonValue(toJson(value.type))
                    out.endObject()
                }
            }
        }

        override fun read(`in`: JsonReader): ParameterType? = if (!`in`.hasNext()) null else when (`in`.peek()) {
            JsonToken.BEGIN_ARRAY -> {
                `in`.beginArray()
                val list = mutableListOf<String>()
                while (`in`.peek() != JsonToken.END_ARRAY)
                    list += `in`.nextString()
                `in`.endArray()
                ParameterType.Enum(list)
            }
            JsonToken.BEGIN_OBJECT -> {
                `in`.beginObject()
                var hasKind = false
                var type: ParameterType? = null
                while (`in`.peek() != JsonToken.END_OBJECT)
                    when (`in`.nextName()) {
                        "kind" -> {
                            hasKind = true
                            if (`in`.nextString() != "option")
                                throw JsonSyntaxException("Expected 'option' for key 'kind'.")
                        }
                        "type" -> {
                            type = read(`in`)
                        }
                        else -> throw JsonSyntaxException("Unknown key during object parsing.")
                    }
                `in`.endObject()
                if (!hasKind)
                    throw JsonSyntaxException("Missing 'kind' attribute.")
                if (type == null)
                    throw JsonSyntaxException("Missing 'type' attribute.")
                ParameterType.Option(type)
            }
            JsonToken.STRING -> when (`in`.nextString()) {
                "bool" -> ParameterType.Bool
                "int" -> ParameterType.Int
                "real" -> ParameterType.Real
                "string" -> ParameterType.String
                else -> throw JsonSyntaxException("Unexpected string representation for parameter types.")
            }
            else -> null
        }
    }

    private object ParameterValuesTypeAdapter : TypeAdapter<ParameterValue.Values>() {
        override fun write(out: JsonWriter, value: ParameterValue.Values?) {
            when (value) {
                null -> out.nullValue()
                is ParameterValue.Values.True -> out.value(true)
                is ParameterValue.Values.False -> out.value(false)
                is ParameterValue.Values.Number -> out.value(value.number)
                is ParameterValue.Values.String -> out.value(value.string)
            }
        }

        override fun read(`in`: JsonReader): ParameterValue.Values? =
            if (!`in`.hasNext()) null else when (`in`.peek()) {
                JsonToken.BOOLEAN -> if (`in`.nextBoolean()) ParameterValue.Values.True else ParameterValue.Values.False
                JsonToken.NUMBER -> ParameterValue.Values.Number(`in`.nextInt())
                JsonToken.STRING -> ParameterValue.Values.String(`in`.nextString())
                else -> throw JsonSyntaxException("Unexpected token, expected a boolean value, a number or a string.")
            }
    }

    private object ParameterDefaultValuesTypeAdapter : TypeAdapter<ParameterDefinition.DefaultValue>() {
        override fun write(out: JsonWriter, value: ParameterDefinition.DefaultValue?) {
            when (value) {
                null -> out.nullValue()
                is ParameterDefinition.DefaultValue.Null -> out.value("null")
                is ParameterDefinition.DefaultValue.True -> out.value(true)
                is ParameterDefinition.DefaultValue.False -> out.value(false)
                is ParameterDefinition.DefaultValue.Number -> when (value) {
                    is ParameterDefinition.DefaultValue.Number.Integer -> out.value(value.n)
                    is ParameterDefinition.DefaultValue.Number.Real -> out.value(value.n)
                }
            }
        }

        override fun read(`in`: JsonReader): ParameterDefinition.DefaultValue? {
            TODO("not implemented") // To change body of created functions use File | Settings | File Templates.
        }
    }

    /**
     * Utility method. Allows to use the `in` operator of Kotlin on [JsonObject]s.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    private operator fun JsonObject.contains(key: String): Boolean = this.has(key)
}
