package jani.model.models

import jani.model.Identifier

sealed class ModelFeature(val name: Identifier) {

    constructor(name: String) : this(Identifier(name))

    val asString
        get() = name.plainString

    object Arrays : ModelFeature("arrays")
    object Datatypes : ModelFeature("datatypes")
    object DerivedOperators : ModelFeature("derived-operators")
    object EdgePriorities : ModelFeature("edge-priorities")
    object Functions : ModelFeature("functions")
    object HyperbolicFunctions : ModelFeature("hyperbolic-functions")
    object NamedExpressions : ModelFeature("named-expressions")
    object NonDetSelection : ModelFeature("nondet-selection")
    object TradeoffProperties : ModelFeature("tradeoff-properties")
    object TrigonometricFunctions : ModelFeature("trigonometric-functions")

    override fun equals(other: Any?): Boolean = other != null && other is ModelFeature && name == other.name

    override fun hashCode(): Int = name.hashCode()

    private class ModelFeatureImpl(name: String) : ModelFeature(name) {
        init {
            require(name.startsWith("x-"), { "The name of custom model features must start with 'x-'." })
        }
    }

    companion object {
        fun new(name: String): ModelFeature = ModelFeatureImpl(name)
    }
}
