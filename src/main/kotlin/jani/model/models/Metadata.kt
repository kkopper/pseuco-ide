package jani.model.models

data class Metadata(
    val version: String?,
    val author: String?,
    val description: String?,
    val doi: String?,
    val url: String?
)
