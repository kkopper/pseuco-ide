package jani.model.models

import com.google.gson.annotations.SerializedName

enum class ModelType {

    /**
     * A labelled transition system (or Kripke structure or finite state automaton) (untimed).
     */
    @SerializedName("lts")
    LTS,

    /**
     * A discrete-time Markov chain (untimed)
     */
    @SerializedName("dtmc")
    DTMC,

    /**
     * A continuous-time Markov chain (timed)
     */
    @SerializedName("ctmc")
    CTMC,

    /**
     * A discrete-time Markov decision process (untimed)
     */
    @SerializedName("mdp")
    MDP,

    /**
     * A continuous-time Markov decision process (timed)
     */
    @SerializedName("ctmdp")
    CTMDP,

    /**
     * A Markov automaton (timed)
     */
    @SerializedName("ma")
    MA,

    /**
     * A timed automaton (timed)
     */
    @SerializedName("ta")
    TA,

    /**
     * A probabilistic timed automaton (timed)
     */
    @SerializedName("pta")
    PTA,

    /**
     * A stochastic timed automaton (timed)
     */
    @SerializedName("sta")
    STA,

    /**
     * A hybrid automaton (timed)
     */
    @SerializedName("ha")
    HA,

    /**
     * A probabilistic hybrid automaton (timed)
     */
    @SerializedName("pha")
    PHA,

    /**
     * A stochastic hybrid automaton (timed)
     */
    @SerializedName("sha")
    SHA,
}
