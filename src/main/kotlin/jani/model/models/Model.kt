package jani.model.models

import com.google.gson.annotations.SerializedName
import jani.model.Identifier

data class Model(
    @SerializedName("jani-version") val version: Int,
    val name: String,
    val metadata: Metadata?,
    val type: ModelType,
    val features: List<ModelFeature>?,
    val actions: List<Action>?

        // TODO more properties
) {

    data class Action(
        val name: Identifier,
        val comment: String?
    ) {
        constructor(name: Identifier) : this(name, null)
        constructor(name: String) : this(Identifier(name), null)
        constructor(name: String, comment: String?) : this(Identifier(name), comment)
    }
}
