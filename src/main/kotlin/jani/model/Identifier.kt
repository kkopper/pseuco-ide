package jani.model

data class Identifier(private val identifier: String) {

    init {
        assert(identifier.matches(Regex("[^#].*$")))
    }

    val plainString: String
        get() = identifier
}
