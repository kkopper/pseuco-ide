package properties

import fxGui.Main
import java.io.IOException
import java.util.Properties

/**
 * The `build.properties` resource file.
 *
 * @author Konstantin Kopper
 * @since 2.1.0
 */
object BuildProperties : Properties() {

    /**
     * The branch (or tag) this build is based on.
     *
     * @author Konstantin Kopper
     * @since 2.1.0
     */
    val branch: String by this

    /**
     * The commit this build is based on on.
     *
     * @author Konstantin Kopper
     * @since 2.1.0
     */
    val commit: String by this

    /**
     * The pipeline ID from GitLab if built by the CI server, empty otherwise.
     *
     * @author Konstantin Kopper
     * @since 2.1.0
     */
    val pipeline: String by this

    init {
        try {
            load(this::class.java.getResourceAsStream("/build.properties"))
        } catch (e: IOException) {
            System.err.println("Reading build properties failed.")
            if (Main.DEBUG)
                e.printStackTrace(Main.STANDARD_ERR)
            this["branch"] = ""
            this["commit"] = ""
            this["pipeline"] = ""
        }
    }
}
