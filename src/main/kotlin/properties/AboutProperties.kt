package properties

import fxGui.Main
import java.io.IOException
import java.util.Properties

/**
 * The `about.properties` resource file.
 *
 * @author Konstantin Kopper
 * @since 2.1.0
 */
object AboutProperties : Properties() {

    /**
     * The version of the IDE as set in the build script.
     *
     * @author Kontantin Kopper
     * @since 2.1.0
     */
    val version: String by this

    /**
     * The description of the IDE as set in the build script.
     *
     * @author Konstantin Kopper
     * @since 2.1.0
     */
    val description: String by this

    /**
     * The copyright notice.
     * Format: `Copyright (c) $copyright`.
     *
     * @author Konstantin Kopper
     * @since 2.1.0
     */
    val copyright: String by this

    /**
     * The name of the license of the IDE.
     * Format: `Licensed under the\n$license`.
     *
     * @author Konstantin Kopper
     * @since 2.1.0
     */
    val license: String by this

    /**
     * The link to the source code repository.
     *
     * @author Konstantin Kopper
     * @since 2.1.0
     */
    val repo: String by this

    init {
        try {
            load(this::class.java.getResourceAsStream("/about.properties"))
        } catch (e: IOException) {
            System.err.println("Could not read properties. Using some default values.")
            if (Main.DEBUG)
                e.printStackTrace(Main.STANDARD_ERR)
            this["version"] = "undefined"
            this["description"] = "pseuCo IDE"
            this["copyright"] = "undefined"
            this["license"] = "undefined"
            this["repo"] = ""
        }
    }
}
