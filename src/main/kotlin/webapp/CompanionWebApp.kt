package webapp

import com.pseuco.PseuCoFileSharer
import com.pseuco.websocket.PseuCoWebSocket
import config.Config
import freemarker.cache.ClassTemplateLoader
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.freemarker.FreeMarker
import io.ktor.freemarker.FreeMarkerContent
import io.ktor.http.HttpStatusCode
import io.ktor.http.content.default
import io.ktor.http.content.defaultResource
import io.ktor.http.content.resources
import io.ktor.http.content.static
import io.ktor.http.content.staticBasePackage
import io.ktor.request.receiveParameters
import io.ktor.response.respond
import io.ktor.response.respondRedirect
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import kotlinx.coroutines.launch
import java.awt.Desktop
import java.net.URI
import kotlin.system.exitProcess

/**
 * The web application accompanying the IDE.
 * Hosts static help pages and provides a dynamic page to change the settings of the IDE.
 *
 * @author Konstantin Kopper
 * @since 2.0.0
 */
object CompanionWebApp {

    /**
     * The port [server] is listening on.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    private const val port = 37562

    /**
     * Flag indicating whether the settings were changed.
     * Used to decide if a confirmation message should be displayed after reloading the settings page.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    private var settingsChanged = false

    /**
     * The actual server listening on [port].
     * Accepts connections at the following paths:
     *  * `/settings`: GET and POST requests, to either show or update the settings
     *  * `/help`: Static help pages.
     *  * `/css`, `/js`, `/webfonts`: Utility files.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    private val server = embeddedServer(Netty, port) {
        install(FreeMarker) {
            templateLoader = ClassTemplateLoader(CompanionWebApp::class.java.classLoader, "webapp/templates")
        }

        routing {
            get("/settings") {
                val currentConfig = Config.let {
                    mutableMapOf(
                        "workspace" to it.workspace,
                        "use_websocket" to it.useWebSocket,
                        "enforce_api" to PseuCoFileSharer.enforceAPI,
                        "skip_jdk_warning" to it.skipJdkWarning,
                        "accept_sharing_agreement" to it.acceptSharingAgreement,
                        "accept_analysis_agreement" to it.acceptAnalysisAgreement
                    )
                }
                if (settingsChanged) {
                    settingsChanged = false
                    currentConfig["confirmation"] = true
                }
                call.respond(FreeMarkerContent("settings.ftl", currentConfig, "e"))
            }

            post("/settings") {
                call.receiveParameters().also { params ->
                    params["sharingMechanism"]?.also {
                        (it == "ws").also {
                            Config.useWebSocket = it
                            if (it) PseuCoFileSharer.useServer(PseuCoWebSocket) else PseuCoFileSharer.useAPI()
                        }
                    }
                    params["jdkWarning"].also { Config.skipJdkWarning = it == null || it == "off" }
                    params["sharingAgreement"].also { Config.acceptSharingAgreement = it == "on" }
                    params["analysisAgreement"].also { Config.acceptAnalysisAgreement = it == "on" }
                    Config.save()
                }
                settingsChanged = true
                call.respondRedirect("/settings")
            }

            get("/reset") { call.respondRedirect("/settings", true) }

            post("/reset") {
                call.receiveParameters().also { params ->
                    if ("resetConfirm" !in params) {
                        call.respondRedirect("/reset")
                        return@post
                    }
                }
                Config.file.deleteOnExit()
                launch {
                    // Wait for response to be sent and kill IDE afterwards.
                    Thread.sleep(250)
                    exitProcess(0)
                }
                call.respond(HttpStatusCode.OK, "Success. Please close this tab and restart the IDE.")
            }

            static {
                staticBasePackage = "webapp"
                static("css") { resources("css") }
                static("js") { resources("js") }
                static("webfonts") { resources("fonts") }
                // Static help pages
                static("help") {
                    staticBasePackage = "webapp/html/help"
                    static("sharing") { defaultResource("sharing.html") }
                    defaultResource("help.html")
                }
                // Main page
                defaultResource("html/main.html")
                // Configuration file
                static("config") { default(Config.file) }
            }
        }
    }

    /**
     * Starts [server]. Blocking if [wait] is set.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    fun start(wait: Boolean = false) {
        server.start(wait)
    }

    /**
     * Stops [server] immediately.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    fun stop() = server.stop(0, 0)

    /**
     * The [URI] this server is reachable at.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    val uri: URI = URI.create("http://localhost:$port")

    /**
     * Opens this web application in the local default browser.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    fun open() = Desktop.getDesktop().browse(uri)

    /**
     * Flag indicating if the web application is up and running.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    var isActive = true
}
