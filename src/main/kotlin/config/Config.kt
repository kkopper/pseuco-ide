package config

import util.FileUtilities
import java.io.File
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty
import kotlin.reflect.full.createType

/**
 * Class encapsulating a configuration file (encoding key-value pairs in YAML format).
 * After loading the underlying file, properties can be read using the [get] operator
 * or updated using the [set] operator.
 *
 * @author Konstantin Kopper
 * @since 2.0.0
 * @property file The encapsulated config file.
 * @constructor Creates an instance encapsulating the given file.
 */
open class Config(val file: File) {

    /**
     * Internal property mapping.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    private val properties: HashMap<String, String> = HashMap()

    init {
        load()
    }

    /**
     * Default configuration.
     * Uses `config.yml` in the local application data folder as returned by [FileUtilities].
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    companion object : Config(FileUtilities.getAppDataFile("config.yml")) {
        /**
         * The workspace used by the IDE.
         *
         * @author Konstantin Kopper
         * @since 2.0.0
         */
        var workspace: String by super.properties

        /**
         * Flag indicating if WebSockets should be used to interact with [pseuCo.com](https://pseuco.com).
         *
         * @author Konstantin Kopper
         * @since 2.0.0
         */
        var useWebSocket: Boolean by map("use-websocket")

        /**
         * Flag indicating if the warning of a missing JDK should be skipped.
         *
         * @author Konstantin Kopper
         * @since 2.0.0
         */
        var skipJdkWarning: Boolean by map("skip-jdk-warning")

        /**
         * Flag indicating if the sharing agreement was accepted.
         *
         * @author Konstantin Kopper
         * @since 2.0.0
         */
        var acceptSharingAgreement: Boolean by map("accept-sharing-agreement")

        /**
         * Flag indicating if the analysis agreement was accepted.
         *
         * @author Konstantin Kopper
         * @since 2.0.0
         */
        var acceptAnalysisAgreement: Boolean by map("accept-analysis-agreement")
    }

    /**
     * Loads the encapsulated config file into the internal mapping.
     * Unsaved changes will be lost.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    fun load() {
        this.properties.clear()

        if (!file.exists()) return

        file.bufferedReader().useLines {
            it.forEach { line ->
                val (key, value) = line.split(": ")
                properties[key] = value
            }
        }
    }

    /**
     * Writes the internal mapping to the config file.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    fun save() {
        if (!file.exists())
            file.createNewFile()

        file.bufferedWriter().use {
            properties.forEach { property, value ->
                it.write("$property: $value")
                it.newLine()
            }
        }
    }

    /**
     * Gets the value of a given [property] if it exists, an empty string otherwise.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    operator fun get(property: String): String = this.properties[property] ?: ""

    /**
     * Sets the [value] for a given [property].
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    operator fun set(property: String, value: String) = this.properties.set(property, value)

    /**
     * Sets the [value] for a given boolean [property].
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    operator fun set(property: String, value: Boolean) = this.properties.set(property, value.toString())

    /**
     * Checks whether a [property] is set or not.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     * @return `True` if the property is set, `false` otherwise.
     */
    operator fun contains(property: String): Boolean = this.properties.containsKey(property)

    /**
     * Creates a [ReadWriteProperty] useful to delegate properties to.
     * Gets and sets the value of [key] in [properties].
     * Inspired by [this Stack Overflow question](https://stackoverflow.com/questions/36602379/kotlin-how-to-make-a-property-delegate-by-map-with-a-custom-name).
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    protected fun <T : Config, V> map(key: String) = object : ReadWriteProperty<T, V> {
        @Suppress("unchecked_cast", "implicit_cast_to_any")
        override fun getValue(thisRef: T, property: KProperty<*>): V = thisRef[key].let {
            when (property.returnType) {
                Boolean::class.createType() -> it.toBoolean()
                else -> it
            }
        } as V

        override fun setValue(thisRef: T, property: KProperty<*>, value: V) {
            thisRef[key] = value.toString()
        }
    }
}
