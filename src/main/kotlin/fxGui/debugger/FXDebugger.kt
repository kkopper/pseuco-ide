package fxGui.debugger

import codeGen.MainCodeGen
import fxGui.debugger.variables.ReflectionBooleanProperty
import fxGui.debugger.variables.ReflectionIntegerProperty
import fxGui.debugger.variables.ReflectionStringProperty
import javafx.application.Platform
import javafx.event.EventHandler
import javafx.scene.Scene
import javafx.scene.image.Image
import javafx.stage.Stage
import javafx.stage.Window
import pseuco.javaCompiler.debugger.PseuCoDebugger
import pseuco.javaCompiler.debugger.ThreadListener
import util.ReflectionUtils
import java.io.File
import java.io.OutputStream
import java.lang.reflect.InvocationTargetException
import java.net.URLClassLoader
import java.util.concurrent.Semaphore
import java.util.concurrent.atomic.AtomicBoolean

class FXDebugger(toBeExecuted: File, out: OutputStream, err: OutputStream, owner: Window) :
    PseuCoDebugger(toBeExecuted, out, err) {

    private var stage: Stage = Stage().apply { initOwner(owner) }

    private val debuggerPane = DebuggerPane(this, stage)

    /**
     * Line mapping of the pseuCo source file.
     * Can and will be used to show the source for a given line number in a GUI environment.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    internal val pseuCoLines: Map<Int, String> by lazy {
        super.toBeExecuted.bufferedReader().useLines {
            it.mapIndexed { index, s -> index + 1 to s.trim() }.toMap()
        }
    }

    constructor(toBeExecuted: File, owner: Window) : this(toBeExecuted, System.out, System.err, owner)

    override fun executeGeneratedJavaCode() {
        val classLoader = URLClassLoader(arrayOf(this.includeDir.absoluteFile.parentFile.toURI().toURL()))

        // Disable assertions for the compiler classloader
        classLoader.clearAssertionStatus()

        // Initialize debugger class
        val debuggerClass = Class.forName(MainCodeGen.generatedJavaCodeFolder + ".Debugger", true, classLoader)

        // Collection of flags indicating for each thread if it is in debug mode.
        val threadIsDebugged = HashMap<Thread, AtomicBoolean>()
        ReflectionUtils.setPrivateProperty(debuggerClass, "threadIsDebugged", threadIsDebugged)

        // Collection of thread listeners to be informed after the thread made a step.
        val stepListeners = HashMap<Thread, ThreadListener>()
        ReflectionUtils.setPrivateProperty(debuggerClass, "stepListeners", stepListeners)

        // Collection of thread listeners to be informed after the thread terminated.
        val terminateListeners = HashMap<Thread, ThreadListener>()
        ReflectionUtils.setPrivateProperty(debuggerClass, "terminateListeners", terminateListeners)

        // Collection of semaphores used to block the thread.
        val semaphores = HashMap<Thread, Semaphore>()
        ReflectionUtils.setPrivateProperty(debuggerClass, "semaphores", semaphores)

        // Start listener to be informed when a new thread is started.
        // Creates new listeners, a new thread pane, and adds it to the debugger view.
        val startListener = ThreadListener(null) {
            val threadPane = ThreadPane(it)
            synchronized(threadIsDebugged) { threadIsDebugged[it] = threadPane.isDebugged }
            synchronized(stepListeners) { stepListeners[it] = threadPane.suspendedListener }
            synchronized(terminateListeners) { terminateListeners[it] = threadPane.terminatedListener }
            synchronized(semaphores) { semaphores[it] = threadPane.semaphore }
            debuggerPane.addThreadPane(threadPane)
        }
        ReflectionUtils.setPrivateProperty(debuggerClass, "startListener", startListener)

        ReflectionUtils.setPrivateProperty(debuggerClass, "newIntChannelListener", debuggerPane.channelsPane.newIntChannelListener)
        ReflectionUtils.setPrivateProperty(debuggerClass, "newBoolChannelListener", debuggerPane.channelsPane.newBoolChannelListener)
        ReflectionUtils.setPrivateProperty(debuggerClass, "newStringChannelListener", debuggerPane.channelsPane.newStringChannelListener)
        ReflectionUtils.setPrivateProperty(debuggerClass, "updatedIntChannelListener", debuggerPane.channelsPane.updatedIntChannelListener)
        ReflectionUtils.setPrivateProperty(debuggerClass, "updatedBoolChannelListener", debuggerPane.channelsPane.updatedBoolChannelListener)
        ReflectionUtils.setPrivateProperty(debuggerClass, "updatedStringChannelListener", debuggerPane.channelsPane.updatedStringChannelListener)

        // Execute generated Java code
        val c = Class.forName(MainCodeGen.generatedJavaCodeFolder + ".Main", true, classLoader)

        // Global variables, wrapped in reflection properties
        val fields = c.declaredFields
        val bools = fields.filter { it.type == Boolean::class.java }.map { ReflectionBooleanProperty(it, c) }
        val ints = fields.filter { it.type == Int::class.java }.map { ReflectionIntegerProperty(it, c) }
        val strings = fields.filter { it.type == String::class.java }.map { ReflectionStringProperty(it, c) }

        debuggerPane.variablesPane.also {
            it.addBooleans(bools)
            it.addIntegers(ints)
            it.addStrings(strings)
        }

        val argv = emptyArray<String>()

        val m = c.getMethod("main", argv.javaClass)

        val mainAgent = Thread(agents, {
            try {
                m.invoke(null, argv)
            } catch (e: IllegalAccessException) {
                e.printStackTrace()
            } catch (e: InvocationTargetException) {
                val inner = e.cause
                if (inner is RuntimeException && inner.cause is InterruptedException)
                    return@Thread // Thread was interrupted, so just terminate it.

                e.printStackTrace()
            }
        }, "mainAgent")

        mainAgent.contextClassLoader = classLoader

        javaClass.classLoader.setPackageAssertionStatus("include", false)

        // Show debugger view
        Platform.runLater {
            this@FXDebugger.stage.apply {
                scene = Scene(debuggerPane)
                onCloseRequest = EventHandler { this@FXDebugger.interrupt() }
                title = "pseuCo Debugger - ${toBeExecuted.name}"
                icons += Image(this@FXDebugger.javaClass.getResource("/fxGui/images/icon.png").toExternalForm())
                isFullScreen = false
            }.show()
        }

        // Start mainAgent
        startListener.call(mainAgent)
        mainAgent.start()
        mainAgent.join()
        debuggerPane.threads.first { it.thread == mainAgent }.also { it.terminatedListener.call(it.thread) }
    }

    override fun interrupt() {
        super.interrupt()
        stage.close()
    }
}
