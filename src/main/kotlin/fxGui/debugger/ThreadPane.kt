package fxGui.debugger

import fxGui.FontAwesome
import javafx.application.Platform
import javafx.event.ActionEvent
import javafx.fxml.FXML
import javafx.fxml.FXMLLoader
import javafx.fxml.Initializable
import javafx.scene.control.Button
import javafx.scene.control.Label
import javafx.scene.control.Tooltip
import javafx.scene.layout.AnchorPane
import pseuco.javaCompiler.debugger.ThreadListener
import util.CompilerUtilities
import java.net.URL
import java.util.ResourceBundle
import java.util.concurrent.Semaphore
import java.util.concurrent.atomic.AtomicBoolean
import kotlin.reflect.KProperty

/**
 * Controller for single thread views in the debugger scene.
 *
 * @author Konstantin Kopper
 * @since 2.0.0
 * @see Thread
 * @see DebuggerPane
 * @property t The [Thread] represented by this view.
 * @constructor Creates a new pane by loading the corresponding FXML file.
 */
internal class ThreadPane(private val t: Thread) : AnchorPane(), Initializable {

    /**
     * The label which holds the circle indicating the state of [t].
     * Possible states:
     *  * [t] is running: Green circle.
     *  * [t] is suspended: Orange circle.
     *  * [t] is terminated: Red circle.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    @FXML
    private lateinit var statusLabel: Label

    /**
     * Label holding the name of [t].
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    @FXML
    private lateinit var agentLabel: Label

    /**
     * Label holding the pseuCo source [t] is currently suspended at. Holds nothing if [t] is not suspended.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    @FXML
    private lateinit var lineLabel: Label

    /**
     * The button used to execute the next pseuCo statement and then stop again. Calls [step].
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    @FXML
    private lateinit var stepButton: Button

    /**
     * The button used to continue the regular execution of [t]. Calls [run].
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    @FXML
    private lateinit var runButton: Button

    /**
     * The button suspending the execution of [t]. Calls [suspend].
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    @FXML
    private lateinit var suspendButton: Button

    /**
     * The button interrupting [t]. Calls [terminate].
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    @FXML
    private lateinit var terminateButton: Button

    init {
        FXMLLoader(javaClass.getResource("/fxGui/fxml/thread.fxml")).apply {
            setController(this@ThreadPane)
            setRoot(this@ThreadPane)
            load()
        }
    }

    /**
     * The encapsulated thread.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    val thread get() = t

    /**
     * Initializations not covered by FXML.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    override fun initialize(location: URL?, resources: ResourceBundle?) {
        statusLabel.apply {
            text = FontAwesome.FA_CIRCLE
        }

        agentLabel.apply {
            text = t.name.replace(Regex("^(\\w+):\\d+$"), "$1")
        }

        lineLabel.apply {
            text = ""
        }

        stepButton.apply {
            text = FontAwesome.FA_STEP_FORWARD
        }

        runButton.apply {
            text = FontAwesome.FA_PLAY
        }

        suspendButton.apply {
            text = FontAwesome.FA_PAUSE
        }

        terminateButton.apply {
            text = FontAwesome.FA_STOP
        }
    }

    /**
     * Called by [stepButton].
     * Makes one step in the encapsulated thread.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    @FXML
    private fun step(e: ActionEvent) {
        assert(e.source == stepButton)
        started()
        semaphore.release()
    }

    /**
     * Called by [runButton].
     * Continues the execution of the encapsulated thread.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     * @see run
     */
    @FXML
    private fun run(e: ActionEvent) {
        assert(e.source == runButton)
        run()
    }

    /**
     * Continues the execution of the encapsulated thread.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    internal fun run() {
        if (!isDebugging) return // Thread already running
        if (t.state == Thread.State.TERMINATED) return // Thread terminated, thus nor further action required.

        // TODO check on sempahore?

        started()

        isDebugging = false
        semaphore.release()
    }

    /**
     * Called by [suspendButton].
     * Suspends the encapsulated thread.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     * @see suspend
     */
    @FXML
    private fun suspend(e: ActionEvent) {
        assert(e.source == suspendButton)
        suspend()
    }

    /**
     * Suspends the encapsulated thread.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    internal fun suspend() {
        if (isDebugging) return // Already suspended
        if (t.state == Thread.State.TERMINATED) return // Thread terminated, thus no further action required.
        // TODO check if further breaks are necessary
        disableAllButtons()
        isDebugging = true
    }

    /**
     * Called by [terminateButton].
     * Terminates the encapsulated thread.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     * @see terminate
     */
    @FXML
    private fun terminate(e: ActionEvent) {
        assert(e.source == terminateButton)
        terminate()
    }

    /**
     * Terminates the encapsulated thread.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    internal fun terminate() {
        if (t.state == Thread.State.TERMINATED) return // The underlying thread already terminated, thus return.

        disableAllButtons()
        t.interrupt()
    }

    /**
     * Updates the GUI elements to indicate that the encapsulated thread is running.
     * Sets the status icon to green, and disables [stepButton] and [runButton].
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    private fun started() {
        statusLabel.style = "-fx-text-fill: -green-dark;"
        statusLabel.tooltip = Tooltip("Thread running.")

        lineLabel.text = ""

        stepButton.isDisable = true
        runButton.isDisable = true
        suspendButton.isDisable = false
        terminateButton.isDisable = false
    }

    /**
     * Listener useful to inform that the encapsulated thread pauses.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    val suspendedListener = ThreadListener(t) { Platform.runLater { suspended() } }

    /**
     * Updates the GUI elements to indicate that the encapsulated thread is suspended.
     * Sets the color of the status icon to orange, and enables all buttons except [suspendButton].
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    private fun suspended() {
        statusLabel.style = "-fx-text-fill: -orange-dark;"
        statusLabel.tooltip = Tooltip("Thread suspended.")

        var foundSimulate = false

        // Regex identifying class names in stack trace elements as subgroup.
        val r = Regex("^include\\.(\\w+)$")

        for (traceElement in t.stackTrace) {
            if (traceElement.className.matches(r)) {
                val className = traceElement.className.replace(r, "$1")
                if (className == "Simulate") {
                    foundSimulate = true
                    continue
                }
                if (foundSimulate) {
                    // Found class 'Simulate' in the previous iteration,
                    // thus this stack trace element points at the calling point,
                    // which is found in front of the next statement.
                    // Querying the next statement gives the next executed code line.
                    val pseuCoLine = CompilerUtilities.getNextMatchingLine(traceElement.lineNumber, className)
                    val line = debuggerParent.task.pseuCoLines[pseuCoLine]
                    lineLabel.text = line
                    break
                }
            }
            foundSimulate = false
        }

        stepButton.isDisable = false
        runButton.isDisable = false
        suspendButton.isDisable = true
        terminateButton.isDisable = false
    }

    /**
     * Listener useful to inform that the encapsulated thread terminated.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    val terminatedListener = ThreadListener(t) { Platform.runLater { terminated() } }

    /**
     * Updates the GUI element to indicate that the encapsulated thread terminated.
     * Sets the color of the status icon to red, and disables all buttons.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    private fun terminated() {
        statusLabel.style = "-fx-text-fill: -red-dark;"
        statusLabel.tooltip = Tooltip("Thread terminated.")

        lineLabel.text = ""

        disableAllButtons()

        debuggerParent.terminated(t)
    }

    /**
     * [Semaphore] used to block the encapsulated thread.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    val semaphore = Semaphore(0)

    /**
     * Boolean flag indicating if the encapsulated thread is in debug mode.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     * @see AtomicBoolean
     */
    val isDebugged: AtomicBoolean = AtomicBoolean(true)

    /**
     * Boolean wrapper around [isDebugged].
     * Reading or updating simply queries or modifies the boolean value stored by [isDebugged].
     *
     * Allows assigning a new value by classic assignments instead of calling a method on [isDebugged].
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     * @see isDebugged
     */
    private var isDebugging: Boolean by isDebugged

    /**
     * Disables all buttons.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    private fun disableAllButtons() {
        stepButton.isDisable = true
        runButton.isDisable = true
        suspendButton.isDisable = true
        terminateButton.isDisable = true
    }

    /**
     * The [DebuggerPane] this pane is enclosed in.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     * @see DebuggerPane
     */
    private val debuggerParent: DebuggerPane
        get() = (this.parent.parent.parent.parent.parent.parent.parent) as DebuggerPane

    private operator fun <T> AtomicBoolean.getValue(thisRef: T, property: KProperty<*>): Boolean = get()

    private operator fun <T> AtomicBoolean.setValue(thisRef: T, property: KProperty<*>, value: Boolean) = set(value)
}
