package fxGui.debugger

import fxGui.FontAwesome
import fxGui.ZoomEventHandler
import fxGui.debugger.channels.ChannelsPane
import fxGui.debugger.variables.VariablesPane
import javafx.application.Platform
import javafx.collections.ListChangeListener
import javafx.collections.ObservableList
import javafx.event.ActionEvent
import javafx.event.EventHandler
import javafx.fxml.FXML
import javafx.fxml.FXMLLoader
import javafx.scene.Scene
import javafx.scene.control.Button
import javafx.scene.control.Label
import javafx.scene.control.ScrollPane
import javafx.scene.control.ToolBar
import javafx.scene.control.Tooltip
import javafx.scene.image.Image
import javafx.scene.input.KeyEvent
import javafx.scene.layout.AnchorPane
import javafx.scene.layout.Region
import javafx.scene.layout.VBox
import javafx.stage.Stage
import javafx.stage.Window

/**
 * The main view of the pseuCo debugger.
 * Provides a list of threads and controls for each thread or for all threads combined.
 *
 * @author Konstantin Kopper
 * @since 2.0.0
 * @property task The debugger task related to this view.
 * @param owner The window holding this pane. Needed to establish relation with [channelsStage].
 * @constructor Creates a new debugger view bound to the given [task].
 */
internal class DebuggerPane(internal val task: FXDebugger, owner: Window) : AnchorPane() {

    /**
     * The toolbar located at the top of the window.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    @FXML
    private lateinit var controlBar: ToolBar

    /**
     * The button used to continue the execution of all threads. Calls [run].
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    @FXML
    private lateinit var runButton: Button

    /**
     * The button used to suspend all threads. Calls [suspend].
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    @FXML
    private lateinit var suspendButton: Button

    /**
     * The button used to interrupt all threads. Calls [terminate].
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    @FXML
    private lateinit var terminateButton: Button

    /**
     * The button opening the global variable inspector. Calls [showVariables].
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     * @see VariablesPane
     */
    @FXML
    private lateinit var variablesButton: Button

    /**
     * The button opening the channel inspector. Calls [showChannels].
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     * @see ChannelsPane
     */
    @FXML
    private lateinit var channelsButton: Button

    /**
     * The scroll pane holding all [ThreadPane]s.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    @FXML
    private lateinit var threadsScrollPane: ScrollPane

    /**
     * The box used to vertically stack all [ThreadPane]s.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    @FXML
    private lateinit var threadPanes: VBox

    @FXML
    private lateinit var agentsLabel: Label

    internal val variablesPane = VariablesPane()
    private val variablesScene = Scene(variablesPane)
    private val variablesStage = Stage().apply {
        scene = variablesScene
        title = "pseuCo Variables"
        icons += Image(this@DebuggerPane::class.java.getResource("/fxGui/images/icon.png").toExternalForm())
        onCloseRequest = EventHandler { variablesButton.isDisable = false }
        initOwner(owner)
        addEventHandler(KeyEvent.KEY_PRESSED, ZoomEventHandler)
    }.also {
        variablesPane.setStage(it)
    }

    internal val channelsPane = ChannelsPane()
    private val channelsScene = Scene(channelsPane)
    private val channelsStage = Stage().apply {
        scene = channelsScene
        title = "pseuCo Channels"
        icons += Image(this@DebuggerPane::class.java.getResource("/fxGui/images/icon.png").toExternalForm())
        onCloseRequest = EventHandler { channelsButton.isDisable = false }
        initOwner(owner)
        addEventHandler(KeyEvent.KEY_PRESSED, ZoomEventHandler)
    }

    init {
        FXMLLoader(javaClass.getResource("/fxGui/fxml/debugger.fxml")).apply {
            setRoot(this@DebuggerPane)
            setController(this@DebuggerPane)
            load()
        }

        styleProperty().bind(ZoomEventHandler.fxString)
        owner.addEventHandler(KeyEvent.KEY_PRESSED, ZoomEventHandler)

        runButton.apply {
            text = FontAwesome.FA_PLAY
        }

        suspendButton.apply {
            text = FontAwesome.FA_PAUSE
        }

        terminateButton.apply {
            text = FontAwesome.FA_STOP
        }

        variablesButton.apply {
            text = FontAwesome.FA_GLOBE
            tooltip = Tooltip("See the current bindings of global variables")
        }

        channelsButton.apply {
            text = FontAwesome.FA_HANDSHAKE_O
            tooltip = Tooltip("Inspect the content of the programs channels")
        }

        // Register listener to update width of children
        threadPanes.children.addListener(ListChangeListener { c ->
            if (c.next()) {
                c.addedSubList.forEach {
                    (it as Region).prefWidthProperty().bind(threadPanes.widthProperty())
                }
            }
        })
    }

    /**
     * Called by [runButton].
     * Continues the execution of all threads.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     * @see ThreadPane.run
     */
    @FXML
    private fun run(e: ActionEvent) {
        assert(e.source == runButton)
        agentsLabel.text = "Running all threads."
        threadPanes.children.forEach { (it as ThreadPane).run() }
    }

    /**
     * Called by [suspendButton].
     * Suspends all threads.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     * @see ThreadPane.suspend
     */
    @FXML
    private fun suspend(e: ActionEvent) {
        assert(e.source == suspendButton)
        agentsLabel.text = "Suspending all threads."
        threadPanes.children.forEach { (it as ThreadPane).suspend() }
    }

    /**
     * Called by [terminateButton].
     * Terminates all threads.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     * @see ThreadPane.terminate
     */
    @FXML
    private fun terminate(e: ActionEvent) {
        assert(e.source == terminateButton)
        agentsLabel.text = "Terminating all threads."
        threadPanes.children.forEach { (it as ThreadPane).terminate() }
    }

    /**
     * Called by [variablesButton].
     * Opens a new window allowing to inspect the global variables.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    @FXML
    private fun showVariables(e: ActionEvent) {
        assert(e.source == variablesButton)
        variablesButton.isDisable = true
        variablesStage.show()
    }

    /**
     * Called by [channelsButton].
     * Opens a new window allowing to inspect the current state of buffered channels.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    @FXML
    private fun showChannels(e: ActionEvent) {
        assert(e.source == channelsButton)
        channelsButton.isDisable = true
        channelsStage.show()
    }

    /**
     * Adds [t] to the debuggers view.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    fun addThreadPane(t: ThreadPane) {
        Platform.runLater {
            agentsLabel.text = "Starting thread '${t.thread.name}'."
            threadPanes.children += t
        }
    }

    /**
     * A list of [ThreadPane]s included in this view.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    @Suppress("UNCHECKED_CAST")
    val threads: ObservableList<ThreadPane>
        get() = threadPanes.children as ObservableList<ThreadPane>

    fun terminated(t: Thread) {
        agentsLabel.text = "Thread '${t.name}' terminated."
    }
}
