package fxGui.debugger.variables

import javafx.beans.property.ReadOnlyStringPropertyBase
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.lang.reflect.Field

/**
 * JavaFX string property using reflection to periodically check for value changes.
 *
 * @author Konstantin Kopper
 * @since 2.0.0
 * @param name See [getName].
 * @param bean See [getBean].
 * @constructor Creates a new property to watch a field.
 * @param f The field to be monitored.
 * @param instance The instance of which the field should be monitored.
 */
class ReflectionStringProperty(
    f: Field,
    instance: Class<*>,
    private val name: String = f.name,
    private val bean: Any? = null
) : ReadOnlyStringPropertyBase(), ReflectionProperty<String> {

    /**
     * Function which produces the current value of the underlying field.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    private val getter: () -> String = { f.get(instance) as String }

    /**
     * Asynchronous observer checking if the underlying value has changed. If so, all listeners are informed.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    private val observer = GlobalScope.launch { observe(::fireValueChangedEvent) }

    init {
        require(f.type == String::class.java) { "Given field does not have string type." }
        require(f in instance.fields) { "Given field not found in concrete instance." }
    }

    override fun get(): String = getter()
    override fun getName(): String = name
    override fun getBean(): Any? = bean

    override fun stopObserve() {
        observer.cancel()
    }
}
