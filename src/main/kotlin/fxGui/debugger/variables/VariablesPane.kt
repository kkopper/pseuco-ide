package fxGui.debugger.variables

import fxGui.ZoomEventHandler
import javafx.beans.binding.DoubleBinding
import javafx.beans.binding.DoubleExpression
import javafx.beans.property.SimpleStringProperty
import javafx.beans.property.StringProperty
import javafx.beans.value.ObservableNumberValue
import javafx.collections.FXCollections
import javafx.fxml.FXML
import javafx.fxml.FXMLLoader
import javafx.scene.control.TableColumn
import javafx.scene.control.TableView
import javafx.scene.layout.AnchorPane
import javafx.stage.Stage

/**
 * View holding a table to show the binding of global variables.
 *
 * @author Konstantin Kopper
 * @since 2.0.0
 */
class VariablesPane : AnchorPane() {

    /**
     * The table view.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    @FXML
    private lateinit var variableTable: TableView<GlobalVariable>

    /**
     * The column indicating the type of a [GlobalVariable].
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    @FXML
    private lateinit var typeColumn: TableColumn<GlobalVariable, String>

    /**
     * The column indicating the name of a [GlobalVariable].
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    @FXML
    private lateinit var nameColumn: TableColumn<GlobalVariable, String>

    /**
     * The column indicating the value of a [GlobalVariable].
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    @FXML
    private lateinit var valueColumn: TableColumn<GlobalVariable, String>

    /**
     * List of all [ReflectionProperty]s representing a global variable.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    private var properties: List<ReflectionProperty<*>> = emptyList()

    /**
     * List of [GlobalVariable]s as shown in the table.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    private val data = FXCollections.observableArrayList<GlobalVariable>()

    init {
        FXMLLoader(VariablesPane::class.java.getResource("/fxGui/fxml/variables.fxml")).apply {
            setRoot(this@VariablesPane)
            setController(this@VariablesPane)
            load()
        }

        typeColumn.setCellValueFactory { it.value.typeProperty }
        nameColumn.setCellValueFactory { it.value.nameProperty }
        valueColumn.setCellValueFactory { it.value.valueProperty }

        variableTable.items = data

        styleProperty().bind(ZoomEventHandler.fxString)
    }

    /**
     * Adds [list] of boolean variables to [properties] and [data] (and thus to [variableTable]).
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    fun addBooleans(list: List<ReflectionBooleanProperty>) {
        properties += list
        data += FXCollections.observableList(list.map { GlobalVariable(it) })
    }

    /**
     * Adds [list] of integer variables to [properties] and [data] (and thus to [variableTable]).
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    fun addIntegers(list: List<ReflectionIntegerProperty>) {
        properties += list
        data += FXCollections.observableList(list.map { GlobalVariable(it) })
    }

    /**
     * Adds [list] of string variables to [properties] and [data] (and thus to [variableTable]).
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    fun addStrings(list: List<ReflectionStringProperty>) {
        properties += list
        data += FXCollections.observableList(list.map { GlobalVariable(it) })
    }

    fun stopListeners() {
        properties.forEach { it.stopObserve() }
    }

    /**
     * Binds the width of [valueColumn] to the width of [stage] (minus the width of [typeColumn] and [nameColumn]).
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    fun setStage(stage: Stage) {
        operator fun DoubleExpression.minus(other: ObservableNumberValue): DoubleBinding = this.subtract(other)
        operator fun DoubleExpression.minus(other: Int): DoubleBinding = this.subtract(other)
        operator fun DoubleExpression.plus(other: Int): DoubleBinding = this.add(other)
        valueColumn.prefWidthProperty().bind(
                stage.widthProperty() - nameColumn.widthProperty() - typeColumn.widthProperty())
    }

    /**
     * Data class used to fill [variableTable].
     * Uses [StringProperty]s to represent the three columns:
     *  * [typeProperty] for [typeColumn],
     *  * [nameProperty] for [nameColumn] and
     *  * [valueProperty] for [valueColumn].
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     * @param typeProperty Property holding the type of this variable.
     * @param nameProperty Property holding the name of this variable.
     * @param valueProperty Property holding the value of this variable.
     * @constructor Creates a new class given the properties.
     */
    data class GlobalVariable(
        val typeProperty: StringProperty,
        val nameProperty: StringProperty,
        val valueProperty: StringProperty
    ) {
        /**
         * Creates a new instance bound to [property].
         *
         * @author Konstantin Kopper
         * @since 2.0.0
         */
        constructor(property: ReflectionBooleanProperty) : this(
                SimpleStringProperty("bool"),
                SimpleStringProperty(property.name),
                SimpleStringProperty().apply { bind(property.asString()) }
        )

        /**
         * Creates a new instance bound to [property].
         *
         * @author Konstantin Kopper
         * @since 2.0.0
         */
        constructor(property: ReflectionIntegerProperty) : this(
                SimpleStringProperty("int"),
                SimpleStringProperty(property.name),
                SimpleStringProperty().apply { bind(property.asString()) }
        )

        /**
         * Creates a new instance bound to [property].
         *
         * @author Konstantin Kopper
         * @since 2.0.0
         */
        constructor(property: ReflectionStringProperty) : this(
                SimpleStringProperty("string"),
                SimpleStringProperty(property.name),
                SimpleStringProperty().apply { bind(property) }
        )

        /**
         * The type of this variable. Delegates to [typeProperty].
         *
         * @author Konstantin Kopper
         * @since 2.0.0
         */
        var type: String
            get() = typeProperty.get()
            set(value) = typeProperty.set(value)

        /**
         * The name of this variable. Delegates to [nameProperty].
         *
         * @author Konstantin Kopper
         * @since 2.0.0
         */
        var name: String
            get() = nameProperty.get()
            set(value) = nameProperty.set(value)

        /**
         * The name of this variable. Delegates to [valueProperty].
         *
         * @author Konstantin Kopper
         * @since 2.0.0
         */
        var value: String
            get() = valueProperty.get()
            set(value) = valueProperty.set(value)

        /*
        private fun stringProperty() = object : ReadWriteProperty<GlobalVariable, String> {
            //@Suppress("unchecked_cast", "implicit_cast_to_any")
            override fun getValue(thisRef: GlobalVariable, property: KProperty<*>): String = when (property.name) {
                "type" -> typeProperty.get()
                "name" -> nameProperty.get()
                "value" -> valueProperty.get()
                else -> ""
            }

            override fun setValue(thisRef: GlobalVariable, property: KProperty<*>, value: String) {
                when (property.name) {

                }
                thisRef.valueProperty.set(value)
            }
        }
        */
    }
}
