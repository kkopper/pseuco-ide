package fxGui.debugger.variables

/**
 * Common interface of all reflection based properties.
 *
 * @author Konstantin Kopper
 * @since 2.0.0
 * @property T The type of the property.
 */
interface ReflectionProperty<T> {

    /**
     * Return the current value of the property.
     */
    fun get(): T

    /**
     * Periodically check if the underlying has changed. If so, [callback] is called.
     * After each check, 250 milliseconds elapse before the next check.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    fun observe(callback: () -> Unit) {
        var oldValue: T = get()
        while (!Thread.currentThread().isInterrupted) {
            val currentValue = get()
            if (oldValue != currentValue) {
                callback()
                oldValue = currentValue
            }
            try {
                Thread.sleep(250)
            } catch (e: InterruptedException) {
                break
            }
        }
    }

    /**
     * Stop the observation.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     * @see observe
     */
    fun stopObserve()
}
