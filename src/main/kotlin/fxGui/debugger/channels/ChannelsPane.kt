package fxGui.debugger.channels

import fxGui.ZoomEventHandler
import javafx.application.Platform
import javafx.fxml.FXML
import javafx.fxml.FXMLLoader
import javafx.scene.layout.AnchorPane
import javafx.scene.layout.VBox

internal class ChannelsPane : AnchorPane() {

    @FXML
    private lateinit var channelsPanes: VBox

    init {
        FXMLLoader(this::class.java.getResource("/fxGui/fxml/channels.fxml")).apply {
            setRoot(this@ChannelsPane)
            setController(this@ChannelsPane)
            load()
        }

        styleProperty().bind(ZoomEventHandler.fxString)
    }

    // Channel listeners bound to this pane.

    /**
     * Channel listener used to inform about new integer channels.
     * Adds a new channel view to the list of channels.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    internal val newIntChannelListener = ChannelListener<Int> {
        Platform.runLater { channelsPanes.children += ChannelPane(it, ChannelType.INT) }
    }

    /**
     * Channel listener used to inform about new boolean channels.
     * Adds a new channel view to the list of channels.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    internal val newBoolChannelListener = ChannelListener<Boolean> {
        Platform.runLater { channelsPanes.children += ChannelPane(it, ChannelType.BOOL) }
    }

    /**
     * Channel listener used to inform about new string channels.
     * Adds a new channel view to the list of channels.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    internal val newStringChannelListener = ChannelListener<String> {
        Platform.runLater { channelsPanes.children += ChannelPane(it, ChannelType.STRING) }
    }

    // TODO Do we really need three listeners doing the exact same thing?
    // Seems to be redundant, but as ChannelListener is bound to a type, it seems to imply further changes.

    /**
     * Channel listener used to trigger an update of the channels view.
     * Only applicable to integer channels.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    internal val updatedIntChannelListener = ChannelListener<Int> { bq ->
        channelsPanes.children.map { it as ChannelPane<*> }
                .filter { it.type == ChannelType.INT }
                .filter { it.queue == bq }
                .forEach { Platform.runLater(it::update) }
    }

    /**
     * Channel listener used to trigger an update of the channels view.
     * Only applicable to bool channels.
     *
     * @author Konstantin Kopper
     */
    internal val updatedBoolChannelListener = ChannelListener<Boolean> { bq ->
        channelsPanes.children.map { it as ChannelPane<*> }
                .filter { it.type == ChannelType.BOOL }
                .filter { it.queue == bq }
                .forEach { Platform.runLater(it::update) }
    }

    /**
     * Channel listener used to trigger an update of the channels view.
     * Only applicable to s channels.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    internal val updatedStringChannelListener = ChannelListener<String> { bq ->
        channelsPanes.children.map { it as ChannelPane<*> }
                .filter { it.type == ChannelType.STRING }
                .filter { it.queue == bq }
                .forEach { Platform.runLater(it::update) }
    }
}
