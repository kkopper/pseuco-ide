package fxGui.debugger.channels

import javafx.fxml.FXML
import javafx.fxml.FXMLLoader
import javafx.scene.control.Label
import javafx.scene.layout.AnchorPane
import javafx.scene.layout.HBox
import java.util.concurrent.BlockingQueue

internal class ChannelPane<T>(internal val queue: BlockingQueue<T>, internal val type: ChannelType) : AnchorPane() {

    /**
     * The label indicating the capacity of this channel.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    @FXML
    private lateinit var capacityLabel: Label

    /**
     * The label indicating the type of this channel.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    @FXML
    private lateinit var typeLabel: Label

    /**
     * The label indicating the name of this channel.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    @FXML
    private lateinit var nameLabel: Label

    @FXML
    private lateinit var outerChannelPane: AnchorPane

    @FXML
    private lateinit var channelPane: AnchorPane

    @FXML
    private lateinit var elements: HBox

    init {
        FXMLLoader(this::class.java.getResource("/fxGui/fxml/channel.fxml")).apply {
            setRoot(this@ChannelPane)
            setController(this@ChannelPane)
            load()
        }

        val capacity = queue.remainingCapacity()
        capacityLabel.text = capacity.toString()
        typeLabel.text = type.s

        assert(capacity > 0)
        assert(type != ChannelType.NONE)

        update()
    }

    internal fun update() {
        elements.children.clear()
        queue.takeIf { it.isNotEmpty() }?.forEach { elements.children += ChannelElement(it.toString(), this.type) }
                ?: { elements.children += ChannelElement("Channel is empty!", ChannelType.NONE) }()

        channelPane.style = if (queue.remainingCapacity() == 0) "-fx-border-width: 3px; -fx-border-color: indianred;" else ""
    }
}
