package fxGui.debugger.channels

import javafx.scene.control.Label

internal class ChannelElement(private val element: String, private val type: ChannelType) : Label(element) {

    internal constructor(element: Int, type: ChannelType) : this(element.toString(), type)

    internal constructor(element: Boolean, type: ChannelType) : this(element.toString(), type)

    init {
        stylesheets += this::class.java.getResource("/fxGui/css/general.css").toExternalForm()
        stylesheets += this::class.java.getResource("/fxGui/css/channelElement.css").toExternalForm()

        styleClass += type.s
    }
}
