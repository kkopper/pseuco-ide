package fxGui.debugger.channels

import java.util.concurrent.BlockingQueue

class ChannelListener<T>(action: (BlockingQueue<T>) -> Unit) : Comparable<BlockingQueue<T>> {

    private val actions: MutableList<(BlockingQueue<T>) -> Unit> = mutableListOf(action)

    override fun compareTo(other: BlockingQueue<T>): Int {
        invoke(other)
        return 0
    }

    operator fun plusAssign(action: (BlockingQueue<T>) -> Unit) {
        actions += action
    }

    operator fun invoke(q: BlockingQueue<T>) = actions.forEach { it(q) }
}
