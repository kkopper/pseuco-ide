package fxGui.debugger.channels

/**
 * Enumeration of all channel types.
 *
 * @author Konstantin Kopper
 * @since 2.0.0
 * @property s The type of the channel as used in `<s>chan`.
 */
internal enum class ChannelType(internal val s: String) {

    /**
     * Fake channel type.
     * Only used for easier styling of the "Channel is empty!" label.
     */
    NONE("none"),

    /**
     * An `intchan`.
     */
    INT("int"),

    /**
     * A `boolchan`.
     */
    BOOL("bool"),

    /**
     * A `stringchan`.
     */
    STRING("string")
}
