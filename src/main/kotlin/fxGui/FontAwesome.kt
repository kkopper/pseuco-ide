package fxGui

/**
 * Constants providing the unicode characters for [FontAwesome](https://fontawesome.com/v4.7.0) icons.
 * Only useful in GUI environments.
 *
 * @author Konstantin Kopper
 */
internal object FontAwesome {

    /**
     * See [fa-bug](https://fontawesome.com/v4.7.0/icon/bug/).
     *
     * @author Konstantin Kopper
     */
    const val FA_BUG = "\uf188"

    /**
     * See [fa-circle](https://fontawesome.com/v4.7.0/icon/circle/).
     *
     * @author Konstantin Kopper
     */
    const val FA_CIRCLE = "\uf111"

    /**
     * See [fa-check-circle](https://fontawesome.com/v4.7.0/icon/check-circle/).
     *
     * @author Konstantin Kopper
     */
    const val FA_CHECK_CIRCLE = "\uf058"

    /**
     * See [fa-cloud-download](https://fontawesome.com/v4.7.0/icon/cloud-download/).
     *
     * @author Konstantin Kopper
     */
    const val FA_CLOUD_DOWNLOAD = "\uf0ed"

    /**
     * See [fa-cloud-upload](https://fontawesome.com/v4.7.0/icon/cloud-upload/).
     *
     * @author Konstantin Kopper
     */
    const val FA_CLOUD_UPLOAD = "\uf0ee"

    /**
     * See [fa-code](https://fontawesome.com/v4.7.0/icon/code/).
     *
     * @author Konstantin Kopper
     */
    const val FA_CODE = "\uf121"

    /**
     * See [fa-exclamation-circle](https://fontawesome.com/v4.7.0/icon/exclamation-circle/).
     *
     * @author Konstantin Kopper
     */
    const val FA_EXCLAMATION_CIRCLE = "\uf06a"

    /**
     * See [fa-external-link](https://fontawesome.com/v4.7.0/icon/external-link).
     *
     * @author Konstantin Kopper
     */
    const val FA_EXTERNAL_LINK = "\uf08e"

    /**
     * See [fa-file-code-o](https://fontawesome.com/v4.7.0/icon/file-code-o/).
     *
     * @author Konstantin Kopper
     */
    const val FA_FILE_CODE_O = "\uf1c9"

    /**
     * See [fa-file-o](https://fontawesome.com/v4.7.0/icon/file-o/).
     *
     * @author Konstantin Kopper
     */
    const val FA_FILE_O = "\uf016"

    /**
     * See [fa-flag-checkered](https://fontawesome.com/v4.7.0/icon/flag-checkered/).
     *
     * @author Konstantin Kopper
     */
    const val FA_FLAG_CHECKERED = "\uf11e"

    /**
     * See [fa-floppy-o](https://fontawesome.com/v4.7.0/icon/floppy-o/).
     *
     * @author Konstantin Kopper
     */
    const val FA_FLOPPY_O = "\uf0c7"

    /**
     * See [fa-folder-o](https://fontawesome.com/v4.7.0/icon/folder-o/).
     *
     * @author Konstantin Kopper
     */
    const val FA_FOLDER_O = "\uf114"

    /**
     * See [fa-folder-open-o](https://fontawesome.com/v4.7.0/icon/folder-open-o/).
     *
     * @author Konstantin Kopper
     */
    const val FA_FOLDER_OPEN_O = "\uf115"

    /**
     * See [fa-gitlab](https://fontawesome.com/v4.7.0/icon/gitlab).
     *
     * @author Konstantin Kopper
     */
    const val FA_GITLAB = "\uf296"

    /**
     * See [fa-globe](https://fontawesome.com/v4.7.0/icon/globe).
     *
     * @author Konstantin Kopper
     */
    const val FA_GLOBE = "\uf0ac"

    /**
     * See [fa-handshake-o](https://fontawesome.com/v4.7.0/icon/handshake-o).
     *
     * @author Konstantin Kopper
     */
    const val FA_HANDSHAKE_O = "\uf2b5"

    /**
     * See [fa-info-circle](https://fontawesome.com/v4.7.0/icon/info-circle/).
     *
     * @author Konstantin Kopper
     */
    const val FA_INFO_CIRCLE = "\uf05a"

    /**
     * See [fa-pause](https://fontawesome.com/v4.7.0/icon/pause/).
     *
     * @author Konstantin Kopper
     */
    const val FA_PAUSE = "\uf04c"

    /**
     * See [fa-play](https://fontawesome.com/v4.7.0/icon/play/).
     *
     * @author Konstantin Kopper
     */
    const val FA_PLAY = "\uf04b"

    /**
     * See [fa-question-circle](https://fontawesome.com/v4.7.0/icon/question-circle/).
     *
     * @author Konstantin Kopper
     */
    const val FA_QUESTION_CIRCLE = "\uf059"

    /**
     * See [fa-question-circle-o](https://fontawesome.com/v4.7.0/icon/question-circle-o/).
     *
     * @author Konstantin Kopper
     */
    const val FA_QUESTION_CIRCLE_O = "\uf29c"

    /**
     * See [fa-step-forward](https://fontawesome.com/v4.7.0/icon/step-forward/).
     *
     * @author Konstantin Kopper
     */
    const val FA_STEP_FORWARD = "\uf051"

    /**
     * See [fa-stop](https://fontawesome.com/v4.7.0/icon/stop/).
     *
     * @author Konstantin Kopper
     */
    const val FA_STOP = "\uf04d"

    /**
     * See [fa-times-circle](https://fontawesome.com/v4.7.0/icon/times-circle/).
     *
     * @author Konstantin Kopper
     */
    const val FA_TIMES_CIRCLE = "\uf057"

    /**
     * See [fa-trash-o](https://fontawesome.com/v4.7.0/icon/trash-o/).
     *
     * @author Konstantin Kopper
     */
    const val FA_TRASH_O = "\uf014"
}
