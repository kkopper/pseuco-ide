package fxGui

import javafx.beans.property.BooleanProperty
import javafx.beans.property.SimpleBooleanProperty
import javafx.scene.Group
import javafx.scene.control.Alert
import javafx.scene.control.CheckBox
import javafx.scene.control.DialogPane

/**
 * Custom alert extension providing a `Do not show again` checkbox.
 *
 * @author Konstantin Kopper
 * @since 2.0.0
 * @constructor Creates a new alert instance.
 * @param alertType The type of this alert.
 * @param initialState The initial state of the included checkbox.
 */
internal class DoNotShowAgainAlert(
    alertType: Alert.AlertType = AlertType.CONFIRMATION,
    initialState: Boolean = false
) : PseucoAlert(alertType) {

    /**
     * Private property holding the value of the `Do not show again` checkbox.
     * Bound to the selected property of the checkbox.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    private val doNotShowAgainProperty: BooleanProperty = SimpleBooleanProperty(initialState)

    /**
     * The value of the `Do not show again` checkbox.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    var doNotShowAgain: Boolean
        get() = doNotShowAgainProperty.get()
        set(value) = doNotShowAgainProperty.set(value)

    init {
        dialogPane = object : DialogPane() {
            override fun createDetailsButton() = CheckBox().apply {
                text = "Do not show again."
                isAllowIndeterminate = false
                selectedProperty().bindBidirectional(doNotShowAgainProperty)
            }
        }.apply {
            stylesheets += this@DoNotShowAgainAlert.dialogPane.stylesheets
            expandableContent = Group()
            isExpanded = true
            buttonTypes += this@DoNotShowAgainAlert.buttonTypes
            graphic = this@DoNotShowAgainAlert.dialogPane.graphic
        }
    }
}
