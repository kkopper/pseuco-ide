package fxGui.completion

/**
 * Data class providing information on placeholders needed for auto completion.
 * Placeholders are usually encapsulated within replacements.
 *
 * @author Konstantin Kopper
 * @since 2.0.0
 * @see Replacement
 * @see Hints
 * @property position The offset of the current position to this placeholder.
 * @property expand Flag whether the placeholder is expandable.
 * @property text The text to be inserted at offset [position].
 * @constructor Create a new placeholder.
 */
internal data class Placeholder(internal val position: Int, internal val expand: Boolean, internal val text: String) {

    /**
     * Create a new unexpandable placeholder.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    constructor(position: Int, text: String) : this(position, false, text)
}
