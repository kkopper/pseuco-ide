package fxGui.completion

/**
 * Data class providing information on replacements during auto completion.
 *
 * @author Konstantin Kopper
 * @since 2.0.0
 * @see Hints
 * @property displayText The text to be displayed as suggestion.
 * @property template The text to be inserted.
 * @property hasContinuation TODO document property
 * @property placeholders A list of possible placeholders to be inserted into [template].
 * @constructor Create a new replacement.
 */
internal data class Replacement(
    internal val displayText: String,
    internal val template: String,
    internal val hasContinuation: Boolean,
    internal val placeholders: List<Placeholder>
) {

    /**
     * Create a replacement which has no placeholders.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    constructor(displayText: String, template: String) : this(displayText, template, false, listOf())

    /**
     * Create a replacement which has no continuation.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    constructor(displayText: String, template: String, placeholders: List<Placeholder>) :
        this(displayText, template, false, placeholders)
}
