package fxGui.completion

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import fxGui.main.PseuCoArea
import java.io.BufferedReader
import java.io.InputStreamReader

/**
 * Singleton providing hints for auto completion.
 *
 * @author Konstantin Kopper
 * @since 2.0.0
 */
internal object Hints {

    /**
     * Input stream providing the hints encoded as JSON.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    private val jsonHints = this::class.java.getResourceAsStream("/pseucoHints.json")

    /**
     * Map of strings to a list of possible replacements.
     * Initialized at first access from JSON file.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    private val hints: Map<String, @JvmSuppressWildcards List<Replacement>> = Gson().fromJson(
        BufferedReader(InputStreamReader(jsonHints)),
        object : TypeToken<Map<String, @JvmSuppressWildcards List<Replacement>>>() {}.type
    )

    /**
     * Map of keywords to a list of replacements.
     * This list only contains a single replacement: the keyword itself.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    private val keywords = PseuCoArea.PSEUCO_TOKEN.map { it to listOf(Replacement(it, it)) }.toMap()

    /**
     * Union of [keywords] and [hints].
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    private val replacements = keywords + hints

    /**
     * Create a regular expression matching all replacements which start with [str].
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     * @param str The leading substring of a replacement.
     * @return A regex matching replacements starting with [str].
     */
    private fun regex(str: String) = Regex("^$str\\S*$")

    /**
     * Get replacements for a given [text].
     *
     * @param text The text possible replacements should be queried.
     * @return A list of possible replacements for given [text], an empty list if there are no replacements available.
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    internal operator fun get(text: String) =
        replacements.filterKeys { it.matches(regex(text)) }.toSortedMap().values.takeUnless { it.isEmpty() }
            ?.reduce { acc, arr -> acc + arr }
            ?: emptyList()

    /**
     * Checks if replacements are available for a given [text].
     *
     * @param text The string to be checked.
     * @return `True` if replacements are available, `false otherwise`.
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    internal operator fun contains(text: String) =
        text.isNotEmpty() && replacements.filterKeys { it.matches(regex(text)) }.isNotEmpty()
}
