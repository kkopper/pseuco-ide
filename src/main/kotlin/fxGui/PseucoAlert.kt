package fxGui

import javafx.scene.control.Alert
import javafx.scene.control.Label

/**
 * Convenience wrapper around JavaFX alerts.
 * Main purpose is to set own styles.
 *
 * @author Konstantin Kopper
 * @since 2.0.0
 * @see Alert
 * @property alertType The underlying alert type.
 * @constructor Creates a new alert with custom settings.
 * @see javafx.scene.control.Alert.AlertType
 */
internal open class PseucoAlert(alertType: Alert.AlertType) : Alert(alertType) {

    init {
        super.getDialogPane().stylesheets.addAll(Main.GENERAL_STYLESHEET, STYLESHEET)

        headerText = null

        alertType.takeUnless { it == AlertType.NONE }?.apply {
            val icon = when (this) {
                AlertType.CONFIRMATION -> Label(FontAwesome.FA_QUESTION_CIRCLE).apply {
                    styleClass.add("custom-confirmation-graphic")
                }
                AlertType.ERROR -> Label(FontAwesome.FA_TIMES_CIRCLE).apply {
                    styleClass.add("custom-error-graphic")
                }
                AlertType.INFORMATION -> Label(FontAwesome.FA_INFO_CIRCLE).apply {
                    styleClass.add("custom-information-graphic")
                }
                AlertType.WARNING -> Label(FontAwesome.FA_EXCLAMATION_CIRCLE).apply {
                    styleClass.add("custom-warning-graphic")
                }
                AlertType.NONE -> null // Never happens.
            }?.apply {
                // Already set by stylesheet, but overwritten by general label style.
                style = "-fx-font-family: 'FontAwesome'"
            }

            super.setGraphic(icon)
        }
    }

    companion object {
        /**
         * The stylesheet for this class.
         *
         * @author Konstantin Kopper
         * @since 2.0.0
         */
        private val STYLESHEET = PseucoAlert::class.java.getResource("/fxGui/css/alert.css").toExternalForm()
    }
}
