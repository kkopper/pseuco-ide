package fxGui

import javafx.beans.property.StringProperty
import javafx.fxml.FXML
import javafx.fxml.FXMLLoader
import javafx.scene.control.TextArea
import javafx.scene.layout.AnchorPane
import java.net.URL
import kotlin.reflect.KProperty

/**
 * Controller for a pane providing only a single text area.
 *
 * @author Konstantin Kopper
 * @since 2.0.0
 * @property text The text to be displayed in the text area.
 * @constructor Create a new text pane showing the given [text].
 */
class TextPane(text: String) : AnchorPane() {

    /**
     * The underlying text area.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     * @see TextArea
     */
    @FXML
    private lateinit var textArea: TextArea

    /**
     * Create a new text pane showing the content of the given [textFile].
     *
     * @param textFile A URL to the file to be included.
     */
    constructor(textFile: URL) : this(textFile.readText())

    init {
        FXMLLoader(this::class.java.getResource("/fxGui/fxml/text.fxml")).apply {
            setRoot(this@TextPane)
            setController(this@TextPane)
            load()
        }

        textArea.text = text
    }

    /**
     * The text being displayed by [textArea].
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    var text: String by textArea.textProperty()

    operator fun StringProperty.getValue(thisRef: Any, property: KProperty<*>): String = value
    operator fun StringProperty.setValue(thisRef: Any, property: KProperty<*>, value: String) = setValue(value)
}
