package fxGui

import com.pseuco.api.PseuCoShare
import javafx.event.EventHandler
import javafx.scene.control.Hyperlink
import javafx.scene.control.Label
import javafx.scene.control.TextArea
import javafx.scene.layout.GridPane
import javafx.scene.layout.Priority
import java.io.PrintWriter
import java.io.StringWriter
import java.net.URL

/**
 * Alert useful to display exception stack traces.
 *
 * @author Konstantin Kopper
 * @see Throwable
 * @see PseucoAlert
 * @see javafx.scene.control.Alert
 * @property e The exception to be displayed.
 * @constructor Creates a new alert encapsulating the given throwable.
 */
internal class ExceptionAlert(private val e: Throwable) : PseucoAlert(AlertType.ERROR) {

    init {
        // Inspired by
        // http://code.makery.ch/blog/javafx-dialogs-official/

        super.getDialogPane().stylesheets += STYLESHEET

        title = "Something went wrong"
        headerText = null
        contentText = "Uncaught exception:${System.lineSeparator()}${e.localizedMessage}"

        isResizable = false

        // Create expandable Exception.
        val sw = StringWriter()
        val pw = PrintWriter(sw)
        e.printStackTrace(pw)
        val exceptionText = sw.toString()

        val label = Label("The exception stacktrace was:")

        val textArea = TextArea(exceptionText).apply {
            id = "stacktrace"
            isEditable = false

            maxWidth = java.lang.Double.MAX_VALUE
            maxHeight = java.lang.Double.MAX_VALUE
        }

        GridPane.setVgrow(textArea, Priority.ALWAYS)
        GridPane.setHgrow(textArea, Priority.ALWAYS)

        // Create hyperlink to file a bug report.
        val issueLink = Hyperlink("If you believe that this is a bug, you can file a bug report here.").apply {
            id = "issueLink"
            onAction = EventHandler {
                val link = URL("https://dgit.cs.uni-saarland.de/pseuco/pseuco-ide/issues/" +
                        "new?issuable_template=Bug&issue[title]=BUG:")
                PseuCoShare.openInBrowser(link.toURI())
            }
        }

        val expContent = GridPane().apply {
            maxWidth = java.lang.Double.MAX_VALUE
            add(label, 0, 0)
            add(textArea, 0, 1)
            add(issueLink, 0, 2)
        }

        // Set expandable Exception into the dialog pane.
        dialogPane.expandableContent = expContent
    }

    companion object {
        /**
         * The stylesheet with additional settings for the expandable content.
         */
        private val STYLESHEET = ExceptionAlert::class.java.getResource("/fxGui/css/alert_exception.css").toExternalForm()
    }
}
