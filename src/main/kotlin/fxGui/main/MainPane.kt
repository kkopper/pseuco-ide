package fxGui.main

import com.pseuco.PseuCoComFile
import com.pseuco.PseuCoFileSharer
import com.pseuco.api.PseuCoShare
import com.pseuco.api.PseuCoShareException
import com.pseuco.dataraces.PseuCoDRD
import com.pseuco.websocket.PseuCoWebSocket
import config.Config
import fxGui.DoNotShowAgainAlert
import fxGui.ExceptionAlert
import fxGui.FontAwesome
import fxGui.HelpController
import fxGui.Main
import fxGui.PseucoAlert
import fxGui.ZoomEventHandler
import fxGui.debugger.FXDebugger
import fxGui.share.ImportPane
import fxGui.share.SharePane
import fxGui.share.URLAlert
import javafx.application.Platform
import javafx.event.ActionEvent
import javafx.fxml.FXML
import javafx.fxml.FXMLLoader
import javafx.scene.Scene
import javafx.scene.control.Alert
import javafx.scene.control.Button
import javafx.scene.control.ButtonType
import javafx.scene.control.Label
import javafx.scene.control.TextArea
import javafx.scene.control.ToolBar
import javafx.scene.control.Tooltip
import javafx.scene.control.TreeItem
import javafx.scene.control.TreeView
import javafx.scene.image.Image
import javafx.scene.input.KeyCode
import javafx.scene.input.KeyCodeCombination
import javafx.scene.input.KeyCombination
import javafx.scene.input.KeyEvent
import javafx.scene.layout.AnchorPane
import javafx.stage.DirectoryChooser
import javafx.stage.FileChooser
import javafx.stage.Stage
import javafx.stage.StageStyle
import javafx.stage.Window
import kotlinx.coroutines.runBlocking
import org.apache.commons.io.FileUtils
import org.apache.commons.io.comparator.SizeFileComparator
import pseuco.PseuCoTask
import pseuco.javaCompiler.generator.PseuCoJavaGenerator
import pseuco.javaCompiler.runner.PseuCoRunner
import util.FileUtilities
import util.Workspace
import java.io.File
import java.io.IOException
import java.io.PrintStream
import java.nio.file.Files
import java.util.regex.Pattern
import kotlin.concurrent.thread

/**
 * The controller for the main view of the IDE.
 *
 * @author Konstantin Kopper
 * @since 2.0.0
 * @property stage The window hosting this controller. Needed to present dialogs, alerts, ...
 * @constructor Creates a new instance of the main GUI controller.
 */
internal class MainPane(private val stage: Window) : AnchorPane() {

    /**
     * The toolbar placed on the top of the window.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    @FXML
    private lateinit var controlBar: ToolBar

    /**
     * The button used to create a new file. Calls [newFile].
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    @FXML
    private lateinit var newButton: Button

    /**
     * The button used to change the workspace. Calls [changeWorkspace].
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    @FXML
    private lateinit var openButton: Button

    /**
     * The button used to save the currently opened file. Calls [saveFile].
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    @FXML
    private lateinit var saveButton: Button

    /**
     * The button used to export the Java code created for the currently opened file. Calls [runPseuCoTask].
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    @FXML
    private lateinit var exportToJavaButton: Button

    /**
     * The button used to execute the currently opened file. Calls [runPseuCoTask].
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    @FXML
    private lateinit var runButton: Button

    /**
     * The button used to start the debugger with the currently opened file. Calls [runPseuCoTask].
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    @FXML
    private lateinit var debugButton: Button

    /**
     * The button used to interrupt the currently running task.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    @FXML
    private lateinit var stopButton: Button

    /**
     * The button used to open the current file in [pseuCo.com](htps://pseuco.com). Calls [openInPseuCoCom].
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    @FXML
    private lateinit var openInPseuCoComButton: Button

    /**
     * The button used to trigger the export of the currently opened file to [pseuCo.com](https://pseuco.com).
     * Calls [exportToPseuCoCom].
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    @FXML
    private lateinit var exportToPseuCoComButton: Button

    /**
     * The button used to import a file from [pseuCo.com](https://pseuco.com). Calls [importFromPseuCoCom].
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    @FXML
    private lateinit var importFromPseuCoComButton: Button

    /**
     * The button used to trigger the connection to the [pseuCo.com](https://pseuco.com) data race analysis tool.
     * Calls [detectDataRaces].
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    @FXML
    private lateinit var detectDataRacesButton: Button

    /**
     * The button used to open the about scene. Calls [showHelp].
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    @FXML
    private lateinit var helpButton: Button

    /**
     * The file tree displaying all pseuCo files in the workspace.
     *
     * @author Konstantin Kopper
     * @since 2.0.
     */
    @FXML
    private lateinit var fileTree: TreeView<FileWrapper>

    /**
     * The text area used to display any console output.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    @FXML
    private lateinit var consoleTextArea: TextArea

    /**
     * The anchor pane which should include the [codeArea].
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    @FXML
    private lateinit var codeAnchor: AnchorPane

    /**
     * The anchor pane which should include [consoleTextArea].
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    @FXML
    private lateinit var consoleAnchor: AnchorPane

    /**
     * The code input area.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    @FXML
    private lateinit var codeArea: PseuCoArea

    /**
     * The console wrapping the [consoleTextArea].
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    @FXML
    private lateinit var console: Console

    /**
     * The button clearing the console.
     *
     * @author Konstantin Kopper
     * @since 2.0.3
     */
    @FXML
    private lateinit var consoleClearButton: Button

    /**
     * The currently opened file.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    private var opened: File? = null

    init {
        FXMLLoader(javaClass.getResource("/fxGui/fxml/main.fxml")).apply {
            setRoot(this@MainPane)
            setController(this@MainPane)
            load()
        }
    }

    /**
     * Initializes the pane.
     * Called by the load method from the constructor.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    fun initialize() {
        // Set icons for buttons
        this.newButton.apply {
            text = FontAwesome.FA_FILE_O
            tooltip = Tooltip("Create a new file")
        }

        this.openButton.apply {
            text = FontAwesome.FA_FOLDER_OPEN_O
            tooltip = Tooltip("Open a new workspace")
        }

        this.saveButton.apply {
            text = FontAwesome.FA_FLOPPY_O
            tooltip = Tooltip("Save the current file")
        }

        this.exportToJavaButton.apply {
            text = FontAwesome.FA_CODE
            tooltip = Tooltip("Generate Java code")
        }

        this.runButton.apply {
            text = FontAwesome.FA_PLAY
            tooltip = Tooltip("Run")
        }

        this.debugButton.apply {
            text = FontAwesome.FA_BUG
            tooltip = Tooltip("Debug")
        }

        this.stopButton.apply {
            text = FontAwesome.FA_STOP
            tooltip = Tooltip("Stop")
            isDisable = true
        }

        this.openInPseuCoComButton.apply {
            text = "pseuCo.com"
            graphic = Label(FontAwesome.FA_EXTERNAL_LINK)
                // TODO get rid of workaround
                // Label font size is not updated, thus manually adjusted here.
                .also {
                    ZoomEventHandler.fxString.addListener { _, _, newValue ->
                        it.style = "-fx-font-size: ${newValue.substring(15, newValue.length - 2).toInt() * 1.34}%;"
                    }
                }
            tooltip = Tooltip("Open the current file in pseuCo.com")
        }

        this.exportToPseuCoComButton.apply {
            text = FontAwesome.FA_CLOUD_UPLOAD
            tooltip = Tooltip("Share the current file using pseuCo.com")
        }

        this.importFromPseuCoComButton.apply {
            text = FontAwesome.FA_CLOUD_DOWNLOAD
            tooltip = Tooltip("Import a file from pseuCo.com")
        }

        this.detectDataRacesButton.apply {
            text = FontAwesome.FA_FLAG_CHECKERED
            tooltip = Tooltip("Detect data races")
        }

        this.helpButton.apply {
            text = FontAwesome.FA_QUESTION_CIRCLE_O
            tooltip = Tooltip("Help")
        }

        this.consoleClearButton.apply {
            text = FontAwesome.FA_TRASH_O
            tooltip = Tooltip("Clear the console")
        }

        // Make console output non editable
        this.consoleTextArea.isEditable = false

        // Initialise console wrapper
        this.console = Console(this.consoleTextArea)

        // File Tree
        val action: (File?) -> Unit = { _: File? ->
            parseFileTree(Workspace.asFile).also { Platform.runLater { initializeFileTree(it) } }
        }

        Workspace.onFileChange = action
        action(null)

        // Set WebSockets server callback
        PseuCoWebSocket.onOpen = { file ->
            Platform.runLater {
                openFile(file)
                stage.requestFocus()
            }
        }

        styleProperty().bind(ZoomEventHandler.fxString)
        stage.addEventFilter(KeyEvent.KEY_PRESSED, ZoomEventHandler)
    }

    /**
     * Sets keyboard shortcuts on the given scene.
     *
     *  * `CMD + N` - Creates a new file.
     *  * `CMD + O` - Opens a new workspace.
     *  * `CMD + S` - Saves the current file.
     *  * `CMD + R` - Runs the currently open program.
     *  * `CMD + D` - Debugs the currently open program.
     *  * `Ctrl + C` - Cancels the currently running task.
     *  * `CMD + H` - Opens the help/about scene.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     * @param scene The scene running this controller.
     */
    internal fun setShortcuts(scene: Scene) {

        /**
         * Internal runnable encapsulating a GUI button.
         * Fires the button when called (if not disabled), i.e. the underlying action is executed.
         *
         * @author Konstantin Kopper
         * @since 2.0.0
         * @property button The encapsulated GUI button.
         */
        class ShortcutRunnable(private val button: Button) : Runnable {

            override fun run() = button.takeUnless { it.isDisable }?.fire() ?: Unit
        }

        scene.accelerators.putAll(HashMap<KeyCodeCombination, Runnable>().apply {
            put(KeyCodeCombination(KeyCode.N, KeyCombination.SHORTCUT_DOWN), ShortcutRunnable(newButton))
            put(KeyCodeCombination(KeyCode.O, KeyCombination.SHORTCUT_DOWN), ShortcutRunnable(openButton))
            put(KeyCodeCombination(KeyCode.S, KeyCombination.SHORTCUT_DOWN), ShortcutRunnable(saveButton))
            put(KeyCodeCombination(KeyCode.R, KeyCombination.SHORTCUT_DOWN), ShortcutRunnable(runButton))
            put(KeyCodeCombination(KeyCode.D, KeyCombination.SHORTCUT_DOWN), ShortcutRunnable(debugButton))
            put(KeyCodeCombination(KeyCode.C, KeyCombination.CONTROL_DOWN), ShortcutRunnable(stopButton))
            put(KeyCodeCombination(KeyCode.H, KeyCombination.SHORTCUT_DOWN), ShortcutRunnable(helpButton))
        })
    }

    /**
     * Disables all buttons which may start a task requiring a Java compiler ([runButton] and [debugButton]).
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    internal fun disableJavaCompilerTasks() {
        runButton.isDisable = true
        debugButton.isDisable = true
    }

    /**
     * Checks whether the code in the input area has changed compared to the corresponding file content.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     * @return `True`, if the input differs from the file content, `false` otherwise.
     * @see FileUtils.contentEquals
     * @see SizeFileComparator
     */
    private fun hasChanged(): Boolean {
        if (this.opened == null) {
            // No file was opened, thus nothing changed.
            return false
        }

        if (this.opened?.length() != this.codeArea.plainText.length.toLong()) {
            // The file content and the entered code differ in length, thus the content has changed.
            return true
        }

        // Same length, thus compare content
        val tmpDir = FileUtilities.getTmpFolder()
        val tmp = File(tmpDir, ".pseuco-ide-" + System.currentTimeMillis() + ".txt")
        if (tmp.exists()) {
            PseucoAlert(Alert.AlertType.ERROR).apply {
                title = "Could not save the file."
                contentText = "Could not create temporary file: Already exists. Please try again later."
                initOwner(stage)
            }.showAndWait()
            return true // TODO check
        }

        try {
            val created = tmp.createNewFile()
            if (!created) {
                // TODO
            }
        } catch (e: IOException) {
            e.printStackTrace(Main.STANDARD_ERR)
            ExceptionAlert(e).showAndWait()
            return true // TODO check
        }

        try {
            tmp.bufferedWriter().use { it.write(this.codeArea.plainText) }
        } catch (e: IOException) {
            e.printStackTrace(Main.STANDARD_ERR)
            ExceptionAlert(e).showAndWait()
        }

        var changed = false

        val sizeDiffers = SizeFileComparator.SIZE_COMPARATOR.compare(this.opened, tmp)

        if (sizeDiffers != 0) {
            try {
                val contentDiffers = !FileUtils.contentEquals(this.opened, tmp)
                if (contentDiffers) {
                    changed = true
                }
            } catch (e: IOException) {
                e.printStackTrace(Main.STANDARD_ERR)
                ExceptionAlert(e).showAndWait()
                changed = true // TODO check
            }
        }

        val deleted = tmp.delete()
        if (!deleted) {
            tmp.deleteOnExit()
        }

        return changed
    }

    /**
     * Recursively generates [TreeItem]s for the file tree.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     * @param folder The root folder.
     * @return The [TreeItem] representing the root folder.
     */
    private fun parseFileTree(folder: File): TreeItem<FileWrapper> {
        assert(folder.isDirectory)

        val root = TreeItem(FileWrapper(folder))

        Files.newDirectoryStream(folder.toPath()).use {
            it.sorted().forEach {
                val f = it.toFile()

                if (f.isFile && Pattern.matches("^.*$PSEUCO_FILE_REGEX", f.name)) {
                    val fileName = f.name.replace(PSEUCO_FILE_REGEX, "")
                    root.children += TreeItem(FileWrapper(f, fileName)).apply {
                        graphic = Label(FontAwesome.FA_FILE_CODE_O)
                    }
                } else if (f.isDirectory) {
                    val subTree = parseFileTree(f)
                    if (subTree.children.isNotEmpty())
                        root.children += subTree.apply { graphic = Label(FontAwesome.FA_FOLDER_O) }
                }
            }
        }

        return root
    }

    /**
     * Initializes the file tree representing the workspace.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     * @param rootItem The root item of the file tree (representing the workspace directory)
     */
    private fun initializeFileTree(rootItem: TreeItem<FileWrapper>) {
        rootItem.value = FileWrapper(Workspace.asFile, "Workspace")
        this.fileTree.root = rootItem
        rootItem.isExpanded = true

        // Open file on double click
        this.fileTree.setOnMouseClicked { event ->
            if (event.clickCount == 2 && event.source != rootItem) {
                if (this.hasChanged()) {
                    try {
                        save()
                    } catch (e: IOException) {
                        if (Main.DEBUG)
                            e.printStackTrace(Main.STANDARD_ERR)
                        ExceptionAlert(e).showAndWait()
                        return@setOnMouseClicked
                    }
                }

                val fileItem = fileTree.selectionModel.selectedItem
                if (fileItem.children.size == 0)
                    openFile(fileItem.value.file)
            }
        }
    }

    fun openFile(file: File) {
        assert(file.exists())
        assert(file.isFile)

        file.bufferedReader().use {
            codeArea.apply {
                isDisable = false
                clear()
            }
            it.lines().forEach { codeArea += "$it\n" }
        }

        this.opened = file

        // Save the file to ensure the saved file uses newline characters.
        try {
            save()
        } catch (e: IOException) {
            if (Main.DEBUG)
                e.printStackTrace(Main.STANDARD_ERR)
        }

        (stage as Stage).title = "pseuCo IDE - ${file.name}"
    }

    /**
     * Creates a new file in the workspace.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    @FXML
    fun newFile() {
        var newFile = FileChooser().apply {
            initialDirectory = Workspace.asFile
            extensionFilters += FileChooser.ExtensionFilter("pseuCo source files", "*.pseuco", "*.pseuCo")
        }.showSaveDialog(stage) ?: return // Cancelled

        if (newFile.exists())
            return

        if (!newFile.extension.matches(Regex("(?i)pseuco")))
            newFile = File(newFile.parent, "${newFile.name}.pseuco")

        assert(newFile.name.matches(Regex("(?i)^.*\\.pseuco$"))) { "Unexpected file extension." }

        try {
            Workspace.createNewFile(newFile)
            save()
            openFile(newFile)
        } catch (e: Exception) {
            ExceptionAlert(e).showAndWait()
            if (Main.DEBUG)
                e.printStackTrace(Main.STANDARD_ERR)
        }
    }

    /**
     * Change the workspace.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    @FXML
    fun changeWorkspace() {
        val workspaceChooser = DirectoryChooser().apply {
            title = "Choose new workspace"
            initialDirectory = Workspace.asFile
        }

        val newWorkspace = workspaceChooser.showDialog(this.stage) ?: return // Cancelled
        assert(newWorkspace.isDirectory)

        Workspace.change(newWorkspace)
    }

    /**
     * Save the currently opened file.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    @FXML
    fun saveFile() {
        try {
            save()
        } catch (e: IOException) {
            e.printStackTrace(Main.STANDARD_ERR)
            ExceptionAlert(e).showAndWait()
        }
    }

    /**
     * Save the currently opened file.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     * @throws IOException Could not write to file.
     */
    @Throws(IOException::class)
    private fun save() {
        this.opened?.bufferedWriter()?.use { it.write(this.codeArea.plainText) } ?: return
    }

    /**
     * Executes a pseuCo task. Which one depends from the button invoking this method.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    @FXML
    fun runPseuCoTask(e: ActionEvent) {
        if (this.opened == null) {
            PseucoAlert(Alert.AlertType.ERROR).apply {
                title = "No file opened."
                contentText = "You have to open a file before you can execute it."
                initOwner(stage)
            }.showAndWait()
            return
        }

        val task: PseuCoTask = when (e.source) {
            runButton -> {
                PseuCoRunner(this.opened!!, this.console, this.console)
            }
            debugButton -> {
                FXDebugger(this.opened!!, this.console, this.console, this.stage)
            }
            exportToJavaButton -> {
                val destination = DirectoryChooser().apply {
                    title = "Choose export folder."
                    initialDirectory = Workspace.asFile
                }.showDialog(this.stage) ?: return

                PseuCoJavaGenerator(this.opened!!, destination) {
                    Platform.runLater {
                        PseucoAlert(Alert.AlertType.INFORMATION).apply {
                            title = "Successfully exported Java code"
                            contentText = "The Java code was successfully exported to ${destination.absolutePath}."
                        }.showAndWait()
                    }
                }
            }
            else -> {
                PseucoAlert(Alert.AlertType.ERROR).apply {
                    title = "WTF?!"
                    contentText = "You called a function you should not be calling."
                    initOwner(stage)
                }.showAndWait()
                return
            }
        }.apply {
            onError { Platform.runLater { ExceptionAlert(it).showAndWait() } }
            onCompilerError { System.err.println(it) }
        }

        if (hasChanged()) {
            try {
                save()
            } catch (e: IOException) {
                if (Main.DEBUG)
                    e.printStackTrace(Main.STANDARD_ERR)
                ExceptionAlert(e).showAndWait()
                return
            }
        }

        this.console.clear()

        // Set button state
        this.runButton.isDisable = true
        this.debugButton.isDisable = true
        this.stopButton.isDisable = false

        // Set actions
        this.stage.setOnCloseRequest { task.interrupt() }
        this.stopButton.setOnAction { task.interrupt() }

        thread(start = true, name = "PseuCo Task") {
            task.run()

            // Fix buttons
            this.runButton.isDisable = false
            this.debugButton.isDisable = false
            this.stopButton.isDisable = true

            // Delete actions
            this.stage.onCloseRequest = null
            this.stopButton.onAction = null
        }
    }

    @FXML
    fun openInPseuCoCom() {
        val fileName = this.opened?.name?.replace(PSEUCO_FILE_REGEX, "") ?: return
        val pseuCoFile = PseuCoComFile(fileName, codeArea.plainText)

        if ("use-websocket" !in Config) {
            Stage().apply {
                title = "pseuCo.com file sharing"
                scene = Scene(SharePane(this))
                isResizable = false
                initOwner(stage)
            }.showAndWait()
        }

        thread {
            val uri = try {
                runBlocking { PseuCoFileSharer.uploadFile(pseuCoFile) }
            } catch (e: PseuCoShareException) {
                Platform.runLater { ExceptionAlert(e).showAndWait() }
                return@thread
            }
            PseuCoShare.openInBrowser(uri)
        }
    }

    /**
     * Export the current file to [pseuCo.com](https://pseuco.com).
     * Transmits the file to the pseuCo.com API, and opens it in the browser.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    @FXML
    fun exportToPseuCoCom() {
        val fileName = this.opened?.name?.replace(PSEUCO_FILE_REGEX, "") ?: return // TODO show alert
        val pseuCoFile = PseuCoComFile(fileName, codeArea.plainText)

        if (!Config.acceptSharingAgreement) {
            // Sharing agreement not accepted
            val alert = DoNotShowAgainAlert().apply {
                title = "The pseuCo.com sharing agreement"
                contentText = "If you continue, the following will happen: The file will be uploaded to the server " +
                    "and published under a random id. Anyone knowing or guessing this id, and the server " +
                    "operators, can read your file. There is no possibility for you to modify or delete it. " +
                    "Your IP address will be logged if you share files.\n" +
                    "The fine print: Only upload files to which you own the copyright. By continuing, you grant " +
                    "us a non-exclusive, worldwide, royalty-free, sub-licensable and transferable license to use, " +
                    "publish and create derivative works of the file you submit. We cannot guarantee the " +
                    "availability of the file you uploaded."
            }.also { it.showAndWait().takeUnless { it.get() != ButtonType.OK } ?: return }

            if (alert.doNotShowAgain) {
                // Warning should no longer be displayed, hence update the config file.
                Config.acceptSharingAgreement = true
                Config.save()
            }
        }

        thread {
            val uri = try {
                runBlocking { PseuCoShare.shareFile(pseuCoFile, false) }
            } catch (e: PseuCoShareException) {
                Platform.runLater { ExceptionAlert(e).showAndWait() }
                return@thread
            }

            Platform.runLater {
                Stage().apply {
                    title = "pseuCo IDE - File sharing"
                    scene = Scene(URLAlert(uri))
                    icons += Image(this@MainPane::class.java.getResource("/fxGui/images/icon.png").toExternalForm())
                    initOwner(stage)
                }.showAndWait()
            }
        }
    }

    @FXML
    fun importFromPseuCoCom() {
        val dialogStage = Stage()
        val importPane = ImportPane(dialogStage)
        dialogStage.apply {
            title = "pseuCo.com file sharing - Import"
            scene = Scene(importPane)
            icons += Image(this@MainPane::class.java.getResourceAsStream("/fxGui/images/icon.png"))
            isResizable = false
            initOwner(stage)
        }.showAndWait()

        // Cancelled if result is null, hence abort.
        importPane.result ?: return

        thread {
            val remoteFile = try {
                runBlocking { PseuCoShare.downloadFile(importPane.result!!) }
            } catch (e: PseuCoShareException) {
                Platform.runLater { ExceptionAlert(e).showAndWait() }
                return@thread
            }

            if (remoteFile.type != PseuCoComFile.Type.PSEUCO) {
                Platform.runLater {
                    PseucoAlert(Alert.AlertType.ERROR).apply {
                        title = "Unsupported file type."
                        contentText = "Expected a pseuCo file."
                        initOwner(stage)
                        show()
                    }
                }
                return@thread
            }

            val file = Workspace.createNewFile(remoteFile)
            Platform.runLater { openFile(file) }
        }
    }

    @FXML
    fun detectDataRaces() {
        // Ignore if no file is opened
        this.opened ?: return

        if (!Config.acceptAnalysisAgreement) {
            // Analysis agreement not accepted
            val alert = DoNotShowAgainAlert().apply {
                title = "The pseuCo.com analysis agreement"
                contentText = "PseuCo.com can identify potential data races in your program. This analysis is run on " +
                    "the server. You need to be online to use it.\n" +
                    "By using this service, you agree that we submit your pseuCo program to the server to " +
                    "process it. We may temporarily retain a copy of your program as part of log files to help " +
                    "us improve the service."
            }.also { it.showAndWait().takeUnless { it.get() != ButtonType.OK } ?: return }

            if (alert.doNotShowAgain) {
                // Warning should no longer be displayed, hence update the config file.
                Config.acceptAnalysisAgreement = true
                Config.save()
            }
        }

        this.detectDataRacesButton.apply {
            isDisable = true
        }

        this.console.clear()
        val consoleStream = PrintStream(console)

        PseuCoDRD.onMessage { _, msg -> Platform.runLater { consoleStream.println(msg) } }

        thread {
            try {
                PseuCoDRD.analyse(this@MainPane.opened!!)
            } catch (e: PseuCoDRD.DRDException) {
                Platform.runLater { ExceptionAlert(e).apply { initOwner(stage) }.showAndWait() }
                // TODO return here? Button will never be enabled again...
            }

            this@MainPane.detectDataRacesButton.apply {
                isDisable = false
            }
        }
    }

    /**
     * Shows the about scene.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    @FXML
    fun showHelp() {
        val icon = Image(javaClass.getResource("/fxGui/images/icon.png").toExternalForm())

        Stage().apply {
            title = "pseuCo IDE - About"
            scene = Scene(HelpController())
            icons += icon
            isResizable = false
            initStyle(StageStyle.UTILITY)
            initOwner(stage)
        }.show()
    }

    /**
     * Clears the console.
     *
     * @author Konstantin Kopper
     * @since 2.0.3
     */
    @FXML
    fun clearConsole() = console.clear()

    companion object {
        /**
         * Regular expression matching *.pseuco files (ignoring lower-/uppercase).
         *
         * @author Konstantin Kopper
         * @since 2.0.0
         */
        private val PSEUCO_FILE_REGEX = "\\.[pP][sS][eE][uU][cC][oO]$".toRegex()
    }
}
