package fxGui.main

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import fxGui.completion.Hints
import javafx.application.Platform
import javafx.scene.control.ContextMenu
import javafx.scene.control.MenuItem
import javafx.scene.input.KeyCode
import org.apache.commons.lang3.StringUtils
import org.fxmisc.richtext.CodeArea
import org.fxmisc.richtext.LineNumberFactory
import org.fxmisc.richtext.model.PlainTextChange
import org.fxmisc.richtext.model.RichTextChange
import org.fxmisc.richtext.model.StyleSpans
import org.fxmisc.richtext.model.StyleSpansBuilder
import org.fxmisc.richtext.model.TwoDimensional
import org.fxmisc.wellbehaved.event.EventPattern
import org.fxmisc.wellbehaved.event.InputMap
import org.fxmisc.wellbehaved.event.Nodes
import java.io.BufferedReader
import java.io.InputStreamReader
import java.util.ArrayDeque
import java.util.Queue
import java.util.regex.Pattern

/**
 * Code area with custom settings for the pseuCo language.
 *
 * @author Konstantin Kopper
 * @since 2.0.0
 * @see org.fxmisc.richtext.CodeArea
 * @constructor Create a new (empty) [CodeArea] modified for the pseuCo language.
 */
internal class PseuCoArea : CodeArea() {

    private val placeholderHandler = PlaceholderHandler(this)

    /**
     * A list of integers representing the length of placeholders in the text.
     * Sorted in the order of occurrence of [PLACEHOLDER] in the text.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    private val tabs: Queue<Int> = ArrayDeque()

    init {
        super.setParagraphGraphicFactory(LineNumberFactory.get(this))
        super.plainTextChanges().filter(Companion::plainTextChangeFilter).subscribe(this::onPlainTextChange)
        super.richChanges().filter(Companion::richChangeFilter).subscribe(this::onRichTextChange)

        contextMenu = ContextMenu()

        setOnKeyPressed {
            @Suppress("NON_EXHAUSTIVE_WHEN")
            when (it.code) {
                KeyCode.SPACE -> if (it.isControlDown) generateAutoCompletion(caretPosition - 1)
                KeyCode.RIGHT /* 18 */, KeyCode.LEFT /* 16 */ -> if (!it.isShiftDown) {
                    var p = caretPosition
                    // TODO Is it a good idea to rely on the internal enum value (just to prevent code duplicates)?
                    while (this[p].startsWith(PLACEHOLDER)) p += (it.code.ordinal - 17)
                    moveTo(p)
                }
            }
        }

        Nodes.addInputMap(this, InputMap.consume(EventPattern.keyPressed(KeyCode.TAB), {
            assert(it.code == KeyCode.TAB)
            if (placeholderHandler.hasPlaceholderAfter(caretPosition)) {
                val placeholderPosition = placeholderHandler.findNextPlaceholder(caretPosition)
                assert(placeholderPosition != -1)
                val length = placeholderHandler.getLengthForPlaceholderAt(placeholderPosition)
                deleteCharAt(placeholderPosition)
                selectRange(placeholderPosition - length, placeholderPosition)
            } else
                this[caretPosition] = "\t"
        }))
    }

    override fun moveTo(pos: Int) = Platform.runLater { super.moveTo(pos) }

    // Plain text change handling

    /**
     * Handler for plain text changes.
     * Calculates completion, replacement of tabs by spaces, etc.
     *
     * @param change The plain text change to be handled.
     * @author Konstantin Kopper
     * @since 2.0.0
     * @see org.fxmisc.richtext.model.PlainTextChange
     */
    private fun onPlainTextChange(change: PlainTextChange) {
        val position = change.position

        contextMenu.hide()

        when (change.inserted) {
            "\t" -> {
                val column = caretColumn
                val numSpaces = TAB_SPACES - column % TAB_SPACES

                // Remove inserted tab character.
                deleteNextChar()

                // Insert spaces instead of tab character.
                this[position] = StringUtils.repeat(' ', numSpaces)

                moveTo(position + numSpaces)
            }
            "\n" -> {
                val numSpaces = calculateLeadingSpaces(position - 1)

                if (this[position - 1] == "{" && this[position + 1] == "}") {
                    // Indent this line by the number of spaces plus the number of spaces for a tab.
                    // Add a new line and indent the next line by the number of spaces counted.
                    this[position + 1] = StringUtils.repeat(' ', numSpaces + TAB_SPACES) +
                            "\n" +
                            StringUtils.repeat(' ', numSpaces)
                    moveTo(position + 1 + numSpaces + TAB_SPACES)
                } else {
                    // Only indent by the number of spaces counted for the previous line.
                    this[position + 1] = StringUtils.repeat(' ', numSpaces)
                    moveTo(position + 1 + numSpaces)
                }
            }
            "{" -> {
                this[position + 1] = "}#"
                moveTo(position + 1)
            }
            "(" -> {
                this[position + 1] = ")#"
                moveTo(position + 1)
            }
            "[" -> {
                this[position + 1] = "]#"
                moveTo(position + 1)
            }
            "}" -> if (this[position + 1] == "}") {
                deleteNextChar()
            }
            ")" -> if (this[position + 1] == ")") {
                deleteNextChar()
            }
            "]" -> if (this[position + 1] == "]") {
                deleteNextChar()
            }
            "\"" -> if (this[position + 1] == "\"") {
                // Next char is ", thus remove it to simply place caret behind
                // (as a second " is introduced by this change).
                deleteCharAt(position + 1)
            } else {
                // Next char is not ", thus check if previous char is ".
                if (this[position - 1] != "\"") {
                    // Previous char is not ", thus insert additional ".
                    // This check is necessary, as this method will be called for the new char too,
                    // which would otherwise result in an endless chain of calls.
                    this[position + 1] = "\""
                    moveTo(position + 1)
                }
            }
            "}#", ")#", "]#" -> {
                // Inserted by autocompletion.
                // '#' is necessary to distinguish from user entries.
                // Only remove '#' and place caret before bracket.
                deleteText(position + 1, position + 2)
            }
            else -> if (change.inserted.length == 1)
                generateAutoCompletion(position)
        }

        when (change.removed) {
            "{" -> if (this[position] == "}") {
                deleteNextChar()
            }
            "(" -> if (this[position] == ")") {
                deleteNextChar()
            }
            "[" -> if (this[position] == "]") {
                deleteNextChar()
            }
            "\"" -> if (this[position] == "\"" && this[position - 1] != "\"") {
                deleteNextChar()
            }
            " " -> {
                loop@ for (tmpPos in (position - 1) downTo 0) {
                    when (this[tmpPos]) {
                    // Found line separator at tmpPos, thus remove all spaces and place cursor appropriately.
                        "\n" -> {
                            Platform.runLater {
                                deleteText(tmpPos, position)
                                moveTo(tmpPos)
                            }
                            break@loop
                        }
                    // Found space, thus continue.
                        " " -> continue@loop
                    // Neither space nor newline, thus abort.
                        else -> break@loop
                    }
                }
            }
        }

        if (change.removed.let { it.isNotEmpty() && PLACEHOLDER in it && it.length > 1 })
            placeholderHandler.deletePlaceholdersFrom(change.position, change.removed.count { it == PLACEHOLDER })
    }

    /**
     * Calculates the number of leading spaces of a line.
     * Starts counting at [position] and reverses backwards until the next line separator is found.
     *
     * @author Konstantin Kopper
     * @param position The position inside the code area at which the calculation should start.
     * @return The number of leading spaces of the line containing [position].
     */
    private fun calculateLeadingSpaces(position: Int): Int {
        /**
         * Calculates the number of leading spaces recursively.
         *
         * @param position The position inside the code area at which the calculation should start.
         * @param numSpaces The number of leading spaces counted so far.
         * @return The number of leading spaces of the line containing [position].
         */
        tailrec fun calculateLeadingSpacesRecursive(position: Int, numSpaces: Int): Int {
            if (position < 0 || this[position] == "\n")
                return numSpaces
            return calculateLeadingSpacesRecursive(position - 1, if (this[position] == " ") numSpaces + 1 else 0)
        }

        return calculateLeadingSpacesRecursive(position, 0)
    }

    /**
     * Get the word ending at position and starting after the last whitespace character beforehand.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     * @param position The position at which the word ends.
     * @return The word on the left of [position].
     */
    private fun getWordAtPosition(position: Int): String {
        /**
         * Assembles the word recursively until a whitespace is found.
         *
         * @param position The current position.
         * @param word The word assembled so far.
         * @return The word on the left of [position].
         */
        tailrec fun getWordAtPositionRecursive(position: Int, word: String): String {
            return if (position < 0 || this[position].isBlank() || this[position][0] in "()[]{};+-*/.<?!") word
            else getWordAtPositionRecursive(position - 1, this[position] + word)
        }

        return getWordAtPositionRecursive(position, "")
    }

    /**
     * Calculate the content of the dropdown menu and shows it.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     * @param position The position for which the autocompletion should be displayed.
     */
    private fun generateAutoCompletion(position: Int) {
        val word = getWordAtPosition(position)

        if (word !in Hints)
            return

        contextMenu.items.clear()

        val start = position - word.length + 1

        Hints[word].forEach { r ->
            contextMenu.items += MenuItem(r.displayText).apply {
                setOnAction { _ ->
                    deleteText(start, position + 1)
                    this@PseuCoArea[start] = r.template

                    var offset = 0
                    r.placeholders.forEach { p ->
                        val toBeInserted = p.text.let { text ->
                            if (p.expand) {
                                val n = calculateLeadingSpaces(start)
                                val spaces = StringUtils.repeat(' ', n)
                                val tabSpaces = if (r.template[p.position - 1] == '{')
                                    StringUtils.repeat(' ', TAB_SPACES) else ""

                                "\n$spaces$tabSpaces$text$PLACEHOLDER\n$spaces"
                            } else "$text$PLACEHOLDER"
                        }
                        this@PseuCoArea[start + p.position + offset] = toBeInserted
                        placeholderHandler.insertPlaceholderAt(start + p.position + offset, p.text.length)
                        offset += toBeInserted.length
                    }

                    if (r.hasContinuation) {
                        this@PseuCoArea[start + offset + r.template.length] = PLACEHOLDER.toString()
                        placeholderHandler.insertPlaceholderAt(start + offset + r.template.length, 0)
                    }

                    r.placeholders.firstOrNull()?.also { placeholder ->
                        val placeholderStart = start + placeholder.position + if (placeholder.expand) {
                            calculateLeadingSpaces(start) + if (r.template[placeholder.position - 1] == '{') TAB_SPACES + 1 else 0
                        } else 0
                        val placeholderEnd = placeholderStart + placeholder.text.length
                        placeholderHandler.getLengthForPlaceholderAt(placeholderEnd)
                        this@PseuCoArea.deleteText(placeholderEnd, placeholderEnd + 1)
                        this@PseuCoArea.selectRange(placeholderStart, placeholderEnd)
                    }
                }
            }
        }

        val wordPosition = offsetToPosition(start, TwoDimensional.Bias.Forward)
        val menuPosition = localToScreen(
                CHARACTER_WIDTH * wordPosition.minor + LINE_NUMBER_WIDTH - estimatedScrollX,
                CHARACTER_HEIGHT * (wordPosition.major + 1) - estimatedScrollY
        )

        contextMenu.show(this, menuPosition.x, menuPosition.y)
    }

    // Rich text change handling

    /**
     * Handler for rich text changes.
     * Calculates syntax highlighting.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     * @param change The rich text change to be handled.
     * @see org.fxmisc.richtext.model.RichTextChange
     */
    @Suppress("UNUSED_PARAMETER")
    private fun onRichTextChange(change: RichTextChange<Collection<String>, String, Collection<String>>) =
            super.setStyleSpans(0, computeHighlighting())

    /**
     * Computes the pseuCo syntax highlighting.
     *
     * Based on [RichTextFX Java example](https://github.com/TomasMikula/RichTextFX/blob/master/richtextfx-demos/src/main/java/org/fxmisc/richtext/demo/JavaKeywords.java).
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    private fun computeHighlighting(): StyleSpans<Collection<String>> {
        val matcher = SYNTAX_PATTERN.matcher(text)

        var lastKwEnd = 0
        val spansBuilder = StyleSpansBuilder<Collection<String>>()

        while (matcher.find()) {
            val styleClass = when {
                matcher.group("KEYWORD") != null -> "keyword"
                matcher.group("PAREN") != null -> "paren"
                matcher.group("BRACE") != null -> "brace"
                matcher.group("BRACKET") != null -> "bracket"
                matcher.group("SEMICOLON") != null -> "semicolon"
                matcher.group("STRING") != null -> "string"
                matcher.group("COMMENT") != null -> "comment"
                matcher.group("CHANNEL") != null -> "keyword"
                matcher.group("INTEGER") != null -> "integer"
                matcher.group("OPERATOR") != null -> "operator"
                else -> null // Never happens.
            }!!

            spansBuilder.add(emptyList(), matcher.start() - lastKwEnd)
            spansBuilder.add(setOf(styleClass), matcher.end() - matcher.start())
            lastKwEnd = matcher.end()
        }

        spansBuilder.add(emptyList(), text.length - lastKwEnd)

        return spansBuilder.create()
    }

    /**
     * The content of this code area without possible invisible placeholders.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    val plainText: String
        get() = text.filterNot { it == PLACEHOLDER }.also { assert(it.length == length - placeholderHandler.count) }

    companion object {

        /**
         * The number of spaces used to replace tabs.
         *
         * @author Konstantin Kopper
         * @since 2.0.0
         */
        private const val TAB_SPACES = 4

        /**
         * The height of one character.
         * Used to calculate the position of the autocompletion context menu.
         *
         * @author Konstantin Kopper
         * @since 2.0.0
         */
        private const val CHARACTER_HEIGHT = 15.0

        /**
         * The width of a single character.
         * Used to calculate the position of the autocompletion context menu.
         *
         * @author Konstantin Kopper
         * @since 2.0.0
         */
        private const val CHARACTER_WIDTH = 7.9

        /**
         * The width of the line number column.
         * Used to calculate the position of the autocompletion context menu.
         *
         * @author Konstantin Kopper
         * @since 2.0.0
         */
        private const val LINE_NUMBER_WIDTH = 18.0

        /**
         * The character used as a placeholder for tabbing in auto completion.
         *
         * @author Konstantin Kopper
         * @since 2.0.0
         */
        private const val PLACEHOLDER = PlaceholderHandler.PLACEHOLDER

        /**
         * Tokens of the pseuCo language.
         * List taken from [pseuco.de](http://pseuco.de/downloads/PseuCo.html).
         *
         * @author Konstantin Kopper
         * @since 2.0.0
         */
        internal val PSEUCO_TOKEN: List<String> = Gson().fromJson(
                BufferedReader(InputStreamReader(this::class.java.getResourceAsStream("/pseucoKeywords.json"))),
                object : TypeToken<List<String>>() {}.type
        )

        // Regular expressions used for syntax highlighting
        private val KEYWORD_PATTERN = "\\b(${PSEUCO_TOKEN.joinToString("|")})\\b".toRegex()
        private val PAREN_PATTERN = "[()]".toRegex()
        private val BRACKET_PATTERN = "[{}]".toRegex()
        private val BRACE_PATTERN = "[]\\[]".toRegex()
        private val SEMICOLON_PATTERN = ";".toRegex()
        private val STRING_PATTERN = "\"[^\"]*\"".toRegex()
        private val COMMENT_PATTERN = "//.*|/\\*(.|\\R)*?\\*/".toRegex()
        private val CHANNEL_PATTERN = "(int|bool|string)chan\\d+".toRegex() // TODO maybe remove from tokens
        private val INTEGER_PATTERN = "\\b(\\d+)".toRegex()
        private val OPERATOR_PATTERN = "<[!?]".toRegex()

        /**
         * Regular expression combining all syntactic patterns to a single regex.
         * Uses groups to distinguish between those patterns.
         *
         * @author Konstantin Kopper
         * @since 2.0.0
         */
        private val SYNTAX_PATTERN = Pattern.compile(
                "(?<KEYWORD>$KEYWORD_PATTERN)" +
                        "|(?<PAREN>$PAREN_PATTERN)" +
                        "|(?<BRACKET>$BRACKET_PATTERN)" +
                        "|(?<BRACE>$BRACE_PATTERN)" +
                        "|(?<SEMICOLON>$SEMICOLON_PATTERN)" +
                        "|(?<STRING>$STRING_PATTERN)" +
                        "|(?<COMMENT>$COMMENT_PATTERN)" +
                        "|(?<CHANNEL>$CHANNEL_PATTERN)" +
                        "|(?<INTEGER>$INTEGER_PATTERN)" +
                        "|(?<OPERATOR>$OPERATOR_PATTERN)"
        )

        /**
         * The filter applied to rich change subscriptions.
         *
         * @return `True` if the change is not filtered out, `false` otherwise.
         * @author Konstantin Kopper
         * @since 2.0.0
         */
        private fun <PS, SEG, S> richChangeFilter(change: RichTextChange<PS, SEG, S>): Boolean = change.let { it.inserted != it.removed }

        /**
         * The filter applied to plain text change subscriptions.
         *
         * @return `True` if the change is not filtered out, `false` otherwise.
         * @author Konstantin Kopper
         * @since 2.0.0
         */
        private fun plainTextChangeFilter(change: PlainTextChange): Boolean = change.let { it.inserted != it.removed }
    }

    /**
     * Get the character at the specified position.
     *
     * @return A string with the character.
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    operator fun get(position: Int): String = get(position, position + 1)

    /**
     * Get the substring between the given positions.
     *
     * @param start The position where the desired substring start.
     * @param end The position where the desired substring ends (not included).
     * @return The substring in positions [[start], [end]).
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    operator fun get(start: Int, end: Int): String = try {
        getText(start, end)
    } catch (e: Exception) {
        ""
    }

    /**
     * Insert new text at the given position.
     *
     * @param position The position where the text should be inserted.
     * @param text The text to be inserted.
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    operator fun set(position: Int, text: String) = insertText(position, text)

    /**
     * Insert [character] at [position].
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    operator fun set(position: Int, character: Char) = set(position, character.toString())

    /**
     * Append text to this [CodeArea].
     *
     * @param text The text  to be appended.
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    operator fun plusAssign(text: String) = appendText(text)

    /**
     * Deletes the character at [position].
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    private fun deleteCharAt(position: Int) = deleteText(position, position + 1)
}
