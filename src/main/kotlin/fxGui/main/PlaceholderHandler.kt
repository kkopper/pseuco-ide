package fxGui.main

/**
 * Manages placeholders occurring in [codeArea].
 *
 * @author Konstantin Kopper
 * @since 2.0.0
 * @constructor Creates a new placeholder manager.
 */
internal class PlaceholderHandler(private val codeArea: PseuCoArea) {

    /**
     * Internal list managing the stored `length` values.
     * They are stored in the order of the corresponding placeholders occurring in [codeArea].
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    private val tabs = mutableListOf<Int>()

    /**
     * Number of placeholders for which a `length` value is stored.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    val count: Int
        get() = tabs.size

    /**
     * Checks whether [codeArea] has a placeholder starting at [position]` + 1`.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    fun hasPlaceholderAfter(position: Int) = tabs.size - countPlaceholdersUntil(position) > 0

    /**
     * Calculate the position of the next placeholder in [[position], `codeArea.length`).
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     * @return The position of the next placeholder, or -1 if there is no one left.
     */
    fun findNextPlaceholder(position: Int): Int {
        require(position >= 0 && position < codeArea.length) { "Invalid position" }
        for (i in position until codeArea.length)
            if (codeArea[i] == PLACEHOLDER.toString())
                return i
        return -1
    }

    /**
     * Get the stored length for the placeholder at [position].
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     * @throws IllegalArgumentException No [PLACEHOLDER] found at [position].
     */
    fun getLengthForPlaceholderAt(position: Int): Int {
        require(codeArea[position] == PLACEHOLDER.toString()) { "No placeholder found at position" }
        val index = countPlaceholdersUntil(position)
        return tabs.removeAt(index)
    }

    /**
     * Stores [length] for [PLACEHOLDER] at [position].
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     * @throws IllegalArgumentException If [PLACEHOLDER] is not found at [position].
     */
    fun insertPlaceholderAt(position: Int, length: Int) {
        require(position >= 0 && position <= codeArea.length) { "Invalid position" }
        // require(codeArea[position] == PLACEHOLDER.toString()) { "Missing placeholder at given position" }

        val index = countPlaceholdersUntil(position)
        tabs.add(index, length)
    }

    /**
     * Deletes all `length` values for every [PLACEHOLDER] character in [[start], [end]).
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     * @throws IllegalArgumentException If `[start] < 0`, `[end] > [codeArea].length` or `[start] > [end]`.
     */
    fun deletePlaceholdersAt(start: Int, end: Int): Int {
        require(start >= 0) { "Start position should be positive" }
        require(end <= codeArea.length) { "End position should be less than the code areas text length." }
        require(start <= end) { "Start should be less or equal to end." }

        val index1 = countPlaceholdersUntil(start)
        val index2 = countPlaceholdersUntil(end - 1)
        assert(index1 <= index2)

        (index1 until index2).forEach { tabs.removeAt(index1) }

        return index2 - index1
    }

    /**
     * Deletes the [count] next placeholder characters starting at [position].
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     * @throws IllegalArgumentException If [position]` < 0`, [position]` >= `[codeArea]`.length`
     *                                  or [count] exceeds the number of stored values.
     */
    fun deletePlaceholdersFrom(position: Int, count: Int) {
        require(position >= 0 && position < codeArea.length)
        require(count <= tabs.size)

        val index = countPlaceholdersUntil(position)
        assert(tabs.size - count - index >= 0) {
            "Would remove more elements than available: $count of ${tabs.size - index} left elements."
        }

        repeat(count) { tabs.removeAt(index) }
    }

    /**
     * Count the number of [PLACEHOLDER]s in [0, [position]).
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    private fun countPlaceholdersUntil(position: Int) =
            codeArea.text.let { it.dropLast(it.length - position) }.count { it == PLACEHOLDER }

    companion object {
        /**
         * The character used as a placeholder.
         *
         * @author Konstantin Kopper
         * @since 2.0.0
         */
        const val PLACEHOLDER = '\u200c'
    }
}
