package fxGui.main

import java.io.File

/**
 * Wrapper for files combined with a name, i.e. a string.
 * Useful to handle files in GUI environments combined with a different display name.
 *
 * @author Konstantin Kopper
 * @see File
 * @property file The file to be wrapped.
 * @property name The additional name for the wrapped file.
 * @constructor Creates a new instance with the given file and the given string.
 */
class FileWrapper(val file: File, val name: String) {

    /**
     * Convenience constructor.
     * The name of the given file will be used as the second string property.
     * @property file The file to be wrapped.
     */
    constructor(file: File) : this(file, file.name)

    /**
     * Creates a string representation, i.e. returns the string parameter.
     * @return The string parameter.
     */
    override fun toString(): String {
        return name
    }
}
