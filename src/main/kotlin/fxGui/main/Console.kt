package fxGui.main

import javafx.application.Platform
import javafx.scene.control.TextArea
import java.io.IOException
import java.io.OutputStream

/**
 * Wrapper around the console text area. Implements an [OutputStream], such that text can be printed out.
 *
 * @author Konstantin Kopper
 * @since 2.0.0
 * @property consoleTextArea The text area representing the console.
 */
internal class Console(private val consoleTextArea: TextArea) : OutputStream() {

    @Throws(IOException::class)
    override fun write(b: Int) = Platform.runLater { this.consoleTextArea.appendText(String(byteArrayOf((b and 0xff).toByte()))) }

    /**
     * Clears the console.
     */
    fun clear() = this.consoleTextArea.clear()
}
