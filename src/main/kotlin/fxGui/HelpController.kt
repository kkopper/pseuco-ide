package fxGui

import codeGen.MainCodeGen
import com.pseuco.api.PseuCoShare
import javafx.fxml.FXML
import javafx.fxml.FXMLLoader
import javafx.scene.Scene
import javafx.scene.control.Hyperlink
import javafx.scene.control.Label
import javafx.scene.image.Image
import javafx.scene.image.ImageView
import javafx.scene.layout.AnchorPane
import javafx.scene.text.Text
import javafx.scene.text.TextFlow
import javafx.stage.Stage
import properties.AboutProperties
import properties.BuildProperties
import webapp.CompanionWebApp
import java.net.URI

/**
 * The controller for the help dialog.
 *
 * @author Konstantin Kopper
 */
class HelpController : AnchorPane() {

    @FXML
    private lateinit var titleImage: ImageView

    @FXML
    private lateinit var descriptionLabel: Label

    @FXML
    private lateinit var versionLabel: Label

    @FXML
    private lateinit var copyrightLabel: Label

    @FXML
    private lateinit var licenseLabel: Label

    @FXML
    private lateinit var repositoryLink: Hyperlink

    @FXML
    private lateinit var companionLink: Hyperlink

    @FXML
    private lateinit var helpText: TextFlow

    /**
     * The URL of the IDEs repository.
     * Will be passed to browser if `source code`-button is clicked.
     *
     * @author Konstantin Kopper
     * @see repositoryLink
     * @see showSourceCode
     */
    private val repo: String by AboutProperties

    init {
        FXMLLoader(javaClass.getResource("/fxGui/fxml/help.fxml")).apply {
            setRoot(this@HelpController)
            setController(this@HelpController)
            load()
        }

        titleImage.image = Image(javaClass.getResource("/fxGui/images/logo_help.png").toExternalForm())

        descriptionLabel.text = AboutProperties.description
        versionLabel.text = "Version ${AboutProperties.version}"
        copyrightLabel.text = "Copyright \u00A9 ${AboutProperties.copyright}"
        licenseLabel.text = AboutProperties.license

        repositoryLink.apply {
            graphic = Label(FontAwesome.FA_GITLAB).apply { style = "-fx-font-family: FontAwesome" }
        }

        val pipeline = BuildProperties.pipeline

        if (pipeline.isNotEmpty()) {
            helpText.children.also {
                it += Text("Build ")
                it += Text("#$pipeline").apply { styleClass += "mono" }
                it += Text("\n")
            }
        } else
            helpText.children += Text("You are using a custom build.\n")

        val branch = BuildProperties.branch
        val commit = BuildProperties.commit

        if (branch.isNotEmpty() && commit.isNotEmpty()) {
            helpText.children.also {
                it += Text("\nCommit ")
                it += Text(commit).apply { styleClass += "mono" }
                it += Text("\nBranch ")
                it += Text(branch).apply { styleClass += "mono" }
                it += Text("\n")
            }
        }

        helpText.children +=
            Text("\nUsing modified pseuco-java-compiler based on version ${MainCodeGen.version}.")

        if (!CompanionWebApp.isActive)
            companionLink.isDisable = true

        styleProperty().bind(ZoomEventHandler.fxString)
    }

    /**
     * Opens the IDEs repository in the default browser.
     *
     * @author Konstantin Kopper
     */
    @FXML
    fun showSourceCode() {
        PseuCoShare.openInBrowser(URI.create(this.repo))
    }

    /**
     * Shows a dialog with the license text.
     *
     * @author Konstantin Kopper
     */
    @FXML
    fun showLicense() {
        Stage().apply {
            title = "pseuCo IDE - License"
            scene = Scene(TextPane(this@HelpController::class.java.getResource("/licenses/LICENSE")))
            icons += Image(this@HelpController::class.java.getResource("/fxGui/images/icon.png").toExternalForm())
        }.showAndWait()
    }

    /**
     * Opens the [CompanionWebApp] of the IDE.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    @FXML
    fun openCompanion() = CompanionWebApp.open()
}
