package fxGui

import javafx.beans.binding.Bindings
import javafx.beans.property.IntegerProperty
import javafx.beans.property.SimpleIntegerProperty
import javafx.beans.value.ObservableStringValue
import javafx.event.EventHandler
import javafx.scene.input.KeyCharacterCombination
import javafx.scene.input.KeyCode
import javafx.scene.input.KeyCodeCombination
import javafx.scene.input.KeyEvent
import kotlin.reflect.KProperty

/**
 * Handler of key events.
 * Consumes `Ctrl + +`, `Ctrl + -` and `Ctrl + 0`:
 *  * Increases [fontSize] on `Ctrl + +`.
 *  * Decreases [fontSize] on `Ctrl + -`.
 *  * Resets [fontSize] to 100 on `Ctrl + 0`.
 *
 * @author Konstantin Kopper
 * @since 2.0.0
 * @see KeyEvent
 * @see EventHandler
 */
object ZoomEventHandler : EventHandler<KeyEvent> {

    /**
     * JavaFX property storing the current font size in percent, where 100% is the default.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    private val fontSizeProperty = SimpleIntegerProperty(100)

    /**
     * The current font size in percent, where 100% is both the default and the initial value.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    var fontSize by fontSizeProperty

    /**
     * Observable CSS string defining the current font size.
     * May be used to be bound to other properties, such that changes to [fontSize] are respected by the GUI.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    val fxString: ObservableStringValue = Bindings.format("-fx-font-size: %d%%;", fontSizeProperty)

    /**
     * [KeyCodeCombination] of [KeyCode.PLUS] and [KeyCodeCombination.SHORTCUT_DOWN], hence `Ctrl + +`.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    private val plusCodeCombination = KeyCodeCombination(KeyCode.PLUS, KeyCodeCombination.SHORTCUT_DOWN)

    /**
     * [KeyCharacterCombination] of `+` and [KeyCodeCombination.SHORTCUT_DOWN], hence `Ctrl + +`.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    private val plusCharCombination = KeyCharacterCombination("+", KeyCodeCombination.SHORTCUT_DOWN)

    /**
     * [KeyCodeCombination] of [KeyCode.MINUS] and [KeyCodeCombination.SHORTCUT_DOWN], hence `Ctrl + -`.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    private val minusCodeCombination = KeyCodeCombination(KeyCode.MINUS, KeyCodeCombination.SHORTCUT_DOWN)

    /**
     * [KeyCharacterCombination] of `-` and [KeyCodeCombination.SHORTCUT_DOWN], hence `Ctrl + -`.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    private val minusCharCombination = KeyCharacterCombination("-", KeyCodeCombination.SHORTCUT_DOWN)

    /**
     * [KeyCodeCombination] of [KeyCode.DIGIT0] and [KeyCodeCombination.SHORTCUT_DOWN], hence `Ctrl + 0`.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    private val resetCombination = KeyCodeCombination(KeyCode.DIGIT0, KeyCodeCombination.SHORTCUT_DOWN)

    override fun handle(event: KeyEvent) {
        when {
            plusCodeCombination.match(event) || plusCharCombination.match(event) -> {
                fontSize += when {
                    fontSize < 100 -> 10
                    fontSize < 200 -> 25
                    else -> 50
                }
                event.consume()
            }
            minusCodeCombination.match(event) || minusCharCombination.match(event) -> {
                fontSize -= when {
                    fontSize <= 10 -> 0 // Avoid negative font size, as this resets the size to the default.
                    fontSize <= 100 -> 10
                    fontSize <= 200 -> 25
                    else -> 50
                }
                event.consume()
            }
            resetCombination.match(event) -> {
                fontSize = 100
                event.consume()
            }
        }
    }

    operator fun IntegerProperty.getValue(thisRef: Any?, property: KProperty<*>): Int = get()
    operator fun IntegerProperty.setValue(thisRef: Any?, property: KProperty<*>, value: Int) = set(value)
}
