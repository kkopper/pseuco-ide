package fxGui.share

import javafx.fxml.FXML
import javafx.fxml.FXMLLoader
import javafx.scene.control.TextField
import javafx.scene.layout.AnchorPane
import java.awt.Desktop
import java.net.URI

/**
 * Simple pane only displaying a URL.
 *
 * @author Konstantin Kopper
 * @since 2.0.0
 * @param uri The URI to be displayed.
 * @constructor Creates a new instance of this pane.
 */
class URLAlert(private val uri: URI) : AnchorPane() {

    /**
     * The [TextField] used to actually display [uri].
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    @FXML
    private lateinit var urlField: TextField

    init {
        FXMLLoader(this::class.java.getResource("/fxGui/fxml/alert_url.fxml")).apply {
            setRoot(this@URLAlert)
            setController(this@URLAlert)
            load()
        }
        urlField.apply {
            text = "$uri"
            prefColumnCount = text.length
        }
    }

    /**
     * Opens [uri] in the systems default browser.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    @FXML
    private fun openURL() = Desktop.getDesktop().browse(uri)
}
