package fxGui.share

import com.pseuco.PseuCoFileSharer
import com.pseuco.websocket.PseuCoWebSocket
import config.Config
import fxGui.FontAwesome
import javafx.event.ActionEvent
import javafx.fxml.FXML
import javafx.fxml.FXMLLoader
import javafx.scene.control.Button
import javafx.scene.control.Hyperlink
import javafx.scene.control.Label
import javafx.scene.layout.AnchorPane
import javafx.stage.Stage
import webapp.CompanionWebApp
import java.awt.Desktop
import java.net.URI

/**
 * Pane to be shown whenever the sharing method needs to be selected.
 *
 * @author Konstantin Kopper
 * @since 2.0.0
 * @param stage The stage holding this pane. Required to close it on button presses.
 * @constructor Create a new pane.
 */
class SharePane(private val stage: Stage) : AnchorPane() {

    /**
     * The label displaying the icon.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    @FXML
    private lateinit var iconLabel: Label

    /**
     * The `cancel` button.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    @FXML
    private lateinit var cancelButton: Button

    /**
     * The button representing the sharing API.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    @FXML
    private lateinit var sharingButton: Button

    /**
     * The button representing the WebSockets based method.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    @FXML
    private lateinit var socketsButton: Button

    init {
        FXMLLoader(this::class.java.getResource("/fxGui/fxml/share.fxml")).apply {
            setRoot(this@SharePane)
            setController(this@SharePane)
            load()
        }

        iconLabel.apply {
            text = FontAwesome.FA_EXTERNAL_LINK
        }

        // Bind default property of the buttons to trigger the button action on 'Enter'.
        cancelButton.defaultButtonProperty().bind(cancelButton.focusedProperty())
        sharingButton.defaultButtonProperty().bind(sharingButton.focusedProperty())
        socketsButton.defaultButtonProperty().bind(socketsButton.focusedProperty())
    }

    /**
     * Open the help page explaining the sharing methods in the [CompanionWebApp].
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    @FXML
    private fun showMethods(e: ActionEvent) {
        assert(e.source is Hyperlink)
        Desktop.getDesktop().browse(URI.create("${CompanionWebApp.uri}/help/sharing"))
    }

    /**
     * Update the config with the selected sharing process. Called by [sharingButton] and [socketsButton].
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    @FXML
    private fun buttonPressed(e: ActionEvent) {
        assert(e.source is Button)
        Config.apply {
            this.useWebSocket = when (e.source) {
                sharingButton -> {
                    PseuCoFileSharer.useAPI()
                    false
                }
                socketsButton -> {
                    PseuCoFileSharer.useServer(PseuCoWebSocket)
                    true
                }
                else -> return // Should never happen.
            }
            save()
        }
        stage.close()
    }

    /**
     * Closes [stage]. Called by [cancelButton].
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    @FXML
    private fun cancel(e: ActionEvent) {
        assert(e.source == cancelButton)
        stage.close()
    }
}
