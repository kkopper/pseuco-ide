package fxGui.share

import javafx.event.ActionEvent
import javafx.fxml.FXML
import javafx.fxml.FXMLLoader
import javafx.scene.control.Button
import javafx.scene.control.TextField
import javafx.scene.layout.AnchorPane
import javafx.stage.Stage

/**
 * View presenting a text field used to enter a pseuCo.com sharing URL.
 *
 * @author Konstantin Kopper
 * @since 2.0.0
 * @param stage The stage hosting this view. Required to close it.
 * @constructor Creates a new import view.
 */
class ImportPane(private val stage: Stage) : AnchorPane() {

    /**
     * The text field used to input the identifier or the URL.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    @FXML
    private lateinit var idInput: TextField

    /**
     * Calls [cancel].
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    @FXML
    private lateinit var cancelButton: Button

    /**
     * Calls [importPressed].
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    @FXML
    private lateinit var importButton: Button

    /**
     * The identifier entered in [idInput] or `null` if [cancelButton] was pressed.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    var result: String? = null

    init {
        FXMLLoader(this::class.java.getResource("/fxGui/fxml/import.fxml")).apply {
            setRoot(this@ImportPane)
            setController(this@ImportPane)
            load()
        }

        // Listener removing URL prefix (if available)
        idInput.textProperty().addListener { _, _, newValue ->
            idInput.text = SHARING_URL_REGEX.matchEntire(newValue)?.groups?.get(1)?.value ?: newValue
        }
    }

    /**
     * Closes [stage].
     * Called by [cancelButton].
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    @FXML
    private fun cancel(e: ActionEvent) {
        assert(e.source == cancelButton)
        stage.close()
    }

    /**
     * Sets [result] and closes [stage].
     * Called by [importButton].
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    @FXML
    private fun importPressed(e: ActionEvent) {
        assert(e.source == importButton)
        result = idInput.text
        stage.close()
    }

    companion object {
        /**
         * Regular expression matching pseuCo.com file sharing URLs.
         *
         * @author Konstantin Kopper
         * @since 2.0.0
         */
        private val SHARING_URL_REGEX = Regex("^https?://pseuco\\.com/#/edit/remote/(\\w{20})$")
    }
}
