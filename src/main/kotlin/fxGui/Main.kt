package fxGui

import com.pseuco.PseuCoFileSharer
import com.pseuco.api.PseuCoShare
import com.pseuco.dataraces.PseuCoDRD
import com.pseuco.websocket.PseuCoWebSocket
import config.Config
import fxGui.main.MainPane
import javafx.application.Application
import javafx.application.Platform
import javafx.scene.Scene
import javafx.scene.control.Alert
import javafx.scene.image.Image
import javafx.stage.DirectoryChooser
import javafx.stage.Stage
import kotlinx.coroutines.runBlocking
import updates.UpdateCheckException
import updates.Version
import updates.VersionCheck
import util.Workspace
import webapp.CompanionWebApp
import java.io.File
import java.io.IOException
import java.io.PrintStream
import java.net.BindException
import javax.tools.ToolProvider
import kotlin.concurrent.thread
import kotlin.system.exitProcess

/**
 * Main application class.
 *
 * @author Konstantin Kopper
 * @since 2.0.0
 */
internal class Main : Application() {

    override fun start(primaryStage: Stage?) {
        try {
            PseuCoWebSocket.start()
        } catch (e: BindException) {
            PseucoAlert(Alert.AlertType.WARNING).apply {
                title = "Could not start the WebSocket server"
                contentText = "Starting the IDEs WebSocket server failed: The port is already used. " +
                    "Maybe you have already a running instance of the IDE? " +
                    "As a consequence, this instance always uses the sharing API for interacting with pseuCo.com."
            }.showAndWait()
            PseuCoFileSharer.enforceAPI = true
        } catch (e: Exception) {
            ExceptionAlert(e).showAndWait()
            return
        }

        try {
            CompanionWebApp.start()
        } catch (e: BindException) {
            PseucoAlert(Alert.AlertType.WARNING).apply {
                title = "Could not start the companion web application"
                contentText = "Starting the companion web application failed: The port is already used. " +
                    "Maybe you have already a running instance of the IDE?"
            }.showAndWait()
            CompanionWebApp.isActive = false
        } catch (e: Exception) {
            ExceptionAlert(e).showAndWait()
            return
        }

        val workspace = if ("workspace" in Config) {
            // Load workspace from config
            var ws = File(Config.workspace)
            if (!ws.exists() || !ws.isDirectory) {
                // The stored path is not valid, choose a new one.
                PseucoAlert(Alert.AlertType.WARNING).apply {
                    title = "Invalid workspace"
                    contentText = "The stored workspace '${ws.absolutePath}' does not exist. Please choose a new one."
                }.showAndWait()
                ws = DirectoryChooser().apply {
                    title = "Update workspace"
                    initialDirectory = Workspace.DEFAULT
                }.showDialog(primaryStage)?.also {
                    Config.workspace = it.absolutePath
                    Config.save()
                } ?: exitProcess(0)
            }
            ws
        } else {
            // Choose workspace
            DirectoryChooser().apply {
                title = "Choose Workspace"
                initialDirectory = Workspace.DEFAULT
            }.showDialog(primaryStage).takeUnless { it == null }?.also {
                Config.workspace = it.absolutePath
                Config.save()
            } ?: exitProcess(0)
        }

        assert(workspace.exists())
        assert(workspace.isDirectory)

        Workspace.change(workspace)
        Workspace.onChange = { _, new ->
            Config.workspace = new.absolutePath
            Config.save()
        }

        if (Config.useWebSocket)
            PseuCoFileSharer.useServer(PseuCoWebSocket)
        else
            PseuCoFileSharer.useAPI()

        // Main frame
        val controller = try {
            MainPane(primaryStage as Stage)
        } catch (e: IOException) {
            if (Main.DEBUG) {
                e.printStackTrace(Main.STANDARD_ERR)
            }
            ExceptionAlert(e).showAndWait()
            return
        }

        // Check if the Java compiler is available
        if (ToolProvider.getSystemJavaCompiler() == null) {
            if (!Config.skipJdkWarning) {
                val alert = DoNotShowAgainAlert(Alert.AlertType.WARNING).apply {
                    title = "No Java compiler found!"
                    contentText = "To execute pseuCo programs, the IDE requires a Java compiler. " +
                        "If you have a JDK installed, you may start the IDE using the command line. " +
                        "You can still use any function not requiring a Java compiler."
                }.also { it.showAndWait() }

                if (alert.doNotShowAgain) {
                    // Warning should no longer be displayed, hence update the config file.
                    Config.skipJdkWarning = true
                    Config.save()
                }
            }

            controller.disableJavaCompilerTasks()
        }

        primaryStage.apply {
            title = "pseuCo IDE"
            scene = Scene(controller).also { controller.setShortcuts(it) }
            icons += Image(this@Main.javaClass.getResource("/fxGui/images/icon.png").toExternalForm())
        }.show()
    }

    override fun stop() {
        super.stop()
        PseuCoShare.close()
        VersionCheck.close()
        PseuCoWebSocket.stop()
        CompanionWebApp.stop()
        PseuCoDRD.leave()
    }

    companion object {
        /**
         * The stylesheet containing the general settings.
         */
        internal val GENERAL_STYLESHEET = Main::class.java.getResource("/fxGui/css/general.css").toExternalForm()

        /**
         * Flag indicating if the IDE was started in debug mode.
         */
        @JvmField
        var DEBUG = false

        /**
         * The standard output stream.
         * Useful for console output.
         */
        @JvmField
        val STANDARD_OUT: PrintStream = System.out

        /**
         * The standard error stream.
         * Useful for console output.
         */
        @JvmField
        val STANDARD_ERR: PrintStream = System.err
    }
}

/**
 * Main method.
 * Parses the command line arguments and starts the GUI.
 *
 * @author Konstantin Kopper
 * @param args The command line arguments.
 */
fun main(args: Array<String>) {
    if (args.isNotEmpty() && (args[0] == "-d" || args[0] == "--debug")) {
        Main.DEBUG = true

        // Enable assertions
        ClassLoader.getSystemClassLoader().setDefaultAssertionStatus(true)
    }

    // Check for updates.
    if (VersionCheck.isOfficialRelease) {
        thread {
            val update = try {
                runBlocking { VersionCheck.checkForUpdate() }
            } catch (e: UpdateCheckException) {
                if (Main.DEBUG)
                    e.printStackTrace()
                else
                    println(e.localizedMessage)

                return@thread
            }

            if (update) {
                Platform.runLater {
                    PseucoAlert(Alert.AlertType.INFORMATION).apply {
                        title = "Update available"
                        contentText = "A new release of the IDE is available: " +
                            "${VersionCheck.newest.asString} (you are using ${Version.current.asString}). " +
                            "Visit pseuCo.com to download it."
                    }.showAndWait()
                }
            }
        }
    }

    Application.launch(Main::class.java, *args)
}
