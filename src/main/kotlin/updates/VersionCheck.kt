package updates

import com.google.gson.FieldNamingPolicy
import fxGui.Main
import io.ktor.client.HttpClient
import io.ktor.client.engine.apache.Apache
import io.ktor.client.features.json.GsonSerializer
import io.ktor.client.features.json.JsonFeature
import io.ktor.client.request.accept
import io.ktor.client.request.get
import io.ktor.http.ContentType
import io.ktor.http.URLProtocol
import io.ktor.http.withCharset
import org.gitlab.api.v4.tags.Tag
import properties.BuildProperties
import updates.VersionCheck.checkForUpdate
import updates.VersionCheck.isOfficialRelease
import updates.VersionCheck.lastCheck
import updates.VersionCheck.newest
import java.time.LocalDateTime

/**
 * Singleton encapsulating the update check.
 * Check [isOfficialRelease], call [checkForUpdate] and use [newest] and [lastCheck].
 *
 * @author Konstantin Kopper
 * @since 2.1.0
 */
object VersionCheck {

    /**
     * Regular expression identifying official releases.
     *
     * @author Konstantin Kopper
     * @since 2.1.0
     */
    private val versionRegex = Regex("^v\\d+\\.\\d+(\\.\\d+)?$")

    /**
     * `True` if this instance is an official release, `false` otherwise.
     *
     * @author Konstantin Kopper
     * @since 2.1.0
     */
    val isOfficialRelease by lazy { BuildProperties.branch.matches(versionRegex) }

    /**
     * The newest available [Version]. Only set if [checkForUpdate] returned `true`.
     *
     * @author Konstantin Kopper
     * @since 2.1.0
     */
    lateinit var newest: Version
        private set

    /**
     * The point in time the last update check was performed, `null` if never.
     * Useful in GUI environments.
     *
     * @author Konstantin Kopper
     * @since 2.1.0
     * @see checkForUpdate
     */
    var lastCheck: LocalDateTime? = null
        private set

    /**
     * The HTTP client used to communicate with the GitLab API.
     * Can parse JSON responses automatically.
     *
     * @author Konstantin Kopper
     * @since 2.1.0
     */
    private val client by lazy {
        HttpClient(Apache) {
            install(JsonFeature) {
                serializer = GsonSerializer {
                    setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                }
            }
        }
    }

    fun close() = client.close()

    /**
     * A list of available [Version]s. Set by [fetchVersions].
     *
     * @author Konstantin Kopper
     * @since 2.1.0
     */
    private lateinit var versions: List<Version>

    /**
     * Queries the GitLab API and checks whether a newer tag (and hence a newer version) is available.
     * Only to be called if [isOfficialRelease] is `true`.
     *
     * Sets [newest] (if an update is available) and [lastCheck] (always, unless an exception is thrown).
     *
     * @author Konstantin Kopper
     * @since 2.1.0
     * @throws UpdateCheckException Something went wrong.
     * @return `true` if a newer version is available, `false` otherwise.
     */
    @Throws(UpdateCheckException::class)
    suspend fun checkForUpdate(): Boolean {
        assert(isOfficialRelease) { "Do not check for updates if you are not using an official release!" }

        val newest = try {
            fetchVersions()
            versions.last()
        } catch (e: Exception) {
            throw UpdateCheckException("Fetching newest version failed.", e)
        }

        lastCheck = LocalDateTime.now()

        if (newest > Version.current) {
            if (Main.DEBUG)
                println("A newer version (${newest.asString} > ${Version.current.asString}) is available!")
            this.newest = newest
            return true
        }

        assert(newest == Version.current)
        return false
    }

    /**
     * Fetches the available versions. Uses [fetchTags], converts the resulting list and assigns it to [versions].
     *
     * @author Konstantin Kopper
     * @since 2.1.0
     */
    private suspend fun fetchVersions() {
        versions = fetchTags()
            .filter { it.name.matches(versionRegex) }
            .map { Version.fromString(it.name.substring(1)) }
            .sortedWith(Comparator { o1, o2 -> o1.compareTo(o2) })
    }

    /**
     * Queries the GitLab API for a list of [Tag]s.
     *
     * @author Konstantin Kopper
     * @since 2.1.0
     */
    private suspend fun fetchTags(): List<Tag> = client.get {
        url {
            protocol = URLProtocol.HTTPS
            host = GITLAB_HOST
            port = protocol.defaultPort
            path("api", "v4", "projects", "$GITLAB_PROJECT_ID", "repository", "tags")
        }
        accept(ContentType.Application.Json.withCharset(Charsets.UTF_8))
    }

    /**
     * The hostname of the GitLab instance.
     *
     * @author Konstantin Kopper
     * @since 2.1.0
     */
    private const val GITLAB_HOST = "dgit.cs.uni-saarland.de"

    /**
     * The internal project id used by GitLab.
     *
     * @author Konstantin Kopper
     * @since 2.1.0
     */
    private const val GITLAB_PROJECT_ID = 16
}
