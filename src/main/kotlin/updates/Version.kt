package updates

import properties.AboutProperties

/**
 * A version matching the [semantic versioning](https://semver.org) scheme.
 *
 * @author Konstantin Kopper
 * @since 2.1.0
 */
data class Version(val major: Int, val minor: Int, val revision: Int = 0) {

    init {
        require(major >= 0) { "'major' needs to be non-negative." }
        require(minor >= 0) { "'minor' needs to be non-negative." }
        require(revision >= 0) { "'revision' needs to be non-negative." }
    }

    /**
     * A string representation: `[major].[minor].[revision]`
     *
     * @author Konstantin Kopper
     * @since 2.1.0
     */
    val asString = "$major.$minor.$revision"

    /**
     * Compares two versions.
     * Returns
     *  * `± 100` if [major] differs,
     *  * `± 10` if [major] is equal and [minor] differs,
     *  * `± 1` if both [major] and [minor] are equals and [revision] differs,
     *  * `0` otherwise.
     *
     * @author Konstantin Kopper
     * @since 2.1.0
     */
    operator fun compareTo(other: Version): Int = when {
        major > other.major -> 100
        major < other.major -> -100
        minor > other.minor -> 10
        minor < other.minor -> -10
        revision > other.revision -> 1
        revision < other.revision -> -1
        else -> 0
    }

    companion object {

        /**
         * The version of this instance as defined by [AboutProperties].
         *
         * @author Konstantin Kopper
         * @since 2.1.0
         */
        val current by lazy { Version.fromString(AboutProperties.version) }

        /**
         * Creates a new instance by extracting the relevant information from a [String].
         * This [str] must match against the following regex: "`^\d+\.\d+(\.\d+)?$`"
         *
         * @author Konstantin Kopper
         * @since 2.1.0
         */
        fun fromString(str: String): Version = str
            .also { require(it.matches(Regex("^\\d+\\.\\d+(\\.\\d+)?$"))) }
            .split('.')
            .map { it.toInt() }
            .let { if (it.size == 3) Version(it[0], it[1], it[2]) else Version(it[0], it[1]) }
    }
}
