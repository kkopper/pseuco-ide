package updates

class UpdateCheckException(message: String? = null, cause: Throwable? = null) : Exception(message, cause)
