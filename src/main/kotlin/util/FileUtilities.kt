package util

import java.io.File

/**
 * Collection of utility functions concerning the file system.
 *
 * @author Konstantin Kopper
 * @since 2.0.0
 */
object FileUtilities {

    /**
     * Regular expression identifying windows operating systems.
     */
    private val winRegex = Regex("^[Ww]in.*$")

    /**
     * Regular expression identifying macOS.
     */
    private val macRegex = Regex("^[Mm]ac.*$")

    /**
     * Regular expression identifying linux machines.
     */
    private val linuxRegex = Regex("^[Ll]inux.*$")

    /**
     * The file separator used by the current platform.
     */
    @JvmStatic val fileSeparator: String = System.getProperty("file.separator")

    /**
     * Returns the platform independent path of the application data folder.
     * If your operating system is neither Windows nor macOS nor Linux, the current working directory is returned.
     *
     * @return The platform independent path of the application data folder.
     * @author Konstantin Kopper
     */
    @JvmStatic fun getAppData(): String {
        val os = System.getProperty("os.name")

        // Workaround for simpler "when" statement
        // https://discuss.kotlinlang.org/t/using-regex-in-a-when/1794/6
        operator fun Regex.contains(text: CharSequence): Boolean = this.matches(text)

        return when (os) {
            in this.winRegex -> System.getenv("APPDATA")
            in this.macRegex -> assembleFilePath(System.getProperty("user.home"), "Library", "Application Support")
            in this.linuxRegex -> assembleFilePath(System.getProperty("user.home"), ".local", "share")
            else -> System.getProperty("user.dir")
        }
    }

    /**
     * Returns a file representing the applications data folder.
     * Creates the folder if not existing yet.
     *
     * @return A platform independent file pointing at the applications data folder.
     * @author Konstantin Kopper
     */
    @JvmStatic fun getAppDataFolder(): File = File(getAppData(), "pseuCo IDE").also { it.mkdirs() }

    /**
     * Returns a file from inside the applications data folder.
     * Creates the file if not existing yet.
     *
     * @param filename The name of the desired file.
     * @return The file inside the application data folder.
     * @author Konstantin Kopper
     */
    @JvmStatic fun getAppDataFile(filename: String): File = File(getAppDataFolder(), filename).also { it.createNewFile() }

    /**
     * Joins the components of a filepath with the platform specific separator.
     * Created filepath is always terminated by the platform specific separator.
     *
     * @param components The single components of the filepath.
     * @return A filepath created from the components.
     * @author Konstantin Kopper
     */
    @JvmStatic fun assembleFilePath(vararg components: String): String = components.joinToString(fileSeparator, postfix = fileSeparator)

    /**
     * Returns the path of the system folder for temporary files (using `java.io.tmpdir`).
     * If system property is not available, the user directory (using `user.dir`) is returned.
     *
     * @return The platform independent path of the tmp folder.
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    @JvmStatic fun getTmpDir(): String = System.getProperty("java.io.tmpdir").takeIf { it.isNotEmpty() } ?: System.getProperty("user.dir")

    /**
     * Returns a file representing the systems tmp folder (as returned by [getTmpDir]).
     * Creates the folder if not existing yet.
     *
     * @return A platform independent file pointing at the systems tmp folder.
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    @JvmStatic fun getTmpFolder(): File = File(getTmpDir(), "pseuCo IDE").also { it.mkdirs() }
}
