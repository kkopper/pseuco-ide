package util

/**
 * Utility class providing functions to easily handle reflections.
 * Useful for interacting with the generated and compiled Java code.
 *
 * @author Konstantin Kopper
 * @since 2.0.0
 */
object ReflectionUtils {

    /**
     * Sets a property value.
     *
     * @author Konstantin Kopper
     * @param clazz The class on which the property should be set.
     * @param property The name of the property to be set.
     * @param value The new value of [property].
     * @throws NoSuchFieldException [clazz] has no property [property].
     */
    @JvmStatic
    @Throws(NoSuchFieldException::class, SecurityException::class)
    fun setPrivateProperty(clazz: Class<*>, property: String, value: Any) {
        clazz.getDeclaredField(property).apply {
            isAccessible = true
            set(clazz, value)
        }
    }
}
