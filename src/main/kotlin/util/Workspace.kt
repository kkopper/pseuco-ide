package util

import com.pseuco.PseuCoComFile
import java.io.File

/**
 * Singleton managing the workspace of the IDE.
 * Provides some utility methods.
 *
 * @author Konstantin Kopper
 * @since 2.0.0
 */
object Workspace {

    /**
     * A default value. Points to the user directory as given by the system property `user.dir`.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    val DEFAULT = File(System.getProperty("user.dir"))

    /**
     * Internal [File] representing the workspace.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    private var workspace: File = DEFAULT

    /**
     * Returns the internal [File] object.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     * @see workspace
     */
    val asFile: File
        get() = workspace

    /**
     * Action triggered whenever [change] was invoked. Takes the old and the new file as parameters.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    var onChange: (File, File) -> Unit = { _, _ -> }

    /**
     * Action triggered whenever a file inside the workspace was changed. Takes the concerned file as parameter.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    var onFileChange: (File) -> Unit = { _ -> }

    /**
     * Create [file] in the workspace if not existing yet and writes [content] to it.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    fun createNewFile(file: File, content: String = "") {
        if (!file.exists())
            file.createNewFile()

        assert(file.exists())
        assert(file.isFile)

        if (content.isNotEmpty())
            file.writeText(content)

        onFileChange(file)
    }

    /**
     * Create a file in the workspace mimicking [remoteFile].
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     * @return The newly created file.
     */
    fun createNewFile(remoteFile: PseuCoComFile): File {
        tailrec fun numberedFile(name: String, index: Int): File =
                File(workspace, "${name}_$index.pseuco").takeUnless { it.exists() }
                        ?: numberedFile(name, index + 1)

        val file = File(workspace, "${remoteFile.name}.pseuco").takeUnless { it.exists() }
                ?: numberedFile(remoteFile.name, 1)

        createNewFile(file, remoteFile.content)

        return file
    }

    /**
     * Change the [File] representing the [workspace].
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    fun change(new: File) {
        assert(new.exists())
        assert(new.isDirectory)
        val old = workspace
        workspace = new
        onChange(old, new)
    }

    /**
     * Performs [action] using the [File] representing the workspace.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     * @see asFile
     */
    inline fun <T> withFile(action: (File) -> T): T = action(asFile)
}
