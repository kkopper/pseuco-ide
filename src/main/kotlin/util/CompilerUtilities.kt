package util

import codeGen.CodeLineMapping

/**
 * Provides utility method for interacting with the pseuco-java-compiler.
 *
 * @author Konstantin Kopper
 * @since 2.0.0
 */
object CompilerUtilities {

    /**
     * Find the next pseuCo line number for a given Java line.
     *
     * @param n The Java line number.
     * @param className Tha Java class name.
     * @return The next pseuCo line number.
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    tailrec fun getNextMatchingLine(n: Int, className: String): Int {
        val numbers = CodeLineMapping.getPseuCoLineNumber(n, className, true)
        return if (numbers.isNotEmpty()) numbers[0] else getNextMatchingLine(n + 1, className)
    }
}
