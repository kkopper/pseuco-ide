package org.gitlab.api.v4.tags

import org.gitlab.api.v4.commits.Commit

/**
 * A git tag as returned by the GitLab API v4.
 *
 * @author Konstantin Kopper
 * @since 2.1.0
 * @property commit The [Commit] this tag refers to.
 * @property message The tag message if this tag is an annotated tag, `null` for a lightweight tag.
 * @property name The name of this tag.
 * @property release The release information associated with this tag, `null` if none is available.
 * @property target The SHA of the commit this tag refers to.
 */
data class Tag(
    val commit: Commit,
    val message: String?,
    val name: String,
    val release: Release?,
    val target: String
) {

    init {
        assert(target == commit.id)
    }
}
