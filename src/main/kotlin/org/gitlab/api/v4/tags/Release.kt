package org.gitlab.api.v4.tags

/**
 * The release information associated with a [Tag] as returned by the GitLab API v4.
 *
 * @author Konstantin Kopper
 * @since 2.1.0
 * @property tagName The name of the tag this release information is associated with.
 * @property description The release notes.
 */
data class Release(val tagName: String, val description: String)
