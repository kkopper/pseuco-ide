package org.gitlab.api.v4.commits

import java.util.Date

/**
 * A commit as returned by the GitLab API v4.
 *
 * @author Konstantin Kopper
 * @since 2.1.0
 * @property id The full commit SHA.
 * @property shortId A shortened commit SHA.
 * @property title Seems to be the same as [message].
 * @property authorName TODO
 * @property authorEmail TODO
 * @property authoredDate TODO
 * @property committerName TODO
 * @property committedDate TODO
 * @property createdAt TODO
 * @property message The commit message.
 * @property parentIds A list containing the commit SHAs of the parent commits.
 */
data class Commit(
    val id: String,
    val shortId: String,
    val title: String,
    val authorName: String,
    val authorEmail: String,
    val authoredDate: Date,
    val committerName: String,
    val committedDate: Date,
    val createdAt: Date,
    val message: String,
    val parentIds: List<String>,
    val stats: Statistics?
) {

    init {
        assert(id.startsWith(shortId))
    }
}
