package org.gitlab.api.v4.commits

data class Statistics(val additions: Int, val deletions: Int, val total: Int) {

    init {
        assert(additions + deletions == total) { "Inconsistent statistics" }
    }
}
