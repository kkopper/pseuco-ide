package pseuco

import java.io.File

/**
 * Abstract task class related to the pseuCo programming language.
 *
 * @author Konstantin Kopper
 * @since 2.0.0
 * @property toBeExecuted The pseuCo file to be executed by this task.
 */
abstract class PseuCoTask(protected val toBeExecuted: File) : Runnable {

    /**
     * Interrupts this task.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    abstract fun interrupt()
}
