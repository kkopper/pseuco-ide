package pseuco.javaCompiler.runner

import codeGen.MainCodeGen
import pseuco.javaCompiler.PseuCoCompilerTask
import util.ZipUtilities
import java.io.File
import java.io.OutputStream
import java.lang.reflect.InvocationTargetException
import java.net.URLClassLoader

/**
 * PseuCo task executing a given pseuCo program.
 *
 * @author Konstantin Kopper
 * @since 2.0.0
 * @constructor Create a new executing task with the given output streams.
 */
class PseuCoRunner(toBeExecuted: File, out: OutputStream, err: OutputStream) : PseuCoCompilerTask(toBeExecuted, out, err) {

    /**
     * Creates a new executing task using the standard output streams.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    constructor(toBeExecuted: File) : this(toBeExecuted, System.out, System.err)

    override fun initialize() {
        super.initialize()

        // Run pseuCo code
        MainCodeGen.isJavaCodeGeneration = false

        // Do not print the generated Java code to the standard output.
        MainCodeGen.output = false
    }

    override fun unzipIncludeDir() = ZipUtilities.unzipResource("/includeRunner.zip", super.includeDir)

    /**
     * Executes the Java code resulting from the pseuCo code.
     * Uses reflection to call the created Java code.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     * @see main.PseuCoCo.main lines 263 ff.
     */
    override fun executeGeneratedJavaCode() {
        val classLoader = URLClassLoader(arrayOf(this.includeDir.absoluteFile.parentFile.toURI().toURL()))

        // Disable assertions for the compiler classloader
        classLoader.clearAssertionStatus()

        val c = Class.forName(MainCodeGen.generatedJavaCodeFolder + ".Main", true, classLoader)

        val argv = emptyArray<String>()

        val m = c.getMethod("main", argv.javaClass)

        val mainAgent = Thread(agents, {
            try {
                m.invoke(null, argv)
            } catch (e: IllegalAccessException) {
                e.printStackTrace()
            } catch (e: InvocationTargetException) {
                val inner = e.cause
                if (inner is RuntimeException && inner.cause is InterruptedException)
                    return@Thread // Thread was interrupted, so just terminate it.
                e.printStackTrace()
            }
        }, "mainAgent")

        mainAgent.contextClassLoader = classLoader

        mainAgent.start()
    }
}
