package pseuco.javaCompiler.debugger

import codeGen.MainCodeGen
import org.apache.commons.io.FileUtils
import pseuco.javaCompiler.PseuCoCompilerTask
import util.ZipUtilities
import java.io.File
import java.io.IOException
import java.io.OutputStream

/**
 * Abstract pseuCo task executing a given pseuCo program.
 *
 * @author Konstantin Kopper
 * @since 2.0.0
 * @constructor Create a new abstract debugging task.
 */
abstract class PseuCoDebugger(toBeExecuted: File, out: OutputStream, err: OutputStream) :
        PseuCoCompilerTask(toBeExecuted, out, err) {

    /**
     * Create a new debuging task using the standard output streams.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    constructor(toBeExecuted: File) : this(toBeExecuted, System.out, System.err)

    override fun initialize() {
        super.initialize()

        MainCodeGen.isJavaCodeGeneration = false
        MainCodeGen.output = false
    }

    override fun unzipIncludeDir() = ZipUtilities.unzipResource("/includeDebug.zip", includeDir)

    override fun parsePseuCoProgram() {
        super.parsePseuCoProgram()

        try {
            FileUtils.copyURLToFile(javaClass.getResource("/debugger/Debugger.java"), File(this.includeDir, "Debugger.java"))
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }
}
