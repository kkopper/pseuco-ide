package pseuco.javaCompiler.debugger

/**
 * Executes some predefined actions when informed by a (specific) thread.
 *
 * Implements [Comparable] on [Thread], in order to be used in environments
 * where only the standard library is available.
 *
 * @author Konstantin Kopper
 * @since 2.0.0
 * @see Comparable
 * @see Thread
 * @property t A thread this listener should be bound to, `null` if it should not be bounded.
 * @constructor Creates a new listener.
 * @param action The action executed when informed.
 */
class ThreadListener(private val t: Thread?, action: (Thread) -> Unit) : Comparable<Thread> {

    /**
     * List of actions to be executed when called.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    private val actions: MutableList<(Thread) -> Unit> = mutableListOf(action)

    /**
     * Executes the predefined task.
     *
     * @param other The thread this listener is bound to, or `null` if it is unbound. See [other].
     * @return 0, as the signature requires to return an integer.
     * @author Konstantin Kopper
     * @since 2.0.0
     */
    override fun compareTo(other: Thread): Int {
        assert(t == null || t == other)
        actions.forEach { it(other) }
        return 0
    }

    /**
     * Convenience method. Only calls [compareTo].
     * Allows to use a more precise method name if possible.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     * @param t See [compareTo].
     */
    fun call(t: Thread) {
        compareTo(t)
    }

    /**
     * Add another action to this listener.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     * @see actions
     * @param action The action to be added.
     */
    operator fun plusAssign(action: (Thread) -> Unit) {
        actions += action
    }

    /**
     * Convenience method. Only calls [call].
     * Allows to use more precise Kotlin syntax.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     * @param t See [call].
     */
    operator fun invoke(t: Thread) = call(t)
}
