package pseuco.javaCompiler.generator

import codeGen.CodeGen
import codeGen.MainCodeGen
import pseuco.javaCompiler.PseuCoCompilerTask
import java.io.File

/**
 * PseuCo task exporting the generated Java code.
 * Uses the functionality provided by the compiler,
 * i.e. encapsulates the behaviour of the `-e` command line flag.
 *
 * @author Konstantin Kopper
 * @property toBeExecuted The pseuCo file which should be executed.
 * @property destination The folder where the exported code should be placed.
 * @constructor Create a new generator.
 */
class PseuCoJavaGenerator(toBeExecuted: File, private val destination: File, private val onSuccess: () -> Unit = {}) :
        PseuCoCompilerTask(toBeExecuted) {

    init {
        assert(toBeExecuted.isFile)

        destination.takeUnless { it.exists() }?.mkdirs()
        assert(destination.isDirectory)
    }

    /**
     * Custom compiler settings to export Java source code.
     *
     * @author Konstantin Kopper
     * @since 2.0.0
     * @see main.PseuCoCo.main lines 168 ff.
     */
    override fun initialize() {
        super.initialize()

        MainCodeGen.output = false

        // Copied from PseuCoCo.java, lines 173 ff.
        MainCodeGen.isJavaCodeGeneration = true
        MainCodeGen.generatedJavaCodeFolder = this.includeDir.absolutePath
        MainCodeGen.generatedJavaCodeFolderPath = destination.absolutePath

        CodeGen.mainPackageName = destination.name
        CodeGen.monitorPackageName = destination.name
        CodeGen.structPackageName = destination.name

        MainCodeGen.initiateListOfExternJavaFiles()
        MainCodeGen.createClassesOfExternalFiles(MainCodeGen.generatedJavaCodeFolderPath)
    }

    /**
     * Empty function, as code is never executed.
     *
     * @author Konstantin Kopper
     * @throws NotImplementedError Always thrown, as this function is empty.
     * @since 2.0.0
     */
    @Throws(NotImplementedError::class)
    override fun executeGeneratedJavaCode(): Nothing = throw NotImplementedError()

    override fun run() {
        try {
            unzipIncludeDir()
            initialize()
            parsePseuCoProgram()
            onSuccess()
        } catch (e: Exception) {
            onError(e)
        } finally {
            removeUnzipped()
        }
    }
}
