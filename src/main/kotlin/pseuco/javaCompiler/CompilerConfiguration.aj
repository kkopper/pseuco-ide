package pseuco.javaCompiler;

import codeGen.CodeLine;
import codeGen.CodeLineList;
import codeGen.MainCodeGen;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * AspectJ definitions modifying the pseuco-java-compiler to suit the debugger functionality.
 *
 * The aspects are applied to only the compiler classes during the build process,
 * thus relying on imports from outside the compiler scope will crash it.
 *
 * @author Konstantin Kopper
 */
public aspect CompilerConfiguration {

    /**
     * Regular expression matching {@code Simulate.HWInterrupt();}.
     */
    private static final String HWInterruptRegex = "Simulate\\.HWInterrupt\\(\\);";

    pointcut HWInterruptCheck(): execution(* tree.ASTStmtBlock.generateCode(..));

    /*
     * Removes multiple subsequent 'Simulate.HWInterrupt();'.
     * This is necessary as the debugger manipulates them to use them for stepping.
     */
    CodeLineList around(): HWInterruptCheck() {
        final CodeLineList res = proceed();

        if (MainCodeGen.isJavaCodeGeneration)
            // No HWInterrupts() created.
            return res;

        boolean foundInterrupted = false;

        for (CodeLine l : res.getCodeLineList()) {
            final String code = l.getCode();
            final boolean isInterrupted = Pattern.matches("^.*" + HWInterruptRegex + ".*$", code);

            if (foundInterrupted && isInterrupted)
                l.setCode(code.replaceAll(HWInterruptRegex, ""));

            foundInterrupted = isInterrupted;
        }

        return res;
    }

    pointcut ASTStart(): execution(* tree.ASTStartExpression.generateCode(..));

    /*
     * Modifies the generated Java code:
     *
     *  - Removes the error handling calls for catching runtime exceptions.
     *    These exceptions are instead thrown as they are now caught by the PseuCoThread declaration.
     *
     *    This allows to use them for interrupting running threads.
     *
     *  - Replaces the name assigned to PseuCoThreads by the corresponding agent name (if available).
     */
    CodeLineList around(): ASTStart() {
        final CodeLineList res = proceed();

        if (MainCodeGen.isJavaCodeGeneration) {
            // Java code is exported, thus does not modify.
            return res;
        }

        final String r1 = "^(\\s*public\\s+void\\s+run\\(\\)\\s*\\{)\\s*try\\s*\\{\\s*$";
        final String r2 = "^(.*})catch\\s*\\(Exception e\\)\\s*\\{\\s*$";
        final String r3 = "^\\s*}\\s*$";
        final String r4 = "^\\s*ErrorHandling\\.handleExceptions\\(e\\);\\s*$";

        for (final CodeLine line : res.getCodeLineList()) {
            assert line != null;

            final String code = line.getCode();

            if (code.matches(r1)) {
                line.setCode(code.replaceAll(r1, "$1"));
            } else if (code.matches(r2)) {
                line.setCode(code.replaceAll(r2, "$1"));
            } else if (code.matches(r3) || code.matches(r4)) {
                line.setCode("");
            }
        }

        String agentName = "anonymousAgent";

        final String r5 = "^(.*\\.)?(\\w*)\\.start\\(\\);?.*$";
        final String c5 = res.getCodeLineList().getLast().getCode();

        if (c5.matches(r5))
            agentName = c5.replaceAll(r5, "$2");

        final String r6 = "^\\s*new\\s+PseuCoThread\\s*\\(\\s*\"(\\w+)\"\\s*,\\s*new\\s+Runnable\\s*\\(\\)\\s*\\{\\s*$";
        final CodeLine l6 = res.getCodeLineList().getFirst();
        final String c6 = l6.getCode();

        Matcher m = Pattern.compile(r6).matcher(c6);
        if (m.matches())
            l6.setCode(new StringBuilder(c6).replace(m.start(1), m.end(1), agentName).toString());

        return res;
    }

    pointcut CatchInterruptedExceptions(): execution(* tree.ASTStatement.generateCode(..)) ||
            execution(* tree.ASTDeclStatement.generateCode(..));

    /*
     * Adds 'throw' statements when catching 'InterruptedException's.
     * This mechanism is used to externally kill threads.
     */
    CodeLineList around(): CatchInterruptedExceptions() {
        final CodeLineList res = proceed();

        if (MainCodeGen.isJavaCodeGeneration)
            return res;

        for (final CodeLine line : res.getCodeLineList()) {
            if (line.getCode().matches("^\\s*}\\s*catch\\s*\\(\\s*InterruptedException\\s+e\\s*\\)\\s*\\{\\s*$")) {
                line.addCode(" throw new RuntimeException(e);");
                break;
            }
        }

        return res;
    }

}
