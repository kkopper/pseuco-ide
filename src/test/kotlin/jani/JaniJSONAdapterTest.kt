package jani

import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import jani.interaction.basic.Extension
import jani.interaction.basic.Identifier
import jani.interaction.basic.Metadata
import jani.interaction.basic.ModellingFormalism
import jani.interaction.basic.ParameterDefinition
import jani.interaction.basic.ParameterType
import jani.interaction.basic.ParameterValue
import jani.interaction.basic.Version
import jani.interaction.basic.messages.Authenticate
import jani.interaction.basic.messages.Capabilities
import jani.interaction.basic.messages.Close
import jani.interaction.basic.messages.ReplyUpdateServerParameters
import jani.interaction.basic.messages.RequestUpdateServerParameters
import jani.interaction.tasks.ProvideTaskMessage
import jani.interaction.tasks.ProvideTaskProgress
import jani.interaction.tasks.ProvideTaskStatus
import jani.interaction.tasks.StopTask
import jani.interaction.tasks.TaskEnded
import jani.interaction.tasks.analyse.messages.QueryAnalysisEngines
import jani.interaction.tasks.analyse.messages.ReplyAnalysisEngines
import jani.model.models.ModelFeature
import jani.model.models.ModelType
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertIterableEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertAll

/**
 * Tests for the conversion between JANI objects and JSON strings.
 *
 * @author Konstantin Kopper
 * @see JaniJSONAdapter
 */
@DisplayName("JANI \u2194 JSON conversion")
internal class JaniJSONAdapterTest {

    private lateinit var metadata: Metadata
    private lateinit var version: Version
    private lateinit var parameterDefinition: ParameterDefinition
    private lateinit var parameterValue: ParameterValue
    private lateinit var analysisEngine: ReplyAnalysisEngines.AnalysisEngine

    @BeforeEach
    fun setUp() {
        version = Version(1, 2, 3)
        metadata = Metadata("metaTest", version, "John Doe", "Some test data", "https://url.com")
        parameterDefinition = ParameterDefinition(Identifier("myParameter"), "myParameter", "This is parameter", "parameters", true, ParameterType.Bool, ParameterDefinition.DefaultValue.True)
        parameterValue = ParameterValue(Identifier("myParameter"), ParameterValue.Values.Number(42))
        analysisEngine = ReplyAnalysisEngines.AnalysisEngine(Identifier("V8"), metadata, listOf(parameterDefinition), listOf(ModelFeature.Arrays), listOf(ModelType.LTS), listOf(ModellingFormalism.new("x-groot")))
    }

    /* --- Basic messages --- */

    @Test
    @DisplayName("Authenticate: Object \u2192 JSON")
    fun testAuthenticateSerialize() {
        val msg = Authenticate(42)
        val str = JaniJSONAdapter.serialize(msg)

        assertEquals("""{"jani-versions":[42]}""", str)
    }

    @Test
    @DisplayName("Authenticate: Object \u2192 JSON (complete)")
    fun testAuthenticateSerialize2() {
        // val msg = Authenticate("John Doe", "secret", 1, 42)
        val msg = Authenticate(listOf(1, 42), listOf(Extension.PersistentState), "John Doe", "secret")
        val str = JaniJSONAdapter.serialize(msg)

        val jsonObject = JsonParser.parseString(str).let {
            assertTrue(it.isJsonObject)
            it.asJsonObject
        }

        assertAll("JSON object fields of Authenticate message", {
            // JANI versions
            assertTrue("jani-versions" in jsonObject, "Missing 'jani-versions' attribute")
            assertIterableEquals(listOf(1, 42), jsonObject["jani-versions"].asJsonArray.map { it.asInt })
        }, {
            // Login attribute
            assertTrue("login" in jsonObject, "Missing 'login' attribute")
            assertEquals("John Doe", jsonObject["login"].asString, "Invalid 'login' attribute")
        }, {
            // Password attribute
            assertTrue("password" in jsonObject, "Missing 'password' attribute")
            assertEquals("secret", jsonObject["password"].asString, "Invalid 'password' attribute")
        }, {
            // Extensions
            assertNotNull(jsonObject["extensions"])
            assertIterableEquals(listOf("persistent-state"), jsonObject["extensions"].asJsonArray.map { it.asString })
        })
    }

    @Test
    @DisplayName("Authenticate: JSON \u2192 Object")
    fun testAuthenticateDeserialize() {
        val str = """{"jani-versions":[1,42]}"""
        val msg = JaniJSONAdapter.deserializeMessage(str)

        assertTrue(msg is Authenticate)
        msg as Authenticate
        assertAll(
                { assertIterableEquals(listOf(1, 42), msg.versions) },
                { assertNull(msg.extensions) },
                { assertNull(msg.login) },
                { assertNull(msg.password) }
        )
    }

    @Test
    @DisplayName("Authenticate: JSON \u2192 Object (complete)")
    fun testAuthenticateDeserialize2() {
        val str = """{"jani-versions":[1,24],"login":"Peter","password":"12345"}"""
        val msg = JaniJSONAdapter.deserializeMessage(str)

        assertTrue(msg is Authenticate)
        msg as Authenticate
        assertAll(
                { assertIterableEquals(listOf(1, 24), msg.versions) },
                { assertEquals("Peter", msg.login) },
                { assertEquals("12345", msg.password) },
                { assertNull(msg.extensions) }
        )
    }

    @Test
    @DisplayName("Capabilities: Object \u2192 JSON")
    fun testCapabilitiesSerialize() {
        val msg = Capabilities(1, null, metadata, null, listOf("analyse", "transform"))
        val str = JaniJSONAdapter.serialize(msg)

        val jsonObject = JsonParser.parseString(str).let {
            assertTrue(it.isJsonObject)
            it.asJsonObject
        }

        assertAll("JSON object fields", {
            assertTrue("jani-version" in jsonObject, "Missing field 'jani-version'.")
            assertEquals(msg.version, jsonObject["jani-version"].asInt, "JANI version is not matching.")
        }, {
            assertNull(jsonObject["extensions"])
        }, {
            assertTrue("metadata" in jsonObject)
            checkMetadata(jsonObject["metadata"])
        })
    }

    @Test
    @DisplayName("Capabilities: Object \u2192 JSON (complete)")
    fun testCapabilitiesSerialize2() {
        val msg = Capabilities(42, listOf(Extension.PersistentState), metadata, listOf(), listOf("analyse", "transform"))

        val jsonObject = JsonParser.parseString(JaniJSONAdapter.serialize(msg)).let {
            assertTrue(it.isJsonObject, "Expected a JSON object")
            it.asJsonObject
        }

        assertAll("JSON object fields of Capabilities message", {
            assertTrue("type" in jsonObject, "Missing 'type' attribute")
            assertEquals("capabilities", jsonObject["type"].asString, "Invalid 'type' attribute")
        }, {
            assertTrue("jani-version" in jsonObject, "Missing 'version' attribute")
            assertEquals(msg.version, jsonObject["jani-version"].asInt, "Invalid 'version' attribute")
        }, {
            assertTrue("extensions" in jsonObject, "Missing 'extensions' attribute")
            assertNotNull(jsonObject["extensions"])
            assertIterableEquals(listOf("persistent-state"), jsonObject["extensions"].asJsonArray.map { it.asString },
                    "Invalid extensions' attribute")
        }, {
            assertTrue("metadata" in jsonObject, "Missing 'metadata' attribute")
            assertNotNull(jsonObject["metadata"])
            checkMetadata(jsonObject["metadata"])
        }, {
            assertTrue("parameters" in jsonObject, "Missing 'parameters' attribute")
            assertNotNull(jsonObject["parameters"])
        }, {
            assertTrue("roles" in jsonObject, "Missing 'roles' attribute")
            assertNotNull(jsonObject["roles"])
        })
    }

    @Test
    @DisplayName("Capabilities: JSON \u2192 Object")
    fun testCapabilitiesDeserialize() {
        val str = """{"type":"capabilities","jani-version":42,"metadata":{"name":"metaTest","version":{"major":1,"minor":2,"revision":3},"author":"John Doe","description":"Some test data","url":"https://url.com"},"roles":["x-myRole"]}"""
        val msg = JaniJSONAdapter.deserializeMessage(str)

        assertTrue(msg is Capabilities)
        msg as Capabilities
        assertAll(
                { assertEquals(42, msg.version, "Invalid 'jani-version' attribute") },
                { assertNull(msg.extensions, "Invalid 'extensions' attribute") },
                { assertEquals(metadata, msg.metadata, "Invalid 'metadata' attribute") },
                { assertNull(msg.parameters, "Invalid 'parameters' attribute") },
                { assertIterableEquals(listOf("x-myRole"), msg.roles, "Invalid 'roles' attribute") }
        )
    }

    @Test
    @DisplayName("Capabilities: JSON \u2192 Object (complete)")
    fun testCapabilitiesDeserialize2() {
        val str = """{"type":"capabilities","jani-version":123,"extensions":["persistent-state"],"metadata":{"name":"metaTest","version":{"major":1,"minor":2,"revision":3},"author":"John Doe","description":"Some test data","url":"https://url.com"},"parameters":[],"roles":["analyse","transform","x-check"]}"""
        val msg = JaniJSONAdapter.deserializeMessage(str)

        assertTrue(msg is Capabilities)
        msg as Capabilities
        assertAll(
                { assertEquals(123, msg.version, "Invalid 'version' attribute") },
                {
                    assertNotNull(msg.extensions)
                    assertIterableEquals(listOf(Extension.PersistentState), msg.extensions!!, "Invalid 'extensions' attribute")
                },
                { assertEquals(metadata, msg.metadata, "Invalid 'metadata' attribute") },
                {
                    assertNotNull(msg.parameters)
                    assertIterableEquals(emptyList<ParameterDefinition>(), msg.parameters!!, "Invalid 'parameters' attribute")
                },
                { assertIterableEquals(listOf("analyse", "transform", "x-check"), msg.roles, "Invalid 'roles' attribute") }
        )
    }

    @Test
    @DisplayName("Close: Object \u2192 JSON")
    fun testCloseSerialize() {
        val msg = Close(null)
        val str = JaniJSONAdapter.serialize(msg)

        assertEquals("""{"type":"close"}""", str)
    }

    @Test
    @DisplayName("Close: Object \u2192 JSON (complete)")
    fun testCloseSerialize2() {
        val msg = Close("Just a test")
        val str = JaniJSONAdapter.serialize(msg)

        val jsonObject = JsonParser.parseString(str).let {
            assertTrue(it.isJsonObject, "Expected a JSON object")
            it.asJsonObject
        }

        assertAll({
            assertTrue("type" in jsonObject, "Missing 'type' attribute")
            assertEquals("close", jsonObject["type"].asString, "Invalid 'type' attribute")
        }, {
            assertTrue("reason" in jsonObject, "Missing 'reason' attribute")
            assertNotNull(jsonObject["reason"])
            assertEquals(msg.reason, jsonObject["reason"].asString, "Invalid 'reason' attribute")
        })
    }

    @Test
    @DisplayName("Close: JSON \u2192 Object")
    fun testCloseDeserialize() {
        val str = """{"type":"close"}"""
        val msg = JaniJSONAdapter.deserializeMessage(str)

        assertTrue(msg is Close)
        msg as Close
        assertNull(msg.reason, "Invalid 'reason' attribute")
    }

    @Test
    @DisplayName("Close: JSON \u2192 Object (complete)")
    fun testCloseDeserialize2() {
        val str = """{"type":"close","reason":"Just a test"}"""
        val msg = JaniJSONAdapter.deserializeMessage(str)

        assertTrue(msg is Close)
        msg as Close
        assertNotNull(msg.reason)
        assertEquals("Just a test", msg.reason, "Invalid 'reason' attribute")
    }

    @Test
    @DisplayName("RequestUpdateServerParameters: Object \u2192 JSON")
    fun testRequestUpdateServerParametersSerialize() {
        val msg = RequestUpdateServerParameters(42, emptyList())
        val str = JaniJSONAdapter.serialize(msg)

        val jsonObject = JsonParser.parseString(str).let {
            assertTrue(it.isJsonObject, "Expected a JSON object")
            it.asJsonObject
        }

        assertAll({
            assertTrue("type" in jsonObject, "Missing 'type' attribute")
            assertEquals("server-parameters", jsonObject["type"].asString, "Invalid 'type' attribute")
        }, {
            assertTrue("id" in jsonObject, "Missing 'id' attribute")
            assertEquals(msg.id, jsonObject["id"].asInt, "Invalid 'id' attribute")
        }, {
            assertTrue("parameters" in jsonObject, "Missing 'parameters' attribute")
            assertIterableEquals(msg.parameters, jsonObject["parameters"].asJsonArray.asIterable(), "Invalid 'parameters' attribute")
        })
    }

    @Test
    @DisplayName("RequestUpdateServerParameters: Object \u2192 JSON (complete)")
    fun testRequestUpdateServerParametersSerialize2() {
        val msg = RequestUpdateServerParameters(1, listOf(parameterValue))
        val str = JaniJSONAdapter.serialize(msg)

        val jsonObject = JsonParser.parseString(str).let {
            assertTrue(it.isJsonObject, "Expected a JSON object")
            it.asJsonObject
        }

        assertAll({
            assertTrue("type" in jsonObject, "Missing 'type' attribute")
            assertEquals("server-parameters", jsonObject["type"].asString, "Invalid 'type' parameter")
        }, {
            assertTrue("id" in jsonObject, "Missing 'id' attribute")
            assertEquals(msg.id, jsonObject["id"].asInt, "Invalid 'id' attribute")
        }, {
            assertTrue("parameters" in jsonObject, "Missing 'parameters' attribute")
            assertTrue(jsonObject["parameters"].isJsonArray, "Invalid 'parameters' attribute: expected a JSON array")
            jsonObject["parameters"].asJsonArray.forEach { checkParameterValue(it) }
        })
    }

    @Test
    @DisplayName("RequestUpdateServerParameters: JSON \u2192 Object")
    fun testRequestUpdateServerParametersDeserialize() {
        val str = """{"type":"server-parameters","id":42,"parameters":[]}"""
        val msg = JaniJSONAdapter.deserializeMessage(str)

        assertTrue(msg is RequestUpdateServerParameters)
        msg as RequestUpdateServerParameters

        assertAll({
            assertEquals(42, msg.id, "Invalid 'id' attribute")
        }, {
            assertIterableEquals(emptyList<ParameterValue>(), msg.parameters, "Invalid 'parameters' attribute")
        })
    }

    @Test
    @DisplayName("RequestUpdateServerParameters: JSON \u2192 Object  (complete)")
    fun testRequestUpdateServerParametersDeserialize2() {
        val str = """{"type":"server-parameters","id":1,"parameters":[{"id":"myParameter","value":42}]}"""
        val msg = JaniJSONAdapter.deserializeMessage(str)

        assertTrue(msg is RequestUpdateServerParameters)
        msg as RequestUpdateServerParameters

        assertAll({
            assertEquals(1, msg.id, "Invalid 'id' attribute")
        }, {
            assertIterableEquals(listOf(parameterValue), msg.parameters, "Invalid 'parameters' attribute")
        })
    }

    @Test
    @DisplayName("ReplyUpdateServerParameters: Object \u2192 JSON (valid)")
    fun testReplyUpdateServerParametersSerializeValid() {
        val msg = ReplyUpdateServerParameters(42)
        val str = JaniJSONAdapter.serialize(msg)

        val jsonObject = JsonParser.parseString(str).let {
            assertTrue(it.isJsonObject)
            it.asJsonObject
        }

        assertAll({
            assertTrue("type" in jsonObject, "Missing 'type' attribute")
            jsonObject["type"].also {
                assertTrue(it.isJsonPrimitive && it.asJsonPrimitive.isString, "Invalid 'type' attribute: expected a string")
                assertEquals("server-parameters", it.asString, "Invalid 'type' attribute")
            }
        }, {
            assertTrue("id" in jsonObject, "Missing 'id' attribute")
            jsonObject["id"].also {
                assertTrue(it.isJsonPrimitive && it.asJsonPrimitive.isNumber, "Invalid 'id' attribute: expected a number")
                assertEquals(msg.id, it.asInt, "Invalid 'id' attribute")
            }
        }, {
            assertTrue("success" in jsonObject, "Missing 'success' attribute")
            jsonObject["success"].also {
                assertTrue(it.isJsonPrimitive && it.asJsonPrimitive.isBoolean, "Invalid 'success' attribute: expected a boolean value")
                assertTrue(it.asBoolean, "Invalid 'success' attribute")
            }
        }, {
            assertFalse("error" in jsonObject, "Found 'error' attribute")
        })
    }

    @Test
    @DisplayName("ReplyUpdateServerParameters: Object \u2192 JSON (invalid)")
    fun testReplyUpdateServerParametersSerializeInvalid() {
        val msg = ReplyUpdateServerParameters(24, "Your parameters suck.")
        val str = JaniJSONAdapter.serialize(msg)

        val jsonObject = JsonParser.parseString(str).let {
            assertTrue(it.isJsonObject)
            it.asJsonObject
        }

        assertAll({
            assertTrue("type" in jsonObject, "Missing 'type' attribute")
            jsonObject["type"].also {
                assertTrue(it.isJsonPrimitive && it.asJsonPrimitive.isString, "Invalid 'type' attribute: expected a string")
                assertEquals("server-parameters", it.asString, "Invalid 'type' attribute")
            }
        }, {
            assertTrue("id" in jsonObject, "Missing 'id' attribute")
            jsonObject["id"].also {
                assertTrue(it.isJsonPrimitive && it.asJsonPrimitive.isNumber, "Invalid 'id' attribute: expected a number")
                assertEquals(msg.id, it.asInt, "Invalid 'id' attribute")
            }
        }, {
            assertTrue("success" in jsonObject, "Missing 'success' attribute")
            jsonObject["success"].also {
                assertTrue(it.isJsonPrimitive && it.asJsonPrimitive.isBoolean, "Invalid 'success' attribute: expected a boolean value")
                assertFalse(it.asBoolean, "Invalid 'success' attribute")
            }
        }, {
            assertTrue("error" in jsonObject, "Missing 'error' attribute")
            jsonObject["error"].also {
                assertTrue(it.isJsonPrimitive && it.asJsonPrimitive.isString, "Invalid 'error' attribute: expected a string")
                assertEquals(msg.error!!, it.asString, "Invalid 'error' attribute")
            }
        })
    }

    @Test
    @DisplayName("ReplyUpdateServerParameters: JSON \u2192 Object (valid)")
    fun testReplyUpdateServerParametersDeserializeValid() {
        val str = """{"type":"server-parameters","id":42,"success":true}"""
        val msg = JaniJSONAdapter.deserializeMessage(str)

        assertTrue(msg is ReplyUpdateServerParameters)
        msg as ReplyUpdateServerParameters

        assertAll({
            assertEquals(42, msg.id, "Invalid 'id' attribute")
        }, {
            assertTrue(msg.success, "Invalid 'success' attribute")
        }, {
            assertNull(msg.error, "Found 'error' attribute")
        })
    }

    @Test
    @DisplayName("ReplyUpdateServerParameters: JSON \u2192 Object (invalid)")
    fun testReplyUpdateServerParametersDeserializeInvalid() {
        val str = """{"type":"server-parameters","id":123,"success":false,"error":"wrong"}"""
        val msg = JaniJSONAdapter.deserializeMessage(str)

        assertTrue(msg is ReplyUpdateServerParameters)
        msg as ReplyUpdateServerParameters

        assertAll({
            assertEquals(123, msg.id, "Invalid 'id' attribute")
        }, {
            assertFalse(msg.success, "Invalid 'success' attribute")
        }, {
            assertNotNull(msg.error, "Missing 'error' attribute")
            assertEquals("wrong", msg.error!!, "Invalid 'error' attribute")
        })
    }

    /* --- Tasks --- */

    @Test
    @DisplayName("ProvideTaskStatus: Object \u2192 JSON")
    fun testProvideTaskStatusSerialize() {
        val msg = ProvideTaskStatus(42, "Running...")
        val str = JaniJSONAdapter.serialize(msg)

        val jsonObject = JsonParser.parseString(str).let {
            assertTrue(it.isJsonObject, "Expected a JSON object")
            it.asJsonObject
        }

        assertAll({
            assertTrue("type" in jsonObject, "Missing 'type' attribute")
            jsonObject["type"].also {
                assertTrue(it.isJsonPrimitive && it.asJsonPrimitive.isString, "Invalid 'type' attribute: expected a string")
                assertEquals("task-status", it.asString, "Invalid 'type' attribute")
            }
        }, {
            assertTrue("id" in jsonObject, "Missing 'id' attribute")
            jsonObject["id"].also {
                assertTrue(it.isJsonPrimitive && it.asJsonPrimitive.isNumber, "Invalid 'id' attribute: expected a number")
                assertEquals(msg.id, it.asInt, "Invalid 'id' attribute")
            }
        }, {
            assertTrue("status" in jsonObject, "Missing 'status' attribute")
            jsonObject["status"].also {
                assertTrue(it.isJsonPrimitive && it.asJsonPrimitive.isString, "Invalid 'status' attribute: expected a string")
                assertEquals(msg.status, it.asString, "Invalid 'status' attribute")
            }
        })
    }

    @Test
    @DisplayName("ProvideTaskStatus: JSON \u2192 Object")
    fun testProvideTaskStatusDeserialize() {
        val str = """{"type":"task-status","id":123,"status":"Nearly done"}"""
        val msg = JaniJSONAdapter.deserializeMessage(str)

        assertTrue(msg is ProvideTaskStatus)
        msg as ProvideTaskStatus

        assertAll({
            assertEquals(123, msg.id, "Invalid 'id' attribute")
        }, {
            assertEquals("Nearly done", msg.status, "Invalid 'status' attribute")
        })
    }

    @Test
    @DisplayName("ProvideTaskProgress: Object \u2192 JSON")
    fun testProvideTaskProgressSerialize() {
        val msg = ProvideTaskProgress(42, .75)
        val str = JaniJSONAdapter.serialize(msg)

        val jsonObject = JsonParser.parseString(str).let {
            assertTrue(it.isJsonObject, "Expected a JSON object")
            it.asJsonObject
        }

        assertAll({
            assertTrue("type" in jsonObject, "Missing 'type' attribute")
            jsonObject["type"].also {
                assertTrue(it.isJsonPrimitive && it.asJsonPrimitive.isString, "Invalid 'type' attribute: expected a string")
                assertEquals("task-progress", it.asString, "Invalid 'type' attribute")
            }
        }, {
            assertTrue("id" in jsonObject, "Missing 'id' attribute")
            jsonObject["id"].also {
                assertTrue(it.isJsonPrimitive && it.asJsonPrimitive.isNumber, "Invalid 'id' attribute: expected a number")
                assertEquals(msg.id, it.asInt, "Invalid 'id' attribute")
            }
        }, {
            assertTrue("progress" in jsonObject, "Missing 'progress' attribute")
            jsonObject["progress"].also {
                assertTrue(it.isJsonPrimitive && it.asJsonPrimitive.isNumber, "Invalid 'progress' attribute: expected a number")
                assertEquals(msg.progress, it.asDouble, "Invalid 'progress' attribute")
            }
        })
    }

    @Test
    @DisplayName("ProvideTaskProgress: JSON \u2192 Object")
    fun testProvideTaskProgressDeserialize() {
        val str = """{"type":"task-progress","id":345,"progress":0.68}"""
        val msg = JaniJSONAdapter.deserializeMessage(str)

        assertTrue(msg is ProvideTaskProgress)
        msg as ProvideTaskProgress

        assertAll({
            assertEquals(345, msg.id, "Invalid 'id' attribute")
        }, {
            assertEquals(.68, msg.progress, "Invalid 'progress' attribute")
        })
    }

    @Test
    @DisplayName("ProvideTaskMessage: Object \u2192 JSON")
    fun testProvideTaskMessageSerialize() {
        val msg = ProvideTaskMessage(814, ProvideTaskMessage.Severity.ERROR, "Something went wrong")
        val str = JaniJSONAdapter.serialize(msg)

        val jsonObject = JsonParser.parseString(str).let {
            assertTrue(it.isJsonObject, "Expected a JSON object")
            it.asJsonObject
        }

        assertAll({
            assertTrue("type" in jsonObject, "Missing 'type' attribute")
            jsonObject["type"].also {
                assertTrue(it.isJsonPrimitive && it.asJsonPrimitive.isString, "Invalid 'type' attribute: expected a string")
                assertEquals("task-message", it.asString, "Invalid 'type' attribute")
            }
        }, {
            assertTrue("id" in jsonObject, "Missing 'id' attribute")
            jsonObject["id"].also {
                assertTrue(it.isJsonPrimitive && it.asJsonPrimitive.isNumber, "Invalid 'id' attribute: expected a number")
                assertEquals(msg.id, it.asInt, "Invalid 'id' attribute")
            }
        }, {
            assertTrue("severity" in jsonObject, "Missing 'severity' attribute")
            jsonObject["severity"].also {
                assertTrue(it.isJsonPrimitive && it.asJsonPrimitive.isString, "Invalid 'severity' attribute: expected a string")
                assertEquals("error", it.asString, "Invalid 'severity' attribute")
            }
        }, {
            assertTrue("message" in jsonObject, "Missing 'message' attribute")
            jsonObject["message"].also {
                assertTrue(it.isJsonPrimitive && it.asJsonPrimitive.isString, "Invalid 'message' attribute: expected a string")
                assertEquals(msg.message, it.asString, "Invalid 'message' attribute")
            }
        })
    }

    @Test
    @DisplayName("ProvideTaskMessage: JSON \u2192 Object")
    fun testProvideTaskMessageDeserialize() {
        val str = """{"type":"task-message","id":478,"severity":"info","message":"(y)"}"""
        val msg = JaniJSONAdapter.deserializeMessage(str)

        assertTrue(msg is ProvideTaskMessage)
        msg as ProvideTaskMessage

        assertAll({
            assertEquals(478, msg.id, "Invalid 'id' attribute")
        }, {
            assertEquals(ProvideTaskMessage.Severity.INFO, msg.severity, "Invalid 'severity' attribute")
        }, {
            assertEquals("(y)", msg.message, "Invalid 'message' attribute")
        })
    }

    @Test
    @DisplayName("StopTask: Object \u2192 JSON")
    fun testStopTaskSerialize() {
        val msg = StopTask(987)
        val str = JaniJSONAdapter.serialize(msg)

        val jsonObject = JsonParser.parseString(str).let {
            assertTrue(it.isJsonObject, "Expected a JSON object")
            it.asJsonObject
        }

        assertAll({
            assertTrue("type" in jsonObject, "Missing 'type' attribute")
            jsonObject["type"].also {
                assertTrue(it.isJsonPrimitive && it.asJsonPrimitive.isString, "Invalid 'type' attribute: expected a string")
                assertEquals("task-stop", it.asString, "Invalid 'type' attribute")
            }
        }, {
            assertTrue("id" in jsonObject, "Missing 'id' attribute")
            jsonObject["id"].also {
                assertTrue(it.isJsonPrimitive && it.asJsonPrimitive.isNumber, "Invalid 'id' attribute: expected a number")
                assertEquals(msg.id, it.asInt, "Invalid 'id' attribute")
            }
        })
    }

    @Test
    @DisplayName("StopTask: JSON \u2192 Object")
    fun testStopTaskDeserialize() {
        val str = """{"type":"task-stop","id":321}"""
        val msg = JaniJSONAdapter.deserializeMessage(str)

        assertTrue(msg is StopTask)
        msg as StopTask

        assertEquals(321, msg.id, "Invalid 'id' attribute")
    }

    @Test
    @DisplayName("TaskEnded: Object \u2192 JSON")
    fun testTaskEndedSerialize() {
        val msg = TaskEnded(42)
        val str = JaniJSONAdapter.serialize(msg)

        val jsonObject = JsonParser.parseString(str).let {
            assertTrue(it.isJsonObject, "Expected a JSON object")
            it.asJsonObject
        }

        assertAll({
            assertTrue("type" in jsonObject, "Missing 'type' attribute")
            jsonObject["type"].also {
                assertTrue(it.isJsonPrimitive && it.asJsonPrimitive.isString, "Invalid 'type' attribute: expected a string")
                assertEquals("task-end", it.asString, "Invalid 'type' attribute")
            }
        }, {
            assertTrue("id" in jsonObject, "Missing 'id' attribute")
            jsonObject["id"].also {
                assertTrue(it.isJsonPrimitive && it.asJsonPrimitive.isNumber, "Invalid 'id' attribute: expected a number")
                assertEquals(msg.id, it.asInt, "Invalid 'id' attribute")
            }
        })
    }

    @Test
    @DisplayName("TaskEnded: JSON \u2192 Object")
    fun testTaskEndedDeserialize() {
        val str = """{"type":"task-end","id":25}"""
        val msg = JaniJSONAdapter.deserializeMessage(str)

        assertTrue(msg is TaskEnded)
        msg as TaskEnded

        assertEquals(25, msg.id, "Invalid 'id' attribute")
    }

    /* --- Analyse --- */

    @Test
    @DisplayName("QueryAnalysisEngines: Object \u2192 JSON")
    fun testQueryAnalysisEnginesSerialize() {
        val msg = QueryAnalysisEngines(735)
        val str = JaniJSONAdapter.serialize(msg)

        val jsonObject = JsonParser.parseString(str).let {
            assertTrue(it.isJsonObject, "Expected a JSON object")
            it.asJsonObject
        }

        assertAll({
            assertTrue("type" in jsonObject, "Missing 'type' attribute")
            jsonObject["type"].also {
                assertTrue(it.isJsonPrimitive && it.asJsonPrimitive.isString, "Invalid 'type' attribute: expected a string")
                assertEquals("analysis-engines", it.asString, "Invalid 'type' attribute")
            }
        }, {
            assertTrue("id" in jsonObject, "Missing 'id' attribute")
            jsonObject["id"].also {
                assertTrue(it.isJsonPrimitive && it.asJsonPrimitive.isNumber, "Invalid 'id' attribute: expected a number")
                assertEquals(msg.id, it.asInt, "Invalid 'id' attribute")
            }
        })
    }

    @Test
    @DisplayName("QueryAnalysisEngines: JSON \u2192 Object")
    fun testQueryAnalysisEnginesDeserialize() {
        val str = """{"type":"analysis-engines","id":517}"""
        val msg = JaniJSONAdapter.deserializeMessage(str)

        assertTrue(msg is QueryAnalysisEngines)
        msg as QueryAnalysisEngines

        assertEquals(517, msg.id, "Invalid 'id' attribute")
    }

    @Test
    @DisplayName("ReplyAnalysisEngines: Object \u2192 JSON")
    fun testReplyAnalysisEnginesSerialize() {
        val msg = ReplyAnalysisEngines(471, listOf(analysisEngine))
        val str = JaniJSONAdapter.serialize(msg)

        val jsonObject = JsonParser.parseString(str).let {
            assertTrue(it.isJsonObject, "Expected a JSON object")
            it.asJsonObject
        }

        assertAll({
            assertTrue("type" in jsonObject, "Missing 'type' attribute")
            jsonObject["type"].also {
                assertTrue(it.isJsonPrimitive && it.asJsonPrimitive.isString, "Invalid 'type' attribute: expected a string")
                assertEquals("analysis-engines", it.asString, "Invalid 'type' attribute")
            }
        }, {
            assertTrue("id" in jsonObject, "Missing 'id' attribute")
            jsonObject["id"].also {
                assertTrue(it.isJsonPrimitive && it.asJsonPrimitive.isNumber, "Invalid 'id' attribute: expected a number")
                assertEquals(msg.id, it.asInt, "Invalid 'id' attribute")
            }
        }, {
            assertTrue("engines" in jsonObject, "Missing 'engines' attribute")
            jsonObject["engines"].also {
                assertTrue(it.isJsonArray, "Invalid 'engines' attribute: expected an array")
                assertAll(it.asJsonArray.map { { checkAnalysisEngine(it) } })
            }
        })
    }

    @Test
    @DisplayName("ReplyAnalysisEngines: JSON \u2192 Object")
    fun testReplyAnalysisEnginesDeserialize() {
        val str = """{"type":"analysis-engines","id":929,"engines":[{"id":"W16","metadata":{"name":"metaTest","version":{"major":1,"minor":2,"revision":3},"author":"John Doe","description":"Some test data","url":"https://url.com"}}]}"""
        val msg = JaniJSONAdapter.deserializeMessage(str)

        assertTrue(msg is ReplyAnalysisEngines)
        msg as ReplyAnalysisEngines

        assertAll({
            assertEquals(929, msg.id, "Invalid 'id' attribute")
        }, {
            assertAll(msg.engines.map {
                {
                    assertAll({
                        assertEquals("W16", it.id.plainString, "Invalid 'id' attribute for analysis engine")
                    }, {
                        assertEquals(metadata, it.metadata, "Invalid 'metadata' attribute for analysis engine")
                    })
                }
            })
        })
    }

    /* --- Utility functions --- */

    /**
     * Checks if [json] matches [metadata].
     *
     * @author Konstantin Kopper
     */
    private fun checkMetadata(json: JsonElement) {
        assertTrue(json.isJsonObject)
        val jsonObject = json.asJsonObject
        assertAll("Metadata object", {
            // Name attribute
            assertTrue("name" in jsonObject, "Missing 'name' attribute")
            assertEquals(metadata.name, jsonObject["name"].asString, "Invalid 'name' attribute")
        }, {
            // Version attribute
            assertTrue("version" in jsonObject, "Missing 'version' attribute")
            checkVersion(jsonObject["version"])
        }, {
            // Author attribute
            if (metadata.author != null) {
                assertTrue("author" in jsonObject, "Missing 'author' attribute")
                assertEquals(metadata.author!!, jsonObject["author"].asString, "Invalid 'author' attribute")
            } else
                assertNull(jsonObject["author"])
        }, {
            if (metadata.description != null) {
                assertTrue("description" in jsonObject, "Missing 'description' attribute")
                assertEquals(metadata.description, jsonObject["description"].asString, "Invalid 'description' attribute")
            } else
                assertNull(jsonObject["description"])
        }, {
            if (metadata.url != null) {
                assertTrue("url" in jsonObject, "Missing 'url' attribute")
                assertEquals(metadata.url, jsonObject["url"].asString, "Invalid 'url' attribute")
            } else
                assertNull(jsonObject["url"])
        })
    }

    /**
     * Checks if [json] matches [version].
     *
     * @author Konstantin Kopper
     */
    private fun checkVersion(json: JsonElement) {
        assertTrue(json.isJsonObject)
        val jsonObject = json.asJsonObject
        assertAll("Version object", {
            // Major attribute
            assertTrue("major" in jsonObject, "Missing 'major' attribute")
            assertEquals(version.major, jsonObject["major"].asInt, "Invalid 'major' attribute")
        }, {
            // Minor attribute
            assertTrue("minor" in jsonObject, "Missing 'minor' attribute")
            assertEquals(version.minor, jsonObject["minor"].asInt, "Invalid 'minor' attribute")
        }, {
            // Revision attribute
            assertTrue("revision" in jsonObject, "Missing 'revision' attribute")
            assertEquals(version.revision, jsonObject["revision"].asInt, "Invalid 'revision' attribute")
        })
    }

    /**
     * Checks if [json] matches [parameterValue].
     *
     * @author Konstantin Kopper
     */
    private fun checkParameterValue(json: JsonElement) {
        assertTrue(json.isJsonObject, "Expected a JSON object")
        val jsonObject = json.asJsonObject
        assertAll("ParameterValue object fields", {
            assertTrue("id" in jsonObject, "Missing 'id' attribute")
            assertEquals(parameterValue.identifier.plainString, jsonObject["id"].asString, "Invalid 'id' attribute")
        }, {
            assertTrue("value" in jsonObject, "Missing 'value' attribute")
            when (val value = parameterValue.value) { // Required to allow smart casts
                is ParameterValue.Values.True -> jsonObject["value"].also {
                    assertTrue(it.isJsonPrimitive && it.asJsonPrimitive.isBoolean, "Invalid 'value' attribute: expected a boolean value")
                    assertTrue(it.asBoolean, "Invalid 'value' attribute")
                }
                is ParameterValue.Values.False -> jsonObject["value"].also {
                    assertTrue(it.isJsonPrimitive && it.asJsonPrimitive.isBoolean, "Invalid 'value' attribute: expected a boolean value")
                    assertFalse(it.asBoolean, "Invalid 'value' attribute")
                }
                is ParameterValue.Values.Number -> jsonObject["value"].also {
                    assertTrue(it.isJsonPrimitive && it.asJsonPrimitive.isNumber, "Invalid 'value' attribute: expected a number")
                    assertEquals(value.number, it.asInt, "Invalid 'value' attribute")
                }
                is ParameterValue.Values.String -> jsonObject["value"].also {
                    assertTrue(it.isJsonPrimitive && it.asJsonPrimitive.isString, "Invalid 'value' attribute: expected a string")
                    assertEquals(value.string, it.asString, "Invalid 'value' attribute")
                }
            }
        })
    }

    /**
     * Checks if [json] matches [analysisEngine].
     *
     * @author Konstantin Kopper
     * @see ReplyAnalysisEngines.AnalysisEngine
     */
    private fun checkAnalysisEngine(json: JsonElement) {
        assertTrue(json.isJsonObject, "Expected a JSON object for ")
        val jsonObject = json.asJsonObject

        assertAll("AnalysisEngine fields", {
            assertTrue("id" in jsonObject, "Missing 'id' attribute")
            jsonObject["id"].also {
                assertTrue(it.isJsonPrimitive && it.asJsonPrimitive.isString, "Invalid 'id' attribute: expected a string")
                assertEquals(analysisEngine.id.plainString, it.asString, "Invalid 'id' attribute")
            }
        }, {
            assertTrue("metadata" in jsonObject, "Missing 'metadata' attribute")
            checkMetadata(jsonObject["metadata"])
        }, {
            if (analysisEngine.parameters != null) {
                assertTrue("parameters" in jsonObject, "Missing 'parameters' attribute")
                jsonObject["parameters"].also {
                    assertTrue(it.isJsonArray, "Invalid 'parameters' attribute: expected an array")
                    assertAll(it.asJsonArray.map { { checkParameterDefinition(it) } })
                }
            } else
                assertFalse("parameters" in jsonObject, "Found 'parameters' attribute")
        }, {
            if (analysisEngine.modelFeatures != null) {
                assertTrue("model-features" in jsonObject, "Missing 'model-features' attribute")
                jsonObject["model-features"].also {
                    assertTrue(it.isJsonArray, "Invalid 'model-features' attribute: expected an array")
                    assertAll(it.asJsonArray.mapIndexed { n, el ->
                        {
                            assertTrue(el.isJsonPrimitive && el.asJsonPrimitive.isString, "Invalid element in 'model-features': expected a string")
                            assertEquals(analysisEngine.modelFeatures!![n].asString, el.asString, "Invalid element in 'model-features'")
                        }
                    })
                }
            } else
                assertFalse("model-features" in jsonObject, "Found 'model-features' attribute")
        }, {
            if (analysisEngine.modelTypes != null) {
                assertTrue("model-types" in jsonObject, "Missing 'model-types' attribute")
                jsonObject["model-types"].also {
                    assertTrue(it.isJsonArray, "Invalid 'model-types' attribute: expected an array")
                    assertAll(it.asJsonArray.mapIndexed { n, el ->
                        {
                            assertTrue(el.isJsonPrimitive && el.asJsonPrimitive.isString, "Invalid element in 'model-types': expected a string")
                            assertEquals(analysisEngine.modelTypes!![n].name.toLowerCase(), el.asString, "Invalid element in 'model-types'")
                        }
                    })
                }
            } else
                assertFalse("model-types" in jsonObject, "Found 'model-types' attribute")
        }, {
            if (analysisEngine.modellingFormalisms != null) {
                assertTrue("modelling-formalisms" in jsonObject, "Missing 'modelling-formalisms' attribute")
                jsonObject["modelling-formalisms"].also {
                    assertTrue(it.isJsonArray, "Invalid 'modelling-formalisms' attribute: expected an array")
                    assertAll(it.asJsonArray.mapIndexed { n, el ->
                        {
                            assertTrue(el.isJsonPrimitive && el.asJsonPrimitive.isString, "Invalid element in 'modelling-formalisms': expected a string")
                            assertEquals(analysisEngine.modellingFormalisms!![n].asString, el.asString, "Invalid element in 'modelling-formalisms'")
                        }
                    })
                }
            } else
                assertFalse("modelling-formalisms" in jsonObject, "Found 'modelling-formalisms' attribute")
        })
    }

    /**
     * Checks if [json] matches [parameterDefinition].
     *
     * @author Konstantin Kopper
     * @see ParameterDefinition
     */
    private fun checkParameterDefinition(json: JsonElement) {
        assertTrue(json.isJsonObject, "Expected a JSON object as parameter definition")
        val jsonObject = json.asJsonObject

        assertAll("ParameterDefinition fields", {
            assertTrue("id" in jsonObject, "Missing 'id' attribute")
            jsonObject["id"].also {
                assertTrue(it.isJsonPrimitive && it.asJsonPrimitive.isString, "Invalid 'id' attribute: expected a string")
                assertEquals(parameterDefinition.id.plainString, it.asString, "Invalid 'id' attribute")
            }
        }, {
            assertTrue("name" in jsonObject, "Missing 'name' attribute")
            jsonObject["name"].also {
                assertTrue(it.isJsonPrimitive && it.asJsonPrimitive.isString, "Invalid 'name' attribute: expected a string")
                assertEquals(parameterDefinition.name, it.asString, "Invalid 'name' attribute")
            }
        }, {
            if (parameterDefinition.description != null) {
                assertTrue("description" in jsonObject, "Missing 'description' attribute")
                jsonObject["description"].also {
                    assertTrue(it.isJsonPrimitive && it.asJsonPrimitive.isString, "Invalid 'description' attribute: expected a string")
                    assertEquals(parameterDefinition.description!!, it.asString, "Invalid 'description' attribute")
                }
            } else
                assertFalse("description" in jsonObject, "Found 'description' attribute")
        }, {
            if (parameterDefinition.category != null) {
                assertTrue("category" in jsonObject, "Missing 'category' attribute")
                jsonObject["category"].also {
                    assertTrue(it.isJsonPrimitive && it.asJsonPrimitive.isString, "Invalid 'category' attribute: expected a string")
                    assertEquals(parameterDefinition.category!!, it.asString, "Invalid 'category' attribute")
                }
            } else
                assertFalse("category" in jsonObject, "Found 'category' attribute")
        }, {
            assertTrue("is-global" in jsonObject, "Missing 'is-global' attribute")
            jsonObject["is-global"].also {
                assertTrue(it.isJsonPrimitive && it.asJsonPrimitive.isBoolean, "Invalid 'is-global' attribute: expected a boolean value")
                assertEquals(parameterDefinition.isGlobal, it.asBoolean, "Invalid 'is-global' attribute")
            }
        }, {
            assertTrue("type" in jsonObject, "Missing 'type' attribute")
            jsonObject["type"].also {
                assertTrue(it.isJsonPrimitive && it.asJsonPrimitive.isString, "Invalid 'type' attribute: expected a string")
                assertEquals("bool", it.asString, "Invalid 'type' attribute")
            }
        }, {
            assertTrue("default-value" in jsonObject, "Missing 'default-value' attribute")
            jsonObject["default-value"].also {
                assertTrue(it.isJsonPrimitive && it.asJsonPrimitive.isBoolean, "Invalid 'default-value' attribute: expected a boolean value")
                assertEquals(true, it.asBoolean, "Invalid 'default-value' attribute")
            }
        })
    }

    private operator fun JsonObject.contains(key: String) = has(key)
}
