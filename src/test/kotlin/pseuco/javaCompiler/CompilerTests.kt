package pseuco.javaCompiler

import kotlinx.serialization.Required
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertIterableEquals
import org.junit.jupiter.api.Assertions.assertTimeoutPreemptively
import org.junit.jupiter.api.Assumptions.assumeTrue
import org.junit.jupiter.api.DynamicTest.dynamicTest
import org.junit.jupiter.api.TestFactory
import org.junit.jupiter.api.condition.DisabledIfEnvironmentVariable
import org.junit.jupiter.api.fail
import pseuco.javaCompiler.runner.PseuCoRunner
import java.io.ByteArrayOutputStream
import java.io.File
import java.time.Duration
import kotlin.reflect.full.functions
import kotlin.reflect.jvm.isAccessible

@DisabledIfEnvironmentVariable(named = "CI", matches = "true")
class CompilerTests {

    @Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
    private val tests = File(this::class.java.getResource("/pseuco-tests/tests").toURI())
        .listFiles { f -> f.extension == "json" }
        .map { f ->
            Json { isLenient = true }.decodeFromString<CompilerTestConfig>(f.readText()).let {
                CompilerTest(File(f.absolutePath.replace(Regex("json$"), "pseuco")),
                    "${it.description} (${f.nameWithoutExtension})", it.correctnessProperties)
            }
        }

    @TestFactory
    fun compilerTests() = tests.map { test ->
        test.correctnessProperties.map { cp ->
            dynamicTest(test.description) {
                assumeTrue(cp is CorrectnessProperty.DeterministicOutput) { "Type of correctness property is currently unsupported." }
                assumeTrue(cp.requireTermination) { "Tests not requiring termination are currently skipped." }

                val outStream = ByteArrayOutputStream()
                val errStream = ByteArrayOutputStream()

                val joinFunction = PseuCoCompilerTask::class.functions.find { it.name == "joinAgents" }!!.apply { isAccessible = true }

                PseuCoRunner(test.file, outStream, errStream).apply {
                    onError { fail(it) }
                    assertTimeoutPreemptively(Duration.ofSeconds(2)) { run() }
                }.also { joinFunction.call(it) }

                val output = outStream.toString().lines().filter { it.isNotEmpty() }
                when (cp) {
                    is CorrectnessProperty.DeterministicOutput -> {
                        assertEquals(cp.output.size, output.size)
                        assertIterableEquals(cp.output, output)
                    }
                    else -> fail { "Unsupported correctness property: '${cp::class.simpleName!!.toLowerCase()}'" }
                }
            }
        }
    }.flatten()

    private data class CompilerTest(val file: File, val description: String, val correctnessProperties: List<CorrectnessProperty>)

    @Serializable
    private data class CompilerTestConfig(val description: String, val correctnessProperties: List<CorrectnessProperty>)

    @Serializable
    private sealed class CorrectnessProperty {
        @Required val requireTermination: Boolean = true

        @Serializable
        @SerialName("deterministicOutput")
        data class DeterministicOutput(val output: List<String>) : CorrectnessProperty()

        @Serializable
        @SerialName("possibleOutputs")
        data class PossibleOutputs(val possibilities: List<String>) : CorrectnessProperty()
    }
}
