package fxGui.main

import javafx.application.Application
import javafx.stage.Stage
import org.apache.commons.lang3.StringUtils
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.condition.DisabledIfEnvironmentVariable
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.CsvSource
import org.junit.jupiter.params.provider.ValueSource
import kotlin.concurrent.thread

/**
 * Tests for the pseuCo code input area.
 *
 * @author Konstantin Kopper
 * @see PseuCoArea
 */
@DisplayName("Code input area tests")
@DisabledIfEnvironmentVariable(named = "CI", matches = "true")
class PseuCoAreaTest {

    private lateinit var codeArea: PseuCoArea

    @BeforeEach
    fun init() {
        codeArea = PseuCoArea()
    }

    @Test
    @DisplayName("Replace tab by spaces")
    fun testSingleTabReplacement() {
        typeString("\t")
        assertEquals("    ", codeArea.text)
    }

    @Test
    @DisplayName("Add closing double quote (empty string)")
    fun testDoubleQuoteEmpty() {
        typeString("""println("""")
        assertEquals("""println("")""", codeArea.text)
    }

    @Test
    @DisplayName("Add closing double quote")
    fun testDoubleQuote() {
        typeString("""println("Hello World""")
        assertEquals("""println("Hello World")""", codeArea.text)
    }

    @Test
    @DisplayName("Add closing parenthesis")
    fun testCompletionParenthesis() {
        typeString("void testFun(")
        assertEquals("void testFun()", codeArea.text)
    }

    @Test
    @DisplayName("Add closing square bracket")
    fun testCompletionSquareBrackets() {
        typeString("int[")
        assertEquals("int[]", codeArea.text)
    }

    @Test
    @DisplayName("Add closing curly brace")
    fun testCompletionCurlyBrace() {
        typeString("mainAgent {")
        assertEquals("mainAgent {}", codeArea.text)
    }

    @Test
    @DisplayName("No double closing parentheses")
    fun testClosingParentheses() {
        typeString("void myFun(")
        assertEquals("void myFun()", codeArea.text)
        typeString("int x)")
        assertEquals("void myFun(int x)", codeArea.text)
    }

    @Test
    @DisplayName("No double closing square brackets")
    fun testClosingSquareBracket() {
        typeString("int x = arr[")
        assertEquals("int x = arr[]", codeArea.text)
        typeString("42")
        assertEquals("int x = arr[42]", codeArea.text)
    }

    @Test
    @DisplayName("No double closing curly brace")
    fun testClosingCurlyBrace() {
        typeString("void fun() {")
        assertEquals("void fun() {}", codeArea.text)
        typeString("/* just a comment */")
        assertEquals("void fun() {/* just a comment */}", codeArea.text)
    }

    @DisplayName("Basic bracket matching")
    @ParameterizedTest(name = "Matching {0} and {1}")
    @CsvSource("(, )", "[, ]", "{, }")
    fun testCompletionBrackets(opening: String, closing: String) {
        typeString(opening)
        assertEquals("$opening$closing", codeArea.text)
    }

    @DisplayName("Replace multiple tabs")
    @ParameterizedTest(name = "{0} tabs")
    @ValueSource(ints = [2, 3, 5, 42])
    fun testMultipleTabReplacement(numTabs: Int) {
        operator fun String.times(n: Int) = StringUtils.repeat(this, n)
        typeString("\t" * numTabs)
        assertEquals("    " * numTabs, codeArea.text)
    }

    @Test
    @DisplayName("Define an empty mainAgent")
    fun testEmptyMainAgent() {
        typeString("mainAgent {\n")
        assertEquals("mainAgent {\n    \n}", codeArea.text)
    }

    @Test
    @DisplayName("Type a simple function")
    fun testDefineFunction() {
        typeString("int simpleFun(int x, int y) {\nreturn x + y;")
        assertEquals("int simpleFun(int x, int y) {\n    return x + y;\n}", codeArea.text)
    }

    @Test
    @DisplayName("Type a complex function")
    fun testDefineComplexFunction() {
        typeString("int complexFun(int x, int y")
        moveRight()
        typeString(" {\nint z;\nif (x < y")
        moveRight()
        typeString(" {\nz = 42;")
        moveRight(6)
        typeString(" else {\nz = 123;")
        moveRight(6)
        typeString("\nreturn z;")
        assertEquals("""int complexFun(int x, int y) {
            |    int z;
            |    if (x < y) {
            |        z = 42;
            |    } else {
            |        z = 123;
            |    }
            |    return z;
            |}
        """.trimMargin(), codeArea.text)
    }

    /**
     * Adds string character by character to code area.
     * This should simulate typing.
     *
     * @author Konstantin Kopper
     */
    private fun typeString(t: String) = t.forEach {
        codeArea[codeArea.caretPosition] = it.toString()
        Thread.sleep(5)
    }

    /**
     * Moves the cursor inside the code area.
     *
     * @author Konstantin Kopper
     * @param n The number of characters to be moved. Use a negative number to move the cursor to the left.
     */
    private fun moveRight(n: Int = 1) {
        codeArea.moveTo(codeArea.caretPosition + n)
    }

    companion object : Application() {
        @JvmStatic
        @BeforeAll
        fun initJavaFX() {
            thread(start = true, isDaemon = true) {
                Application.launch(this::class.java)
            }
        }

        override fun start(primaryStage: Stage?) {
        }
    }
}
