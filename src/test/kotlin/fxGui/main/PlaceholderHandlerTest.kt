package fxGui.main

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertIterableEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertAll
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.api.condition.DisabledIfEnvironmentVariable
import org.junit.jupiter.api.extension.ExtensionContext
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.ArgumentsProvider
import org.junit.jupiter.params.provider.ArgumentsSource
import org.junit.jupiter.params.provider.ValueSource
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.anyInt
import org.mockito.Mockito.mock
import java.util.stream.Stream

/**
 * Tests [PlaceholderHandler] using a mocked [PseuCoArea].
 *
 * @author Konstantin Kopper
 */
@DisplayName("Handling of placeholders")
@DisabledIfEnvironmentVariable(named = "CI", matches = "true")
class PlaceholderHandlerTest {

    @Mock
    private lateinit var codeArea: PseuCoArea
    private lateinit var placeholderHandler: PlaceholderHandler

    @BeforeEach
    fun setUp() {
        codeArea = mock(PseuCoArea::class.java)
        `when`(codeArea.text).thenReturn(code)
        `when`(codeArea.length).thenReturn(code.length)
        `when`(codeArea[anyInt()]).then { code[it.arguments.first() as Int].toString() }

        placeholderHandler = PlaceholderHandler(codeArea)
        val list = getTabsList(placeholderHandler) as MutableList<Int>
        list.add(15)
        list.add(0)
    }

    @Test
    @DisplayName("Check count property")
    fun testCountProperty() {
        assertEquals(2, placeholderHandler.count, "Invalid 'count' property")
    }

    @Test
    @DisplayName("Check number of placeholders")
    fun testCountPlaceholdersUntil() {
        val method = placeholderHandler::class.java.getDeclaredMethod("countPlaceholdersUntil", Int::class.java).apply { isAccessible = true }
        fun callPrivateMethod(position: Int) = method.invoke(placeholderHandler, position)

        assertAll({
            assertAll((0..31).map {
                {
                    assertEquals(0, callPrivateMethod(it), "Invalid number of placeholders for position $it")
                }
            })
        }, {
            assertAll((32..33).map {
                {
                    assertEquals(1, callPrivateMethod(it), "Invalid number of placeholders for position $it")
                }
            })
        }, {
            assertAll((34 until code.length).map {
                {
                    assertEquals(2, callPrivateMethod(it), "Invalid number of placeholders for position $it")
                }
            })
        })
    }

    @Test
    @DisplayName("Check for placeholders")
    fun testHasPlaceholder() {
        assertAll({
            assertAll((0..33).map {
                {
                    assertTrue(placeholderHandler.hasPlaceholderAfter(it), "Did not find placeholder after position $it")
                }
            })
        }, {
            assertAll((34 until code.length).map {
                {
                    assertFalse(placeholderHandler.hasPlaceholderAfter(it), "Found placeholder after position $it")
                }
            })
        })
    }

    @Test
    @DisplayName("Find placeholder positions")
    fun testFindNextPlaceholder() {
        assertAll({
            assertAll((0..31).map {
                { assertEquals(31, placeholderHandler.findNextPlaceholder(it), "Unexpected position of next placeholder") }
            })
        }, {
            assertAll((32..33).map {
                { assertEquals(33, placeholderHandler.findNextPlaceholder(it), "Unexpected position of next placeholder") }
            })
        }, {
            assertAll((34 until code.length).map {
                { assertEquals(-1, placeholderHandler.findNextPlaceholder(it), "Unexpected position of next placeholder") }
            })
        })
    }

    @Test
    @DisplayName("Rejection of invalid positions")
    fun testGetLengthForPlaceholderInvalid() {
        assertAll(((0..30) + 32 + (34 until code.length)).map {
            {
                assertThrows<IllegalArgumentException>("Expected exception for position $it") { placeholderHandler.getLengthForPlaceholderAt(it) }
                Unit
            }
        })
    }

    @Test
    @DisplayName("Check length of first placeholder")
    fun testGetLengthForPlaceholder1() =
            assertEquals(15, placeholderHandler.getLengthForPlaceholderAt(31), "Unexpected 'length' for position 31")

    @Test
    @DisplayName("Check length of second placeholder")
    fun testGetLengthForPlaceholder2() =
            assertEquals(0, placeholderHandler.getLengthForPlaceholderAt(33), "Unexpected 'length' for position 33")

    @Test
    @DisplayName("Insertion of a new placeholder before any other")
    fun testInsertPlaceholderFront() {
        placeholderHandler.insertPlaceholderAt(2, 2)
        assertIterableEquals(listOf(2, 15, 0), getTabsList(placeholderHandler), "Incorrect internal list")
    }

    @Test
    @DisplayName("Insertion of a new placeholder between existing")
    fun testInsertPlaceholderMiddle() {
        placeholderHandler.insertPlaceholderAt(32, 42)
        assertIterableEquals(listOf(15, 42, 0), getTabsList(placeholderHandler), "Incorrect internal list")
    }

    @Test
    @DisplayName("Insertion of a new placeholder after any other")
    fun testInsertPlaceholderBack() {
        placeholderHandler.insertPlaceholderAt(34, 7)
        assertIterableEquals(listOf(15, 0, 7), getTabsList(placeholderHandler), "Incorrect internal list")
    }

    @Test
    @DisplayName("Rejection of invalid positions when inserting")
    fun testInsertPlaceholderInvalid() {
        assertAll((-1..code.length + 1 step (code.length + 2)).map {
            {
                assertThrows<IllegalArgumentException>("Invalid position $it") { placeholderHandler.insertPlaceholderAt(it, 0) }
                Unit
            }
        })
    }

    @Test
    @DisplayName("Deletion of text which includes no placeholders")
    fun testDeletePlaceholdersAtEmpty() {
        println("Test")
        assertAll({
            assertAll((0..31).map { start ->
                {
                    assertAll((start..32).map { end ->
                        { assertEquals(0, placeholderHandler.deletePlaceholdersAt(start, end), "Unexpected number of deleted placeholders") }
                    })
                }
            })
        })
    }

    @ParameterizedTest(name = "Delete one placeholder between position {0} and {1}")
    @ArgumentsSource(SinglePlaceholderRangeProvider::class)
    @DisplayName("Delete single placeholder")
    fun testDeletePlaceholdersAt1(start: Int, end: Int) {
        assertEquals(1, placeholderHandler.deletePlaceholdersAt(start, end), "Unexpected number of deleted placeholders")
        assertIterableEquals(listOf(0), getTabsList(placeholderHandler), "Invalid internal list")
    }

    @ParameterizedTest(name = "Delete two placeholders between position {0} and {1}")
    @ArgumentsSource(DoublePlaceholderRangeProvider::class)
    @DisplayName("Delete two placeholders")
    fun testDeletePlaceholderAt2(start: Int, end: Int) {
        assertEquals(2, placeholderHandler.deletePlaceholdersAt(start, end), "Unexpected number of deleted placeholders")
        assertIterableEquals(emptyList<Int>(), getTabsList(placeholderHandler), "Invalid internal list")
    }

    @ParameterizedTest(name = "Delete {1} placeholder(s) following position {0}")
    @ArgumentsSource(PositionCountRangeProvider::class)
    @DisplayName("Delete placeholders following a given position")
    fun testDeletePlaceholderFrom1(position: Int, count: Int) {
        val expectedList = listOf(15, 0).drop(count)
        placeholderHandler.deletePlaceholdersFrom(position, count)
        assertIterableEquals(expectedList, getTabsList(placeholderHandler), "Invalid internal list")
    }

    @ParameterizedTest(name = "Delete one placeholder following position {0}")
    @ValueSource(ints = [32, 33])
    @DisplayName("Delete a placeholder following a position")
    fun testDeletePlaceholderFrom2(position: Int) {
        placeholderHandler.deletePlaceholdersFrom(position, 1)
        assertIterableEquals(listOf(15), getTabsList(placeholderHandler), "Invalid internal list")
    }

    /**
     * Creates an argument stream mapping each element of [[start1], [end1]] to each element of [[start2], [end2]].
     *
     * @author Konstantin Kopper
     */
    private open class TwoDimensionalRangeProvider(
        private val start1: Int,
        private val start2: Int,
        private val end1: Int,
        private val end2: Int
    ) : ArgumentsProvider {
        override fun provideArguments(context: ExtensionContext?): Stream<out Arguments> =
                (start1..end1).fold(emptyList<Pair<Int, Int>>()) { list, start ->
                    list + (start2..end2).map { end -> start to end }
                }.stream().map { Arguments.of(it.first, it.second) }
    }

    private object SinglePlaceholderRangeProvider : TwoDimensionalRangeProvider(0, 33, 31, 34)
    private object DoublePlaceholderRangeProvider : TwoDimensionalRangeProvider(0, 35, 31, code.length)

    private object PositionCountRangeProvider : TwoDimensionalRangeProvider(0, 1, 31, 2)

    companion object {
        private val code = PlaceholderHandler.PLACEHOLDER.let { "mainAgent {\n    /* statement */$it;$it\n}" }

        @Suppress("unchecked_cast")
        private fun getTabsList(handler: PlaceholderHandler) =
                PlaceholderHandler::class.java.getDeclaredField("tabs").apply { isAccessible = true }.get(handler) as List<Int>
    }
}
