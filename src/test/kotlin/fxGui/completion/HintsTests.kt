package fxGui.completion

import org.junit.jupiter.api.Assertions.assertArrayEquals
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertAll
import org.junit.jupiter.api.condition.DisabledIfEnvironmentVariable

@DisabledIfEnvironmentVariable(named = "CI", matches = "true")
internal class HintsTests {

    @BeforeEach
    fun init() {
        // Just use the singleton here, at it will be initialized at the first use.
        Hints
    }

    @Test
    @DisplayName("Hints for input 'c'")
    fun testC() {
        assertTrue("c" in Hints)
        val replacements = Hints["c"]
        assertEquals(6, replacements.size, "Unexpected number of hints.")
        assertArrayEquals((replacementsCase + replacementsCondition + arrayOf(Replacement("continue", "continue"))), replacements.toTypedArray(), "Unexpected replacements.")
    }

    @Test
    @DisplayName("Hints for input 'case'")
    fun testCase() {
        assertTrue("case" in Hints)
        val replacements = Hints["case"]
        assertEquals(4, replacements.size, "Unexpected number of hints.")
        assertArrayEquals(replacementsCase, replacements.toTypedArray(), "Unexpected replacements.")
    }

    @Test
    @DisplayName("Hints for input 'cond'")
    fun testCondition() {
        assertTrue("cond" in Hints)
        val replacements = Hints["cond"]
        assertEquals(1, replacements.size, "Unexpected number of hints.")
        assertArrayEquals(replacementsCondition, replacements.toTypedArray(), "Unexpected replacements.")
    }

    /*
    @ParameterizedTest
    @ValueSource(strings = { "a", "b", "g", "h", "k", "n", "o", "q", "t", "v", "x", "y", "z" })
    @DisplayName("No hints available")
    fun testNoHints(input: String) {
        assertFalse(input in Hints)
        assertEquals(0, Hints[input].size, "Expected no hints, but got some.")
    }
    */

    @Test
    @DisplayName("Regex creation")
    fun testRegex() {
        val m = Hints.javaClass.getDeclaredMethod("regex", String::class.java).apply { isAccessible = true }
        val regex = m(Hints, "abc") as Regex

        val allowed = listOf("abc", "abcd", "abc12345")
        val notAllowed = listOf("abd", "xxabcxx", "ABC", "aBc", " abc", "abc ")

        assertAll("Regex 'abc'",
                { allowed.map { assertTrue(regex.matches(it)) } },
                { notAllowed.map { assertFalse(regex.matches(it)) } }
        )
    }

    companion object {
        val replacementsCase = arrayOf(
                Replacement("case ...: {...}", "case : {}", true, listOf(
                        Placeholder(5, "/* case specification */"),
                        Placeholder(8, true, "/* statements */")
                )),
                Replacement("case ... <! ...: {...}", "case  <! : {}", true, listOf(
                        Placeholder(5, "/* channel */"),
                        Placeholder(9, "/* expression */"),
                        Placeholder(12, true, "/* statements */")
                )),
                Replacement("case ... = <? ...: {...}", "case  = <? : {}", true, listOf(
                        Placeholder(5, "/* identifier */"),
                        Placeholder(11, "/* channel */"),
                        Placeholder(14, true, "/* statements */")
                )),
                Replacement("case <? ...: {...}", "case <? : {}", true, listOf(
                        Placeholder(8, "/* channel */"),
                        Placeholder(11, true, "/* statements */")
                ))
        )
        val replacementsCondition = arrayOf(
                Replacement("condition ... with (...);", "condition  with ();", true, listOf(
                        Placeholder(10, "/* name */"),
                        Placeholder(17, "/* expression */")
                ))
        )
    }
}
