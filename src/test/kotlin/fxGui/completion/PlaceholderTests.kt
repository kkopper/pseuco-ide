package fxGui.completion

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test

class PlaceholderTests {

    private lateinit var placeholder: Placeholder

    @BeforeEach
    fun init() {
        placeholder = Placeholder(position, text)
    }

    @Test
    @DisplayName("Properties")
    fun testProperties() {
        assertEquals(position, placeholder.position)
        assertFalse(placeholder.expand)
        assertEquals(text, placeholder.text)
    }

    companion object {
        const val position = 42
        const val text = "The answer to everything."
    }
}
