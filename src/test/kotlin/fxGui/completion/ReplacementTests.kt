package fxGui.completion

import org.junit.jupiter.api.Assertions.assertArrayEquals
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test

internal class ReplacementTests {

    private lateinit var replacement: Replacement

    @BeforeEach
    fun init() {
        replacement = Replacement("The best replacement ever created!", "Insert  here.", true, listOf(
                Placeholder(7, "text")
        ))
    }

    @Test
    @DisplayName("Properties")
    fun testProperties() {
        assertEquals(displayText, replacement.displayText, "Unexpected displayText.")
        assertEquals(template, replacement.template, "Unexpected template.")
        assertEquals(hasContinuation, replacement.hasContinuation, "Unexpected hasContinuation flag.")
        assertArrayEquals(placeholders, replacement.placeholders.toTypedArray())
    }

    companion object {
        const val displayText = "The best replacement ever created!"
        const val template = "Insert  here."
        const val hasContinuation = true
        val placeholders = arrayOf(Placeholder(7, "text"))
    }
}
