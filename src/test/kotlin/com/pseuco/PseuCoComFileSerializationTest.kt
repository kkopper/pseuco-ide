package com.pseuco

import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.encodeToJsonElement
import kotlinx.serialization.json.jsonObject
import kotlinx.serialization.json.jsonPrimitive
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertAll
import org.junit.jupiter.api.assertDoesNotThrow

internal class PseuCoComFileSerializationTest {

    @Test
    @DisplayName("Serialize pseuCo file")
    fun testPseuCoComFileSerialization() {
        val pseuCoComFile =
            PseuCoComFile("helloWorld", """mainAgent{println("Hello World!");}""", PseuCoComFile.Type.PSEUCO)
        val jsonFile = Json.encodeToJsonElement(pseuCoComFile)

        val jsonObject = assertDoesNotThrow { jsonFile.jsonObject }

        assertEquals(3, jsonObject.size)

        assertAll(
            {
                assertTrue(jsonObject.containsKey("name"))
                assertEquals("helloWorld", jsonObject["name"]!!.jsonPrimitive.content)
            },
            {
                assertTrue(jsonObject.containsKey("content"))
                assertEquals("""mainAgent{println("Hello World!");}""", jsonObject["content"]!!.jsonPrimitive.content)
            },
            {
                assertTrue(jsonObject.containsKey("type"))
                assertEquals("pseuco", jsonObject["type"]!!.jsonPrimitive.content)
            }
        )
    }

    @Test
    @DisplayName("Deserialize pseuCo file")
    fun testPseuCoComDeserialization() {
        val jsonFile = """{"name":"helloWorld","content":"mainAgent{println(\"Hello World!\");}","type":"pseuco"}"""
        val pseuCoComFile = Json.decodeFromString<PseuCoComFile>(jsonFile)

        assertAll(
            { assertEquals("helloWorld", pseuCoComFile.name) },
            { assertEquals("""mainAgent{println("Hello World!");}""", pseuCoComFile.content) },
            { assertEquals(PseuCoComFile.Type.PSEUCO, pseuCoComFile.type) }
        )
    }
}
