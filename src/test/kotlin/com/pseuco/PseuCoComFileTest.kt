package com.pseuco

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test

/**
 * PseuCoComFile tests.
 *
 * @author Konstantin Kopper
 * @see PseuCoComFile
 */
@DisplayName("PseuCoCom file wrapper")
internal class PseuCoComFileTest {

    private lateinit var file: PseuCoComFile

    @BeforeEach
    fun init() {
        file = PseuCoComFile(NAME, CONTENT)
    }

    @Test
    @DisplayName("Attributes")
    fun testAttributes() {
        assertEquals(PseuCoComFile.Type.PSEUCO, file.type)
        assertEquals(NAME, file.name)
        assertEquals(CONTENT, file.content)
    }

    companion object {
        private const val NAME = "Hello World"
        private const val CONTENT = "mainAgent {\n\tprintln(\"Hello World!\");\n}"
        private const val EXPECTED_JSON = """{"type":"pseuco","name":"Hello World","content":"mainAgent {\n\tprintln(\"Hello World!\");\n}"}"""
    }
}
