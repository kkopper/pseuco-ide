package com.pseuco.websocket

import com.pseuco.PseuCoComFile
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.decodeFromJsonElement
import kotlinx.serialization.json.encodeToJsonElement
import kotlinx.serialization.json.int
import kotlinx.serialization.json.jsonObject
import kotlinx.serialization.json.jsonPrimitive
import org.junit.jupiter.api.Assertions.assertAll
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertDoesNotThrow
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.EnumSource

internal class SocketMessageSerializationTest {

    @Test
    @DisplayName("Serialize 'get' message")
    fun testGetSerialize() {
        val getMessage = SocketMessage.Request.Get(42)
        val jsonMessage = Json.encodeToJsonElement<SocketMessage>(getMessage)

        val jsonObject = assertDoesNotThrow { jsonMessage.jsonObject }

        assertEquals(2, jsonObject.size)

        assertAll(
            {
                assertTrue(jsonObject.containsKey("type"))
                assertEquals("get", jsonObject["type"]!!.jsonPrimitive.content)
            },
            {
                assertTrue(jsonObject.containsKey("id"))
                assertDoesNotThrow { assertEquals(42, jsonObject["id"]!!.jsonPrimitive.int) }
            }
        )
    }

    @Test
    @DisplayName("Deserialize 'get' message")
    fun testGetDeserialize() {
        val jsonMessage = """{"type":"get","id":42}"""
        val getMessage = Json.decodeFromString<SocketMessage>(jsonMessage)

        assertTrue(getMessage is SocketMessage.Request.Get)

        assertEquals(42, (getMessage as SocketMessage.Request.Get).id)
    }

    @Test
    @DisplayName("Serialize 'open' message")
    fun testOpenSerialize() {
        val openMessage = SocketMessage.Request.Open(PSEUCO_COM_FILE)
        val jsonMessage = Json.encodeToJsonElement<SocketMessage>(openMessage)

        val jsonObject = assertDoesNotThrow { jsonMessage.jsonObject }

        assertAll(
            {
                assertTrue(jsonObject.containsKey("type"))
                assertEquals("open", jsonObject["type"]!!.jsonPrimitive.content)
            },
            {
                assertTrue(jsonObject.containsKey("file"))
                val fileJsonObject = assertDoesNotThrow { jsonObject["file"]!!.jsonObject }
                val fileObject = assertDoesNotThrow { Json.decodeFromJsonElement<PseuCoComFile>(fileJsonObject) }
                assertEquals(PSEUCO_COM_FILE, fileObject)
            }
        )
    }

    @Test
    @DisplayName("Deserialize 'open' message")
    fun testOpenDeserialize() {
        val jsonMessage = """{"type":"open","file":${Json.encodeToString(PSEUCO_COM_FILE)}}"""
        val openMessage = Json.decodeFromString<SocketMessage>(jsonMessage)

        assertTrue(openMessage is SocketMessage.Request.Open)

        assertEquals(PSEUCO_COM_FILE, (openMessage as SocketMessage.Request.Open).file)
    }

    @Test
    @DisplayName("Serialize 'success' message")
    fun testSuccessSerialize() {
        val successMessage = SocketMessage.Response.Success
        val jsonMessage = Json.encodeToJsonElement<SocketMessage>(successMessage)

        // assertTrue(jsonMessage is JsonObject)
        val jsonObject = assertDoesNotThrow { jsonMessage.jsonObject }

        assertEquals(1, jsonObject.size)
        assertTrue(jsonObject.containsKey("type"))
        assertEquals("success", jsonObject["type"]!!.jsonPrimitive.content)
    }

    @Test
    @DisplayName("Deserialize 'success' message")
    fun testSuccessDeserialize() {
        val jsonMessage = """{"type":"success"}"""
        val successMessage = Json.decodeFromString<SocketMessage>(jsonMessage)

        assertTrue(successMessage is SocketMessage.Response.Success)
    }

    @ParameterizedTest
    @EnumSource
    @DisplayName("Serialize 'error' message")
    fun testErrorSerialize(code: SocketMessage.Response.Error.Code) {
        val errorMessage = SocketMessage.Response.Error("Oops!", code)
        val jsonMessage = Json.encodeToJsonElement<SocketMessage>(errorMessage)

        val jsonObject = assertDoesNotThrow { jsonMessage.jsonObject }

        assertEquals(3, jsonObject.size)

        assertAll(
            {
                assertTrue(jsonObject.containsKey("type"))
                assertEquals("error", jsonObject["type"]!!.jsonPrimitive.content)
            },
            {
                assertTrue(jsonObject.containsKey("message"))
                assertEquals("Oops!", jsonObject["message"]!!.jsonPrimitive.content)
            },
            {
                assertTrue(jsonObject.containsKey("code"))
                assertEquals(convertCodeToCamelCase(code), jsonObject["code"]!!.jsonPrimitive.content)
            }
        )
    }

    @ParameterizedTest
    @EnumSource
    @DisplayName("Deserialize 'error' message")
    fun testErrorDeserialize(code: SocketMessage.Response.Error.Code) {
        val jsonMessage = """{"type":"error","message":"Oops!","code":"${convertCodeToCamelCase(code)}"}"""
        val socketMessage = Json.decodeFromString<SocketMessage>(jsonMessage)

        assertTrue(socketMessage is SocketMessage.Response.Error)

        val errorMessage = socketMessage as SocketMessage.Response.Error

        assertAll(
            { assertEquals("Oops!", errorMessage.message) },
            { assertEquals(code, errorMessage.code) }
        )
    }

    companion object {
        private val PSEUCO_COM_FILE = PseuCoComFile("helloWorld", """mainAgent{println("Hello World!");}""", PseuCoComFile.Type.PSEUCO)

        private fun convertCodeToCamelCase(code: SocketMessage.Response.Error.Code) = code.name.toLowerCase().replace(Regex("_(\\w)")) { match -> match.groupValues[1].toUpperCase() }
    }
}
