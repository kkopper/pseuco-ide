# pseuCo IDE

An IDE for the pseuCo programming language written in [Kotlin](https://kotlinlang.org).
Uses the [pseuco-java-compiler](https://dgit.cs.uni-saarland.de/pseuco/pseuco-java-compiler) internally.

## Get the IDE

The current stable version can be downloaded [here](https://dgit.cs.uni-saarland.de/pseuco/pseuco-ide/-/jobs/artifacts/master/download?job=jar).

## Build the IDE from scratch

After cloning the source code, it is required to download the compiler as well.
Assuming you are in the root folder of the repository, type
````
git submodule init
git submodule update
````

This can be circumvented by using `--recurse-submodules` when cloning, which performs these steps automatically.

If you want to find out more about git submodules, have a look at the [Git book](https://git-scm.com/book/en/v2/Git-Tools-Submodules).

### Build

The IDE is built using [Gradle](https://gradle.org) by calling

    gradle jar

This creates an executable `*.jar` in `build/libs/`.

### Running the tests

To run the tests, just call

    gradle test
    
Remark: this call also executes all tests of the [pseuco-java-compiler](https://dgit.cs.uni-saarland.de/pseuco/pseuco-java-compiler).
To execute only the tests of the IDE, use

    gradle :test

Both calls produce a test report located in `build/reports/tests`, the latest report from the build server is available [here](https://dgit.cs.uni-saarland.de/pseuco/pseuco-ide/-/jobs/artifacts/master/download?job=tests).

### Documentation

[Dokka](https://github.com/Kotlin/dokka) is used for the documentation, a report is created by calling

    gradle dokka

This produces a HTML version comparable to Javadocs located at `build/dokka`.
Again, the latest version from the build server is available [here](https://dgit.cs.uni-saarland.de/pseuco/pseuco-ide/-/jobs/artifacts/master/download?job=dokka).
